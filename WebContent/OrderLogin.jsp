<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<!-- saved from url=(0049)https://pay.dscount.com/customer/order_search.asp -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>주문조회</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/AceCounter_AW.js.다운로드"></script><script async="" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/analytics.js.다운로드"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/top_javascript.js.다운로드"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/jquery.min.1.7.2.js.다운로드"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/jquery.cookie.js.다운로드"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/cookie.js.다운로드"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
// 로그인 정보 저장
function confirmSave(checkbox)
{
  var isRemember;
  
  // 로그인 정보 저장한다고 선택할 경우
  if(checkbox.checked)
  {
   
//	isRemember = confirm("이 PC에 로그인 정보를 저장하시겠습니까? \n\nPC방등의 공공장소에서는 개인정보가 유출될 수 있으니 주의해주십시오.");
    isRemember = true;
    if(!isRemember)
      checkbox.checked = false;
  }
}

// 쿠키에서 로그인 정보 가져오기
function getLoginInfo()
{
 var frm = document.login;
 
 // userid 쿠키에서 id 값을 가져온다.
 var id = getCookie("dahongid");
 

 // 가져온 쿠키값이 있으면
 if(id != "")
 {
  frm.userid.value = id;
  frm.userid.style.background="#FFFFFF";
  frm.saveid.checked = true;
 }

 var autologin = getCookie("dahongautologin");
 

 // 가져온 쿠키값이 있으면
 if(autologin != "")
 {
  frm.autologin.checked = true;
 }
}
 
// 쿠키에 로그인 정보 저장
function saveLogin(id)
{
 if(id != "")
 {
  // userid 쿠키에 id 값을 30일간 저장
  setCookie("dahongid", id, 30);
 }else{
  // userid 쿠키 삭제
  setCookie("dahongid", id, -1);
 }
}

function saveAutologin(arg)
{
 if(arg != "")
 {
  // userid 쿠키에 id 값을 30일간 저장
  setCookie("dahongautologin", arg, 30);
 }else{
  // userid 쿠키 삭제
  setCookie("dahongautologin", arg, -1);
 }
}
-->
</script>
<script language="javascript">
<!--
function LoginOk() 
{		
	if ( document.login.userid.value.length == 0 ) {
		alert("사용자 ID를 입력해 주십시오"); 
		document.login.userid.focus();
		return;		
	}
	else if ( document.login.pass.value.length == 0 ) {
		alert("비밀번호를 입력해 주십시오"); 
		document.login.pass.focus();
		return;		
	}
	
	

	
	document.login.action="${ctx}/login.do";
	document.login.submit();
}

function onlyNumber() 
{                          
	if((event.keyCode<48)||(event.keyCode>57) ) event.returnValue=false; 
} 

function trim(str) 
{	
		strNewName = str
		if(strNewName != null) {			
			for(intLoop = 0; intLoop < strNewName.length; intLoop++) {
				if(strNewName.charAt(intLoop) != " ") break				
			}
			startIndex = intLoop;
			strNewName = strNewName.substr(startIndex);
				
			for(intLoop = strNewName.length - 1; intLoop >= 0; intLoop--) {
				if(strNewName.charAt(intLoop) != " ") break							
			}
			endIndex = intLoop;
			strNewName = strNewName.substr(0, endIndex + 1);
			
			if(strNewName.length <= 0) strNewName = null
		}		
		return strNewName
}

//Auto Tab code start

var isNN = (navigator.appName.indexOf("Netscape")!=-1);
if(isNN)document.captureEvents(Event.KEYPRESS);

function autoTab(input,len, e){
  	var keyCode = (isNN)?e.which:e.keyCode; 	
  	var filter = (isNN)?[0,8,9]:[0,8,9,16,17,18,37,38,39,40,46];
  	if(input.value.length >= len && !containsElement(filter,keyCode)){
    input.value = input.value.slice(0,len);
    input.form[(getIndex(input)+1)%input.form.length].select();
}

function containsElement(arr, ele){
    var found = false, index = 0;
    while(!found && index < arr.length)
      if(arr[index]==ele)
        found = true;
      else
        index++;
    return found;
}

function getIndex(input){
    var index = -1, i = 0, found = false;
    while (i < input.form.length && index==-1)
      if (input.form[i] == input)index = i;
      else i++;
    return index;
}

return true;
}

//Auto Tab code end

function SearchGuestOrder()
{
	if(document.guestform.name.value=="")
	{
		alert("\n성명을 적어주세요!");
		document.guestform.name.focus();
		return;
	}

	
	if (trim(document.guestform.phone2.value) == null || document.guestform.phone2.length < 3 ) 
	{
		alert("이동전화국번/지역번호를 3자리 이상 입력해주세요");
		document.guesform.phone2.focus();
		return;
	}
	
	if (trim(document.guestform.phone3.value) == null || document.guestform.phone3.length < 4) 
	{
		alert("번호를 4자리 이상 입력해주세요");
		document.guestform.phone3.focus();
		return;
	}

	document.guestform.submit();

}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function push()
{
		if (event.keyCode == 13){LoginOk();}
}

function next()
{
		if (event.keyCode == 13){ document.login.pass.focus();}
}
//-->
</script>

<style type="text/css">
</style>
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/f.txt"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/f(1).txt"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/f(2).txt"></script></head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>

  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
      <td align="center"><script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>


<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>

<script language="javascript">


function gotoItemMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
	
}

function gotoConceptGoodView(Gserial)
{
	location = "../Shopping/GoodView_Concept.asp?Gserial="+Gserial;	
	
}

function gotoCoordiView(Gserial)
{
	location = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	
}

function openCoordiView(Gserial)
{
	url = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
	
}

function gotoItemGood(arg)
{
	location = "../Shopping/ItemShopping_detail.asp?b="+arg;	
	
}

function gotoMDGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoCOSGoodView(Gserial)
{
	location = "../NShopping/GoodView_CItem.asp?Gserial="+Gserial;	
}

function gotoZinifGoodView(Gserial)
{
	location = "../NShopping/GoodView_ZItem.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotoMDGabalGoodView(Gserial, Gseq)
{
	location = "../Shopping/GoodView_Gabal.asp?Gserial="+Gserial+"&gseq=" + Gseq;	
}

function gotoNormalGoodView(Gserial)
{
	location = "../Shopping/GoodView_NItem.asp?Gserial="+Gserial;	
}


function gotoDahongGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
}

function openCatGoodView(Gserial)
{
	url =  "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function gotoBrandGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Brand.asp?Gserial="+Gserial;	
}

function gotoKeywordGoodView(Gserial)
{
	location = "../Nshopping/GoodView_Keyword.asp?Gserial="+Gserial;	
}


function gotoSaleGoodView(Gserial)
{
	location = "../Shopping/GoodView_Sale.asp?Gserial="+Gserial;	
}


function gotoItemGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoSummerGoodView(Gserial)
{
	location = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
}

function gotoCatGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotomonoGoodView(Gserial)
{
	location = "../Shopping2/GoodView_monomori.asp?Gserial="+Gserial;	
}

function gotoCatCoordiView(Gserial)
{
	location = "../Nshopping/GoodView_StyleBook.asp?Gserial="+Gserial;	
	
}


function gotoBigGoodView(Gserial)
{
	location = "../Shopping/GoodView_Big.asp?Gserial="+Gserial;	
}

function gotoItemShoppingMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
}


function gotoBigShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_BigDetail.asp?b="+arg;	
}


function gotoSaleShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_SaleDetail.asp?a="+arg;	
}



function openItemGoodView(Gserial)
{
	url = "/Nshopping/GoodView_Item.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function openDahongGoodView(Gserial)
{
	url = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function openSummerGoodView(Gserial)
{
	url = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function zoomPicture(arg)
{
   target = "../Shopping/zoomPicture.asp?Gserial=" + arg;
   window.open(target,"Picture","status=no,toolbar=no,scrollbars=no,resizable=no,width=780,height=780")
}

function gotoAlert()
{
	alert("경고창")
}

function addBookMark(){
window.external.AddFavorite('http://dahong.dscount.com', '디스카운트 [다홍/지니프/크리마레 통합멤버쉽]')
}


function bookmarksite(title,url) {
	if (window.sidebar) // firefox 
		window.sidebar.addPanel(title, url, ""); 
	else if(window.opera && window.print) // opera 
	{ 
		var elem = document.createElement('a'); 
		elem.setAttribute('href',url); 
		elem.setAttribute('title',title); 
		elem.setAttribute('rel','sidebar'); 
		elem.click(); 
	} 
	else if(window.external && ('AddFavorite' in window.external)) // ie
		window.external.AddFavorite(url, title);
	else if(window.chrome) // chrome
	{ 
		alert('ctrl+D 를 눌러서 북마크에 추가해주세요!'); 
	}
}


</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/imagess6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  

<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/banners.js.다운로드"></script>
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/lnb.js.다운로드"></script>




<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->


<script language="javascript">
function div_promotion_onoff(state)
	{
		if (state==1)
		{
			document.getElementById('div_promotion').style.display='';
		}
		else
		{
			document.getElementById('div_promotion').style.display='none';
		}
	}

	function goto_promotion()
	{
		document.location.href='http://dahong.dscount.com/Nshopping/promotion_list.asp';
	}

</script>


<%@include file="header.jsp" %>

  

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:60px 0px 50px 0px;"><span class="ht_30 ct_b bt_20">주문 / 배송 조회</span></td>
  </tr>
</tbody></table>


<table width="1185" border="0" cellspacing="0" cellpadding="0" style="border:solid 1px #e7e7e7;">
  <tbody><tr>
    <td width="592" align="center" valign="top" style="padding:35px 0px; border-right:solid 1px #e7e7e7;">
      <table width="520" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td align="left"><span class="ht_16 ct_b bt_60">회원 주문/배송조회</span></td>
      </tr>
      <tr>
        <td align="left" style="padding:12px 0px 30px 0px;"><span class="ht_12 ct_b bt_40">회원님들은 로그인 후 마이페이지에서 확인하실 수 있습니다.</span></td>
      </tr>
      <tr>
        <td>
<!-- 로그인 테이블 시작--> 
<form method="Post" action="" name="login">   
<table border="0" cellspacing="0" cellpadding="0" width="520">
  <tbody><tr>
    <td width="70" align="left"><span class="ht_15 ct_b bt_40">아이디</span></td>
    <td width="300">
    <input type="text" name="userid" onkeydown="next()" maxlength="100" style="border: 1px solid #d9d9d9; color: #333333; FONT-FAMILY: 돋움; FONT-SIZE: 15px; width:280px; height:57px; line-height:57px; vertical-align:middle; padding:0px 0px 0px 10px; ime-mode:inactive; " onfocus="this.className='id_focus'" onblur="if ( this.value == '' ) { this.className='id_blur' }" class="id_blur" tabindex="1">   </td>
    <td width="150" rowspan="3" align="right"><a href="javascript:LoginOk();"><img src="${ctx}/skin/images/m_02.jpg" border="0" tabindex="3"></a></td>
  </tr>
  <tr>
    <td colspan="2" height="6"></td>
    </tr>
  <tr>
    <td align="left"><span class="ht_15 ct_b bt_40">비밀번호</span></td>
    <td width="300">
    <input type="password" name="pass" onkeydown="push()" maxlength="20" style="border: 1px solid #d9d9d9; color: #333333; FONT-FAMILY: 돋움; FONT-SIZE: 15px; width:280px; height:57px; line-height:57px; vertical-align:middle; padding:0px 0px 0px 10px;" onfocus="this.className='pw_focus'" onblur="if ( this.value == '' ) { this.className='pw_blur' }" class="pw_blur" tabindex="2">    </td>
    </tr>
  <tr>
    <td height="15" colspan="3"></td>
  </tr>
  <tr>
    <td height="25"></td>

    </tr>
</tbody></table>
</form>
<!-- 로그인 테이블 종료-->
    
        </td>
      </tr>
    </tbody></table>
    </td>
    <td width="593" align="center" valign="top" style="padding:35px 0px">
    <table width="520" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td align="left"><span class="ht_16 ct_b bt_60">비회원 주문/배송조회</span></td>
      </tr>
      <tr>
        <td align="left" style="padding:12px 0px 30px 0px;"><span class="ht_12 ct_b bt_40">회원가입하시고 할인쿠폰/회원혜택/적립금 등 다양한 서비스를 받으세요.</span></td>
      </tr>
      <tr>
        <td align="left">
    <!-- 정보입력 테이블 시작-->
<form name="guestform" action="${ctx }/order/guest.do" method="get" style="margin:0px;">
    <table border="0" cellspacing="0" cellpadding="0" width="520">
      <tbody><tr>
        <td width="370">
        
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="100" align="left"><span class="ht_15 ct_b bt_40">주문자 성명</span></td>
            <td width="270" align="left"><input type="text" name="name" size="17" style="border: 1px solid #d9d9d9; color: #333333; FONT-FAMILY: 돋움; FONT-SIZE: 15px; width:250px; height:57px; line-height:57px; vertical-align:middle; padding:0px 0px 0px 10px; ime-mode:active; "></td>
          </tr>
        </tbody></table>
        
        </td>
        <td rowspan="3" width="150" align="right"><img src="${ctx}/skin/images/join_bt04.jpg" border="0" onclick="javascript:SearchGuestOrder()" style="cursor:pointer;cursor:hand;"></td>
      </tr>
      <tr>
        <td height="6"></td>
        </tr>
      <tr>
        <td width="370">
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="100"><span class="ht_15 ct_b bt_40">핸드폰 번호</span></td>
            <td width="270">
            <table width="260" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="80" align="left"><select name="phone1" style="border: 1px solid #d9d9d9; color: #333333; FONT-FAMILY: 돋움; FONT-SIZE: 15px; width:75px; height:57px; line-height:57px; vertical-align:middle; padding:0px 0px 0px 10px;">
                    <option value="010" selected="selected">010 </option>
                  <option value="011">011 </option>
                  <option value="016">016 </option>
                  <option value="017">017 </option>
                  <option value="018">018 </option>
                  <option value="019">019 </option>
                  </select></td>
                <td width="20" align="center">-</td>
                <td width="70"><input type="text" name="phone2" onkeyup="autoTab(this,4,event);" onkeypress="onlyNumber()" maxlength="4" size="4" value="" style="border: 1px solid #d9d9d9; color: #333333; FONT-FAMILY: 돋움; FONT-SIZE: 15px; width:65px; height:57px; line-height:57px; vertical-align:middle; padding:0px 0px 0px 10px; ime-mode:active; "></td>
                <td width="20" align="center">-</td>
                <td width="70"><input type="text" name="phone3" onkeypress="onlyNumber()" maxlength="4" size="4" value="" style="border: 1px solid #d9d9d9; color: #333333; FONT-FAMILY: 돋움; FONT-SIZE: 15px; width:65px; height:57px; line-height:57px; vertical-align:middle; padding:0px 0px 0px 10px; ime-mode:active; "></td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table>
        
        </td>
        </tr>
    </tbody></table>
              </form>
    <!-- 정보입력 테이블 종료-->
        </td>
      </tr>
    </tbody></table>
    </td>
  </tr>
  
</tbody></table><br>
<br><br><br><br><br>

<%@include file="footer.jsp" %>

</body></html>