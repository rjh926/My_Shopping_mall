<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<link rel="stylesheet" type="text/css"
	href="${ctx}/skin/css/TextForm.css">
<script>
function needLogin(){
alert('로그인 먼저 하십시오');
location.href='${ctx}/login.jsp';
}
</script>

<script>
function checkMileage(userId)
{
	
	
	if(userId== null){
		
		needLogin();
	}
	else{
		
	}
	
	
	}
	</script>
	<script type="text/javascript">
function div_my_onoff(state) {
	if (state == 1) {
		$("#div_my").css('z-index', '3000');
		document.getElementById('div_my').style.display = '';
	} else {
		document.getElementById('div_my').style.display = 'none';
		$("#div_my").css('z-index', '1000');
	}
}
	</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
											style="background: #eeeeee;">
											<tbody>
												<tr>
													<td align="center" height="49">
														<div
															style="position: relative; z-index: 720; width: 1185px;">
															<div
																style="position: absolute; top: 49px; left: 0px; width: 100%; display: none"
																id="div_dscountlayer"
																onmouseout="div_dscountlayer_onoff(0)"
																onmouseover="div_dscountlayer_onoff(1)"></div>
															<table width="1185" height="49" border="0"
																cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td width="300" align="left"></td>

																		<td width="885" align="right">
																			<!-- 로그인메뉴시작-->
																			<table height="49" border="0" cellspacing="0"
																				cellpadding="0">
																				<tbody>
																					<tr>

																						<td
																							style="padding: 0px 10px 0px 0px; cursor: pointer; cursor: hand;">
																							<c:choose>
																							
																							<c:when test="${user eq null && admin eq null}">
																							<span class="ht_11 ct_b bt_60"><a href="${ctx }/login.jsp">로그인</a></span></td>
																							</c:when>
																							
																							<c:when test="${admin ne null }">
																							관리자님 환영합니다!
																							<span class="ht_11 ct_b bt_60"><a href="${ctx }/logout.do">로그아웃</a></span></td>
																							</c:when>
																							
																							<c:otherwise>
																							${user.name}님 환영합니다!
																							<span class="ht_11 ct_b bt_60"><a href="${ctx }/logout.do">로그아웃</a></span></td>
																							</c:otherwise>
																							
																							</c:choose>
																						<td
																							style="padding: 0px 10px 0px 10px; cursor: pointer; cursor: hand;">
																							<c:if test="${user eq null }">
																							<span class="ht_11 ct_b bt_60"><a href="${ctx }/join.jsp">회원가입</a></span></td>
																							</c:if>
																						<td>
																							<!-- 마이페이지-->
																							<div style="position: relative; z-index: 740;">
																								<div
																									style="position: absolute; left: -15px; top: 49px; display: none"
																									id="div_my" onmouseout="div_my_onoff(0);"
																									onmouseover="div_my_onoff(1);">
																									<table border="0" cellspacing="0"
																										cellpadding="0">
																										<tbody>
																											<tr>
																												<td align="center"><img
																													src="${ctx}/skin/images/top_arr2.png"></td>
																											</tr>
																											<tr>
																												<td>
																													<table border="0" cellspacing="0"
																														cellpadding="0" width="110"
																														style="background: #ffffff; border: solid 1px #e2e2e2;">
																														<tbody>
																															<tr>
																															<c:choose>
																															<c:when test="${user eq null}">
																															<td
																																	onclick="location.href=https://pay.dscount.com/customer/order_search.asp"
																																	align="center"
																																	style="padding-top: 15px; padding-bottom: 10px; cursor: pointer; cursor: hand;"><span
																																	class="ht_13 ct_g5" onclick="location.replace('${ctx}/OrderLogin.jsp')">주문/배송조회</span></td>
																															</c:when>
																															<c:otherwise>
																																<td
																																	onclick="location.href=https://pay.dscount.com/customer/order_search.asp"
																																	align="center"
																																	style="padding-top: 15px; padding-bottom: 10px; cursor: pointer; cursor: hand;"><span
																																	class="ht_13 ct_g5" onclick="location.replace('${ctx}/order/list.do')">주문/배송조회</span></td>
																																	</c:otherwise></c:choose>
																															</tr>
																															
																															<tr>
																															<c:choose>
																															<c:when test="${admin eq null }">
																																<td
																																	
																																	align="center"
																																	style="padding-bottom: 10px; cursor: pointer; cursor: hand;"><span
																																	class="ht_13 ct_g5"><a href="${ctx}/userModify.jsp">개인정보 수정</a></span></td>
																																	</c:when>
																																	<c:otherwise>
																																	<td
																																	align="center"
																																	style="padding-bottom: 10px; cursor: pointer; cursor: hand;"><span
																																	class="ht_13 ct_g5"><a href="${ctx}/upload.jsp">상품등록</a></span></td>
																																	</c:otherwise>
																																	</c:choose>
																															</tr>
																															
																															<tr>
																																<c:choose>
																																<c:when test="${admin ne null }">
																																<td
																														
																																	align="center"
																																	style="padding-bottom: 10px; cursor: pointer; cursor: hand;"><span
																																	class="ht_13 ct_g5" onclick="location.replace('${ctx}/product/modifylist.do');">상품 정보 수정</span></td>
																																</c:when>
																																<c:otherwise>
																																<td
																														
																																	align="center"
																																	style="padding-bottom: 10px; cursor: pointer; cursor: hand;"><span
																																	class="ht_13 ct_g5" onclick="checkMileage(${user.userId})">마일리지 조회</span></td>
																															</c:otherwise></c:choose>
																															</tr>
																															
																														</tbody>
																													</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</div>
																								<table border="0" cellspacing="0"
																									cellpadding="0">
																									<tbody>
																										<tr>
																											<td height="49"
																												style="padding: 0px 10px 0px 10px; cursor: pointer; cursor: hand;"
																												onmouseover="div_my_onoff(1)"
																												onmouseout="div_my_onoff(0)"
																												><span
																												class="ht_11 ct_b bt_60">마이페이지</span><span
																												class="ht_6 ct_b"> ▼</span></td>
																										</tr>
																									</tbody>
																								</table>
																							</div> <!-- 마이페이지-->

																						</td>
																						<td>
																							
																						</td>
																						<td
																							style="padding: 0px 6px 0px 10px; cursor: pointer; cursor: hand;">
																							
																							<table width="100%" border="0" cellspacing="0"
																								cellpadding="0">
																								<tbody>
																									<tr>
																									<c:choose>
																									<c:when test="${user eq null }">
																									<td><a href="javascript:needLogin()"><img src="${ctx}/skin/images/ntop_001.png"></a></td>
																									<td style="padding: 0px 0px 0px 2px;"><span
																											class="ht_13 bt_60"> (<span
																												id="div_CartCnt" style="color: #b24545;">0</span>)
																										</span></td>
																									</c:when>
																									<c:otherwise>
																										<td><a href="${ctx}/myCart/list.do"><img src="${ctx}/skin/images/ntop_001.png"></a></td>
																										<td style="padding: 0px 0px 0px 2px;"><span
																											class="ht_13 bt_60"> (<span
																												id="div_CartCnt" style="color: #b24545;">${cartsNum }</span>)
																										</span></td>
																										</c:otherwise>
																										</c:choose>
																										
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td
																							style="padding: 0px 0px 0px 10px; cursor: pointer; cursor: hand;">
																							<table width="100%" border="0" cellspacing="0"
																								cellpadding="0">
																								<tbody>
																									<tr>
																										<td><img src="${ctx}/skin/images/ntop_002.png"></td>
																										<td style="padding: 0px 0px 0px 2px;"><span
																											class="ht_13 bt_60">(<span
																												id="div_FavoritesCnt"
																												style="color: #b24545;">0</span>)
																										</span></td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td align="right" valign="top"
																							style="padding: 9px 0px 0px 20px;"><script
																								src="${ctx}/skin/js/top_search.js.다운로드"></script> <script
																								language="JavaScript" type="TEXT/JAVASCRIPT">

	function SearchOk() 
	{
		if ( $.trim( $("#textfield").val() ) == "" )
		{
			alert("검색할 상품명을 입력해 주세요"); 
			$("#textfield").focus();
			return;
		}

		document.search.submit();
	}

	function isSerchEnter()
	{
		if (event.keyCode == 13) SearchOk()
	}


</script> <!-- 상품 검색-->
																							<form name="search" id="search" method="get"
																								action="${ctx }/search.do"
																								style="margin: 0px 0px;">

																								<div class="main_title">
																									<div class="searching_box" id="search_query">
																										

																										<div>
																											<input type="text" name="name"
																												class="inp_txt"
																												style="width: 74px; ime-mode: active; background: #ffffff;">
																										</div>
																										<div class="bt">
																											<input type="image" id="query-btn"
																												class="query_bnt" src="${ctx}/skin/images/sc_bt.png"
																												alt="검색">
																										</div>
																									</div>
																								</div>

																							</form> <!-- 상품검색--></td>
																						<!--
    <td>
<div style="position:relative;">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="cursor:pointer;cursor:hand; padding:0px 0px 0px 3px;" onclick="searchform_act()"><img src="//cdn.dscount.com/images_2016/top/ntop_003.png" /></td>
  </tr>
</table>
<div id="layersearchform" style=" position:absolute; right:0px; top:31px; z-index:730; display:none;">

</div>
</div>
    </td>				
-->
																					</tr>
																				</tbody>
																			</table> <!-- 로그인메뉴종료-->
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</td>
												</tr>
											</tbody>
										</table> <!-- 공통상단 종료 -->
									</td>
								</tr>
								<tr>
									<td align="center" style="border-bottom: solid 1px #e5e5e5;">
										<!-- 두번째 라인 메뉴-->
										<table width="1185" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td>
														<div style="position: relative; width: 1185px;">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tbody>
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</tbody>
															</table>
															<table width="1185" height="85" border="0"
																cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td width="123" align="left" valign="bottom"
																			style="padding: 35px 0px 18px 0px;"><a
																			href="${ctx }/main.do"><img
																				src="${ctx}/skin/images/logo.PNG" border="0"></a></td>
																		<td align="left" valign="bottom"
																			style="padding: 25px 0px 0px 40px;">

																			<table border="0" cellspacing="0" cellpadding="0">
																				<tbody>
																					<tr>
																						<td align="left" valign="bottom" width="360"
																							style="padding: 0px 0px 18px 0px;">

																						</td>
																						<td align="right"
																							style="padding: 0px 0px 0px 0px;">
																							<!--상단우측배너-->

																						</td>
																					</tr>
																				</tbody>
																			</table>

																		</td>
																	</tr>
																</tbody>
															</table>



														
														</div>
													</td>
												</tr>
											</tbody>
										</table> <!-- 두번째 라인 메뉴-->
									</td>
								</tr>
								<tr>
									<td align="center" valign="top" height="63"
										style="padding: 0px 0px 0px 0px;">
										<!-- 세번째 라인 메뉴-->
										<table width="1185" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td>
														<table border="0" cellspacing="0" cellpadding="0">
															<tbody>
																<tr>
																
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;">
																		<span class="et_so15 ct_b bt_50"><a href="${ctx}/findAll.do">ALL</a></span></td>
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;">
																		<span class="et_so13 ct_9 bt_10">|</span></td>
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;">
																		<span class="et_so15 ct_b bt_50"><a href="${ctx }/findByGroup.do?group=OUTER">OUTER</a></span></td>
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;">
																		<span class="et_so15 ct_b bt_50"><a href="${ctx }/findByGroup.do?group=T-SHIRTS">T-SHIRTS</a></span></td>
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;"><span
																		class="et_so15 ct_b bt_50"><a href="${ctx }/findByGroup.do?group=SHIRTS">SHIRTS</a></span></td>
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;"><span
																		class="et_so15 ct_b bt_50"><a href="${ctx }/findByGroup.do?group=KNEET/CARDIGAN">KNIT / CARDIGAN</a></span></td>
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;">
																		<span class="et_so15 ct_b bt_50"><a href="${ctx }/findByGroup.do?group=PANTS">PANTS</a></span></td>
																	
																
																	<td
																		style="padding: 0px 15px 0px 15px; cursor: pointer; cursor: hand;">
																		<span class="et_so13 ct_9 bt_10">|</span></td>
																	
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table> <!-- 세번째 라인 메뉴-->
									</td>
								</tr>
							</tbody>
						</table></td>
				</tr>
			</tbody>
		</table>


</body>
</html>
