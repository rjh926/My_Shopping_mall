<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>비밀번호 찾기 </title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/top_javascript.js.다운로드"></script><script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script><script src="${ctx}/skin/js/jquery.cookie.js.다운로드"></script><script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/cookie.js.다운로드"></script>


</head><body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>

<script language="JavaScript">
<!--
function FindPass() 
{		
	if ( document.frm.pass.value=="" ) {
		alert("비밀번호를 입력하세요"); 
		document.frm.Name.focus();
		return;		
	}
	if( document.frm.passchk.value==""){
		alert("비밀번호를 한번더 입력하세요");
		document.frm.passchk.focus();
		return;
	}
	
	if(document.frm.pass.value != document.frm.passchk.value)
		{alert("비밀번호가 일치하지 않습니다.");
		document,frm.passchk.focus();
		return;
		}	
	document.frm.submit();
}

// -->
</script>


  <table width="100%" height="361" border="0" align="center" cellpadding="0" cellspacing="0">
    <tbody><tr> 
      <td align="center" valign="top"> 
 

<table width="700" border="0" cellspacing="0" cellpadding="0" style="border:solid 2px #92ced9;">
  <tbody><tr>
    <td align="center" style="background:#92ced9; padding-top:20px; padding-bottom:20px;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center" style="padding-bottom:15px;"><span class="ht_22 ct_b bt_60">비밀번호 찾기</span></td>
  </tr>
  <tr>
    <td align="center">
<span class="ht_13 ct_b bt_50">
<c:choose>
<c:when test="${password eq null }">
입력하신 정보와 일치하는 회원정보가 없습니다.<br>
확인후 다시 시도해주시기 바랍니다.
</c:when>

<c:otherwise>
변경할 PW를 입력하세요
</c:otherwise>
</c:choose>

</span>
    </td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr>
    <td height="5" style="background:url(//cdn.dscount.com/images_2016/member/id_bg.png) repeat-x;"></td>
  </tr>
  <tr>
    <td align="center" style="padding-top:40px; padding-bottom:40px;">
<form method="Post" action="${ctx}/modifyPw.do" name="frm">
<input type="hidden" value="${id }" name="id">
<input type="hidden" value="${password }" name="password">
<input type="hidden" value="N" name="modi">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
    
    <c:choose>
    
    <c:when test="${password eq null }">
    	입력된 정보가 다릅니다
    </c:when>
    
    <c:otherwise>
    <table border="0" cellspacing="0" cellpadding="0">
  <tbody>
  <tr>
    <td height="12" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td><span class="ht_15 ct_b bt_40">비밀번호</span></td>
    <td style="padding-left:20px;"><input type="password" name="pass" style="border: 1px solid #d9d9d9; color: #616161; FONT-FAMILY: 돋움; FONT-SIZE: 13px; width:390px; height:35px; line-height:35px; vertical-align:middle; padding:0px 0px 0px 10px;"></td>
  </tr>
  <tr>
    <td height="12" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td><span class="ht_15 ct_b bt_40">비밀번호 확인</span></td>
    <td style="padding-left:20px;"><input type="password" name="passchk" style="border: 1px solid #d9d9d9; color: #616161; FONT-FAMILY: 돋움; FONT-SIZE: 13px; width:390px; height:35px; line-height:35px; vertical-align:middle; padding:0px 0px 0px 10px;"></td>
  </tr>
</tbody></table>
    </c:otherwise>
    </c:choose>


    </td>
  </tr>
  <tr>
    <td align="center" style="padding-top:30PX; padding-bottom:30PX;"><a href="javascript:FindPass();"><img src="${ctx}/skin/images/m_09.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td align="center"><span class="ht_13 ct_b bt_50">※ 비밀번호 찾기가 되지 않는 경우에는 고객센터(1577-6654)로 문의해 주시기 바랍니다.</span></td>
  </tr>
</tbody></table>
</form>
    
    </td>
  </tr>
</tbody></table>
          
      </td>
    </tr>
  </tbody></table>
</center>

</body></html>