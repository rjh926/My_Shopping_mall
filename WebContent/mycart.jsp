<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<!-- saved from url=(0038)http://pay.dscount.com/cart/mycart.asp -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>장바구니</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/AceCounter_AW.js.다운로드"></script><script async="" src="${ctx}/skin/js/analytics.js.다운로드"></script><script src="${ctx}/skin/js/top_javascript.js.다운로드"></script><script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script><script src="${ctx}/skin/js/jquery.cookie.js.다운로드"></script><script src="${ctx}/skin/js/cookie.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.bpopup.min.js.다운로드"></script>


<script src="${ctx}/skin/js/f.txt"></script><script src="${ctx}/skin/js/f(1).txt"></script><script src="${ctx}/skin/js/f(2).txt"></script></head><body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><form name="baseinfo" id="baseinfo" method="post">
	<input type="hidden" name="MinDelivery" id="MinDelivery" value="50000">
	<input type="hidden" name="eventMinDelivery" id="eventMinDelivery" value="50000">
	<input type="hidden" name="DeliveryFee" id="DeliveryFee" value="2500">	
</form>

<script language="JavaScript">
<!--
function DelCartAll()
{
	if (confirm("장바구니에 담긴 모든 상품이 삭제됩니다. 장바구니를 비우시겠습니까?"))
	{
		document.location.href="DelCartAll.asp";
	}
}

function DelCartItem(pId)
{
	if (confirm("선택하신 상품을 장바구니에서 삭제하시겠습니까?"))
	{
		document.location.href="${ctx}/myCart/removeItem.do?pId="+pId;
	}
}

function Cart2ConcernItem(cartseq)
{
	
	alert("로그인을 해주시기 바랍니다.");
	document.location.href="http://member.dscount.com/member/LoginForm.asp";
	

}

function checkStock(str)
{
	intGqty = str.value;
	intGcnt = str.id;
	
	if (eval(intGcnt-intGqty)<0)
	{
		alert("최대 주문 가능 수량은 " + intGcnt + "개  입니다");
		str.value = intGcnt;
	}
}

function goPrevPage()
{
	history.go(-2)
}


function updateNormal()
{
	document.cartNormal.action="EditCart.asp"
	document.cartNormal.submit();
}

function updateDelay()
{

	document.CartDelay.action="EditCart.asp"
	document.CartDelay.submit();
}

function formatNumber(str)
{
	number = numOffMask(str.value);

	if (isNaN(number))
		str.value = "";
	else
		str.value = numOnMask(number);
}

function numOffMask(str)
{
	var tmp = str.split(",");
	tmp = tmp.join("");
	return tmp;
}

function numOnMask(str)
{

	if (str < 0) {
		str = Math.abs(str);
		sign = "-";
	} else {
		sign = "";
	}

	str = str + "";
	var idx = str.indexOf(".");

	if (idx < 0) {
		var txtInt = str;
		var txtFloat = "";
	} else {
		var txtInt = str.substring(0,idx);
		var txtFloat = str.substring(idx);
	}

	if (txtInt.length > 3) {
		var c=0;
		var myArray = new Array();
			for(var i=txtInt.length; i>0; i=i-3) {
			myArray[c++] = txtInt.substring(i-3,i);
 			}
		myArray.reverse();
 		txtInt = myArray.join(",");
 		}
 		str = txtInt + txtFloat;

	return sign + str;
}

function SelectedCartNormalDel()
{
	if ( $("#dumyNormal").length > 0 ) 
	{
		var f = document.cartNormal;

		var k=0

		if (f.checkcart.length==undefined) // 한개일 경우..
		{  
			if (f.checkcart.checked==true)
				k++;
		}
		else // 여러개일경우
		{
			for(i=0;i<f.checkcart.length;i++)
			{
				if (f.checkcart[i].checked)
					k++;
			}
		}

		if (k==0)
		{
			alert("삭제하실 상품을 선택해주세요.");
		}
		else
		{
			if (confirm("선택하신 상품을 삭제하시겠습니까?"))
			{
				f.action="DelCartSelected.asp"
				f.submit();
			}
		}  
	}
	else
	{
		alert("장바구니가 비어있습니다");
	}


}

function SelectedCartDelayDel()
{
	var f = document.CartDelay;

	var k=0

	if (f.checkcart.length==undefined) // 한개일 경우..
	{  
		if (f.checkcart.checked==true)
			k++;
	}
	else // 여러개일경우
	{
		for(i=0;i<f.checkcart.length;i++)
		{
			if (f.checkcart[i].checked)
				k++;
		}
	}

	if (k==0)
	{
		alert("삭제하실 상품을 선택해주세요.");
	}
	else
	{
		if (confirm("선택하신 상품을 삭제하시겠습니까?"))
		{
			f.action="DelCartSelected.asp"
			f.submit();
		}
	}  


}

function num_only( Ev )
{
	if (window.event) // IE코드
		var code = window.event.keyCode;
	else // 타브라우저
		var code = Ev.which;

	if ((code > 34 && code < 41) || (code > 47 && code < 58) || (code > 95 && code < 106) || code == 8 || code == 9 || code == 13 || code == 46)
	{
		window.event.returnValue = true;
		return;
	}

	if (window.event)
		window.event.returnValue = false;
	else
		Ev.preventDefault();	
}


function layerPopWishListOnOff(nVal)
{
	if (nVal==0)
	{
		document.all['layerPopOption'].style.display = "none"; 
	}
	else
	{
		document.all['layerPopOption'].style.display = ""; 
	}
}

function GetOptionSize(seq)
{
	$("#div_cart_"+seq+"_size").load("cart_selectgoodcolor.asp?seq="+seq+"&gcolor="+escape(encodeURIComponent($("#Cart_"+seq+"_Color").val())) );
}

function DeleteCartGood(seq)
{
	document.location.href="DelCart.asp?seq="+seq;
}

// -->
</script>


<!-- Normal 카트 스크립트 시작 -->
<script type="" language="JavaScript">
<!--
function layerApplyOption(seq)
{	
	Gcolor = $("#Cart_"+seq+"_Color").val();
	Gsize = $("#Cart_"+seq+"_Size").val();

	if (Gsize=="")
	{
		alert("사이즈를 선택해주세요.");
		return;
	}
	document.location.href="CartUpdate.asp?seq="+seq+"&Gcolor=" + escape(encodeURIComponent(Gcolor)) + "&Gsize=" + escape(encodeURIComponent(Gsize));
}


function allcheck()
{
	$('input:checkbox[name=seq]').each(function(){
		$(this).prop('checked', true);
	});
}	

function disallcheck()
{
	$('input:checkbox[name=seq]').each(function(){
		$(this).prop('checked', false);
	});
}	


function setAllCheckNormal()
{
	if ( $("#allcheck1").prop("checked")==true )
	{
		$(".checkcart1").each(function() {
			$(this).prop('checked', true);
		});
	}
	else
	{
		$(".checkcart1").each(function() {
			$(this).prop('checked', false);
		});
	}

	ComputeGamountNormal();

}	

function ComputeGamountNormal()
{
	
	}
}

function CheckCartNormalSeq()
{
	var argSeq=0;
	var temp="";
	var argCnt = 0;
	
	if ( $("#dumyNormal").length > 0 ) {
		document.dumyNormal.cartseq.value = "";

		if (document.cartNormal.checkcart.length)
		{
			for (var i=0; i<document.cartNormal.checkcart.length ; i++)
			{
				if(document.cartNormal.checkcart[i].checked)
				{
					temp =document.cartNormal.checkcart[i].value;
					array_data = temp.split("/");
					argSeq = array_data[3];

					if (argCnt==0)
						document.dumyNormal.cartseq.value = argSeq;
					else
						document.dumyNormal.cartseq.value = document.dumyNormal.cartseq.value+','+argSeq;

					argCnt++;

				}
			}
		}
		else { 
				if(document.cartNormal.checkcart.checked)
				{
					temp =document.cartNormal.checkcart.value;
					array_data = temp.split("/");
					document.dumyNormal.cartseq.value = array_data[3];
				}
		}
	}
}


//-->	
</script>

<script language="JavaScript">
<!--
function OrderCartNormalAll()
{
	if ( $("#dumyNormal").length > 0 ) 
	{
		var f = document.cartNormal;
		$("#allcheck1").prop("checked", true);
		setAllCheckNormal();

		GoodAmount = numOffMask($("#goodamount").text());

		CheckCartNormalSeq();
		if (eval(GoodAmount > 0))
		{	
			
			document.dumyNormal.action="http://member.dscount.com/Member/LoginForm_non.asp";	
			

			document.dumyNormal.submit();
		}
		else
			alert("장바구니 상품중 선택하신 상품이 없습니다.")
	}
	else
	{
		alert("장바구니가 비어있습니다.")
	}
}

function OrderCartNormal()
{
	if ( $("#dumyNormal").length > 0 ) 
	{
		var f = document.cartNormal;
		GoodAmount = numOffMask($("#goodamount").text());

		CheckCartNormalSeq();

		if (eval(GoodAmount > 0))
		{
			
			document.dumyNormal.action="http://member.dscount.com/Member/LoginForm_non.asp";	
			
			document.dumyNormal.submit();
		}
		else
			alert("장바구니 상품중 선택하신 상품이 없습니다.")
	}
	else
	{
		alert("장바구니가 비어있습니다.")
	}
}

function CartNormal2Concern()
{

	CheckCartNormalSeq();

	if (document.dumyNormal.cartseq.value != "")
	{
		document.dumyNormal.action="../Concern/Cart2Concern.asp";
		document.dumyNormal.submit();
	}
	else
		alert("장바구니 상품중 선택하신 상품이 없습니다.")
}

function DirectCart2Order(cartseq) 
{		
		document.location.href = "/common/Cart/DirectCart2Order.asp?cartseq="+cartseq;
}
// -->
</script>
<!-- Normal 카트 스크립트 종료 -->

<center>


      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td align="center"><script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>


<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>

<script language="javascript">


function gotoItemMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
	
}

function gotoConceptGoodView(Gserial)
{
	location = "../Shopping/GoodView_Concept.asp?Gserial="+Gserial;	
	
}

function gotoCoordiView(Gserial)
{
	location = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	
}

function openCoordiView(Gserial)
{
	url = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
	
}

function gotoItemGood(arg)
{
	location = "../Shopping/ItemShopping_detail.asp?b="+arg;	
	
}

function gotoMDGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoCOSGoodView(Gserial)
{
	location = "../NShopping/GoodView_CItem.asp?Gserial="+Gserial;	
}

function gotoZinifGoodView(Gserial)
{
	location = "../NShopping/GoodView_ZItem.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotoMDGabalGoodView(Gserial, Gseq)
{
	location = "../Shopping/GoodView_Gabal.asp?Gserial="+Gserial+"&gseq=" + Gseq;	
}

function gotoNormalGoodView(Gserial)
{
	location = "../Shopping/GoodView_NItem.asp?Gserial="+Gserial;	
}


function gotoDahongGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
}

function openCatGoodView(Gserial)
{
	url =  "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function gotoBrandGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Brand.asp?Gserial="+Gserial;	
}

function gotoKeywordGoodView(Gserial)
{
	location = "../Nshopping/GoodView_Keyword.asp?Gserial="+Gserial;	
}


function gotoSaleGoodView(Gserial)
{
	location = "../Shopping/GoodView_Sale.asp?Gserial="+Gserial;	
}


function gotoItemGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoSummerGoodView(Gserial)
{
	location = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
}

function gotoCatGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotomonoGoodView(Gserial)
{
	location = "../Shopping2/GoodView_monomori.asp?Gserial="+Gserial;	
}

function gotoCatCoordiView(Gserial)
{
	location = "../Nshopping/GoodView_StyleBook.asp?Gserial="+Gserial;	
	
}


function gotoBigGoodView(Gserial)
{
	location = "../Shopping/GoodView_Big.asp?Gserial="+Gserial;	
}

function gotoItemShoppingMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
}


function gotoBigShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_BigDetail.asp?b="+arg;	
}


function gotoSaleShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_SaleDetail.asp?a="+arg;	
}



function openItemGoodView(Gserial)
{
	url = "/Nshopping/GoodView_Item.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function openDahongGoodView(Gserial)
{
	url = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function openSummerGoodView(Gserial)
{
	url = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function zoomPicture(arg)
{
   target = "../Shopping/zoomPicture.asp?Gserial=" + arg;
   window.open(target,"Picture","status=no,toolbar=no,scrollbars=no,resizable=no,width=780,height=780")
}

function gotoAlert()
{
	alert("경고창")
}

function addBookMark(){
window.external.AddFavorite('http://dahong.dscount.com', '디스카운트 [다홍/지니프/크리마레 통합멤버쉽]')
}


function bookmarksite(title,url) {
	if (window.sidebar) // firefox 
		window.sidebar.addPanel(title, url, ""); 
	else if(window.opera && window.print) // opera 
	{ 
		var elem = document.createElement('a'); 
		elem.setAttribute('href',url); 
		elem.setAttribute('title',title); 
		elem.setAttribute('rel','sidebar'); 
		elem.click(); 
	} 
	else if(window.external && ('AddFavorite' in window.external)) // ie
		window.external.AddFavorite(url, title);
	else if(window.chrome) // chrome
	{ 
		alert('ctrl+D 를 눌러서 북마크에 추가해주세요!'); 
	}
}


</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/images6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  

<script src="${ctx}/skin/js/banners.js.다운로드"></script>
<script src="${ctx}/skin/js/lnb.js.다운로드"></script>




<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->


<script language="javascript">
function div_promotion_onoff(state)
	{
		if (state==1)
		{
			document.getElementById('div_promotion').style.display='';
		}
		else
		{
			document.getElementById('div_promotion').style.display='none';
		}
	}

	function goto_promotion()
	{
		document.location.href='http://dahong.dscount.com/Nshopping/promotion_list.asp';
	}

</script>


    <script type="text/JavaScript">
<!--
function div_my_onoff(state)
{
	if (state==1)
	{
		$("#div_my").css('z-index','3000');
		document.getElementById('div_my').style.display='';
	}
	else
	{
		document.getElementById('div_my').style.display='none';
		$("#div_my").css('z-index','1000');
	}
}

function div_cs_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_cs').style.display='';
	}
	else
	{
		document.getElementById('div_cs').style.display='none';
	}
}

function div_dscountlayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dscountlayer').style.display='';
	}
	else
	{
		document.getElementById('div_dscountlayer').style.display='none';
	}
}


function div_dahonglayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dahonglayer').style.display='';
	}
	else
	{
		document.getElementById('div_dahonglayer').style.display='none';
	}
}

function div_monomorilayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_monomorilayer').style.display='';
	}
	else
	{
		document.getElementById('div_monomorilayer').style.display='none';
	}
}


function div_secondleedslayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_secondleedslayer').style.display='';
	}
	else
	{
		document.getElementById('div_secondleedslayer').style.display='none';
	}
}

function div_ziniflayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_ziniflayer').style.display='';
	}
	else
	{
		document.getElementById('div_ziniflayer').style.display='none';
	}
}

function div_creemarelayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_creemarelayer').style.display='';
	}
	else
	{
		document.getElementById('div_creemarelayer').style.display='none';
	}
}

function searchform_act()
{
	if (document.getElementById("layersearchform").style.display == 'none')
	{
		document.getElementById("layersearchform").style.display = '';
	}
	else
	{
		document.getElementById("layersearchform").style.display ='none';
	}
}

function div_menu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_menu').style.display='';
	}
	else
	{
		document.getElementById('div_menu').style.display='none';
	}
}

function div_allmenu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_allmenu').style.display='';
	}
	else
	{
		document.getElementById('div_allmenu').style.display='none';
	}
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>



<!-- 상단 head -->
<%@include file="header.jsp" %>
<!-- 상단 head -->
<!-- 타이틀 -->

<table width="1185" height="140" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" valign="top" style="padding:50px 0px 0px 0px;"><span class="et_30 ct_b bt_50">MY CART</span></td>
    <td align="right" valign="top" style="padding:30px 0px 0px 0px;"><img src="${ctx}/skin/images/obar_01.jpg"></td>
  </tr>
</tbody></table>

<!-- 타이틀 -->
      
<!-- 장바구니 내용테이블 시작-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td align="center" valign="top">

    <!-- 일반 장바구니 테이블 시작-->

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">

<form name="dumyNormal" id="dumyNormal" method="post">
  <input type="hidden" name="deliveryfee" value="2500">
  <input type="hidden" name="kind" value="1">
  <input type="hidden" name="cartseq" value="">
  <input type="hidden" name="mode" value="C">
  <input type="hidden" name="ExpressDelivery" value="N">
  <input type="hidden" name="CartCnt" value="1">   

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style=" padding:0px 0px 15px 0px;"><span class="ht_18 ct_b bt_50">주문상품 (${cartsNum})</span></td>
  </tr>
  <tr>
    <td align="center" style="background:#f3f3f3;padding:17px 0px 17px 0px; border-top:solid 1px #dedede; border-bottom:solid 1px #dedede;">
      <table width="1185" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
         <td align="center" width="45"><label><input type="checkbox" name="allcheck" id="allcheck1" value="T" checked="" onclick="setAllCheckNormal();"></label></td>                                
         <td align="center" width="500"><span class="ht_13 bt_50">상품명</span></td> 
         <td align="center" width="210"><span class="ht_13 bt_50">판매가</span></td>                                
         <td align="center" width="135"><span class="ht_13 bt_50">수량</span></td>                                
         <td align="center" width="135"><span class="ht_13 bt_50">적립금</span></td>                               
         <td align="center" width="160"><span class="ht_13 bt_50">선택</span></td>                                
        </tr>
      </tbody></table>
     </td>
   </tr>
</tbody></table>
</form>
  
  
 <form name="cartNormal" id="cartNormal" method="post">  
 <!-- 상품 루프 시작-->
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  
  <tbody>
  
  <c:choose>
  <c:when test="${carts eq null || empty carts }">
  <tr>
  <td colspan="6" align="center"><h3> 등록된 상품이 없습니다.</h3></td>
  </tr>
  </c:when>
 <c:otherwise>
 
 <c:forEach items="${carts }" var="cart">
  <tr>
    <td style="border-bottom:solid 1px #dedede;">
    
<div style="position:relative; width:1185px;">
<div style="position:absolute;right:15px;top:15px;"><a href="javascript:DelCartItem(${cart.product.productId})"><img src="${ctx}/skin/images/c_04.png" border="0"></a></div>
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center" width="45">
	
<input type="hidden" name="seq" value="27891400">
    <input type="checkbox" name="checkcart" value="19800/0/1/27891400/N/1" checked="" onclick="ComputeGamountNormal();" class="checkcart1">
	
    </td>
    <td align="left" width="500" style="border-right:solid 1px #f0f0f0; padding:15px 0px 15px 10px;">

<!-- 상품옵션테이블-->
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-right:20px;"><img src="${ctx }/skin/images/${cart.product.image}" width="80px" onclick="JavaScript:GetGoodUrl(1,721131)" style="cursor:pointer;cursor:hand;"></td>
    <td align="left" valign="middle">
		<table border="0" cellspacing="0" cellpadding="0">

		  <tbody><tr>
			<td align="left" style="padding-bottom:10px;"><img src="${ctx}/skin/images/sn_icon_04.gif" border="0"></td>
		  </tr>
		
		  <tr>
			<td align="left"><span class="ht_14 ct_b bt_50">${cart.product.productName }</span></td>
		  </tr>
		
		  <tr>
			<td align="left" style="padding-top:15px;">

		<!-- 재고 있는 상품-->
		<table border="0" cellspacing="0" cellpadding="0">
		  <tbody><tr>
			<td style="padding-right:15px;"><span class="ht_13 ct_br">${cart.product.color } /${cart.product.productSize }</span></td>
		
			<td><a href="javascript:layerPopOpen(27891400);"><img src="${ctx}/skin/images/c_01.jpg" border="0"></a></td>
		
		  </tr>
		</tbody></table>
		<!-- 재고 있는 상품-->

    </td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
<!-- 상품옵션테이블-->
    </td>
    <td align="center" width="210" style="border-right:solid 1px #f0f0f0;">
	<span class="m_title07">	
	
	<span class="ht_13 ct_b">${cart.product.price }원</span> 
	
	</span>
    </td>
    <td align="center" width="135" style="border-right:solid 1px #f0f0f0;">
  
<!-- 수량 체크 박스-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
		
        <td align="center">
		  <table border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td width="25"><a href="${ctx}/myCart/amountModify.do?pId=${cart.product.productId}&amount=${cart.amount}&cal=subtract"><img src="${ctx}/skin/images/cart_minus.gif" style="cursor:hand"></a></td>
              <td width="23" style="padding-left:5px; padding-right:5px;"><input type="text" name="gqty_1" value="${cart.amount }" id="999" style="IME-MODE: disabled; height:28px; width:27px; border:solid 1px #a9a9a9; padding:0px 0px 0px 0px; text-align:center; vertical-align:middle;" onkeydown="num_only(event)"></td>
              <td align="right" width="25"><a href="${ctx}/myCart/amountModify.do?pId=${cart.product.productId}&amount=${cart.amount}&cal=add">
				<img src="${ctx}/skin/images/cart_plus.gif" style="cursor:hand" onclick="chg_cart_ea(cartNormal.gqty_1,up);updateNormal();"></a>			  </td>
            </tr>
          </tbody></table>		 
          </td>

		
      </tr>
    </tbody></table>
<!-- 수량 체크 박스-->
         
<!-- 품절 시작-->

    </td>
    <td align="center" width="135" style="border-right:solid 1px #f0f0f0;">
<span class="ht_13 ct_b">${cart.product.price /100 }원</span>
    </td>
    <td width="160" align="center">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><a href="javascript:DirectCart2Order(27891400)"><img src="${ctx}/skin/images/c_02.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td style="padding-top:5px;"><a href="javascript:Cart2ConcernItem(27891400)"><img src="${ctx}/skin/images/c_03.jpg" border="0"></a></td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
</div>
    
    </td>
  </tr>
  
  </c:forEach>
  </c:otherwise>
</c:choose> 
  
</tbody></table>
<!-- 상품 루프 종료-->

					<!-- 장바구니 비었을때-->
                    <div id="div_cart_normal_info_2" style="display:none" align="center">
                    <table width="1185" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
  					<td style="padding-top:50px; padding-bottom:50px; border:solid 1PX #dedede;" align="center"><span class="ht_15">※ 장바구니에 선택한 상품이 없습니다.</span></td>
  				    </tr>
					</tbody></table>
                    </div> 
					<!-- 장바구니 비었을때-->
					<!-- 당일배송 상품 주문시-->
					<div id="div_cart_normal_info_3" style="display:none" align="center">
                    <table width="1185" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
  					<td style="padding-top:30px; padding-bottom:30px; background:#f3f3f3;" align="center"><span class="ht_13 ct_p2">※ 선택하신 상품들은 당일발송 주문 가능합니다.</span></td>
  				    </tr>
					</tbody></table>
                     </div>
					<!-- 당일배송 상품 주문시-->
					<!-- 일반배송+당일배송 상품 주문시-->
					<div id="div_cart_normal_info_4" style="display:none" align="center"> 
                    <table width="1185" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
  					<td style="padding-top:30px; padding-bottom:30px; background:#f3f3f3;" align="center"><span class="ht_13 ct_p2">※ 당일발송 / 일반배송을 함께 주문하시면 일반배송으로 처리됩니다.</span></td>
  				    </tr>
					</tbody></table>
                    </div>
					<!-- 일반배송+당일배송 상품 주문시--> 
<!-- 상품 금액 계산-->
	<div id="div_cart_normal_info_1" style="display:"> 
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding-top:20px; padding-bottom:30px;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-right:8px;cursor:pointer;cursor:hand;" onclick="javascript:SelectedCartNormalDel();"><img src="${ctx}/skin/images/c_06.jpg"></td>
    <td style="cursor:pointer;cursor:hand;" onclick="javascript:DelCartAll();"><img src="${ctx}/skin/images/c_07.jpg"></td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr>
    <td style="border:solid 1px #3f4248; padding-top:40px; padding-bottom:40px;" align="center">
	<!--
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="26"><span class="ht_16 ct_g10">판매금액</span><input type="text" name="goodamount" readonly value="19,800" style="font-family:'Nanum Gothic', sans-serif;font-size:18px;font-weight:600;text-align:right; vertical-align:middle;border:0px; padding:0px 0px 0px 0px; width:110px;"><span class="ht_18">원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="//cdn.dscount.com/images_2016/cart/c_12.png" /></td>
    <td><span class="ht_16 ct_g10">할인금액</span><input type="text" name="totsaleamount" size="8" readonly value="0" style="font-family:'Nanum Gothic', sans-serif;font-size:18px;color:#f92b82;font-weight:600;text-align:right; vertical-align:middle;border:0px; padding:0px 0px 0px 0px; width:110px;"><span class="ht_18 ct_p3">원</span></td>
        <td style="padding-left:30px; padding-right:30px;"><img src="//cdn.dscount.com/images_2016/cart/c_11.png" /></td>
    <td><span class="ht_16 ct_g10">배송비</span><input type="text" name="deliveryfee" readonly value="2,500" style="font-family:'Nanum Gothic', sans-serif;font-size:18px;font-weight:600;text-align:right; vertical-align:middle;border:0px; padding:0px 0px 0px 0px; width:110px;"><span class="ht_18">원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="//cdn.dscount.com/images_2016/cart/c_13.png" /></td>
    <td><span class="ht_16 ct_g10">결제예정금액</span><input type="text" name="settleamount" size="8" readonly value="22,300" style="font-family:'Nanum Gothic', sans-serif;font-size:18px;font-weight:600;text-align:right; vertical-align:middle;border:0px; padding:0px 0px 0px 0px; width:110px;"><span class="ht_18">원</span></td>
  </tr>
</table>
-->
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="26"><span class="ht_16 ct_g10" style="padding-right:20px;">판매금액</span><span class="ht_18"><b><span id="goodamount">${totalPrice }</span></b>원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_12.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">할인금액</span><span class="ht_18 ct_p3"><b><span id="totsaleamount">0</span></b>원</span></td>
        <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_11.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">배송비</span><span class="ht_18"><b><span id="deliveryfee">2,500</span></b>원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_13.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">결제예정금액</span><span class="ht_18"><b><span id="settleamount">${totalPrice+2500 }</span></b>원</span></td>
  </tr>
</tbody></table>
    </td>
</tr>
</tbody></table>

    </div>
<!-- 상품 금액 계산-->

  </form>
  

 
  
<!-- 버튼 테이블 시작-->
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center" style="padding-top:70px;">
    
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-right:10px; cursor:pointer;cursor:hand;"> <img src="${ctx}/skin/images/c_08.jpg" border="0"></td>
    <td style="padding-right:10px; cursor:pointer;cursor:hand;" onclick="javascript:OrderCartNormal();"><img src="${ctx}/skin/images/c_09.jpg" border="0"></td>
    <td style="cursor:pointer;cursor:hand;" onclick="javascript:OrderCartNormalAll();"><img src="${ctx}/skin/images/c_10.jpg" border="0"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>

<!-- 버튼 테이블 종료-->

    
    </td>
  </tr>
  <tr>
  <td height="70"></td>
  </tr>
</tbody></table>
    <!-- 일반 장바구니 테이블 종료-->


<!--이벤트배너 종료    
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" style="padding-bottom:30px;"><a href="http://creemare.dscount.com/" onfocus="this.blur();"><img src="//cdn.dscount.com/images_2016/shopping/20170602_cart_event.jpg" border="0"></a></td>
  </tr>
</table>   
--> 
    
<table width="1185" border="0" cellspacing="0" cellpadding="0" style="border:1 solid #dfdfdf;">
  <tbody><tr>
   <td align="left">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tbody><tr>
       <td height="20"><span class="ht_13">※ 장바구니에는 5일간 보관을 원칙으로 하며, 주문 후 유효한 결제시간이 지날 경우 구매내역이 자동삭제됩니다.</span></td>
     </tr>
     <tr>
       <td height="20"><span class="ht_13">※ 쿠폰사용은 로그인 후 구매과정에서 적용되며, 무료배송은 5만원이상 구매시 적용됩니다.</span></td>
     </tr>
     <tr>
       <td height="20"><span class="ht_13">※ 비회원으로 구매하시면 적립금 , 할인쿠폰이 지급되지 않습니다.</span> </td>
     </tr>
   </tbody></table>
   </td>
  </tr>
  <tr>
    <td height="150"></td>
  </tr>
</tbody></table>

    
    


              </td>
            </tr>
          </tbody></table> 
<!-- 장바구니 내용테이블 종료-->       

<!-- 스크립트 시작 bottom 위에 있어야 함 -->    

<!-- 에이스카운트 시작 -->
<!-- *) 장바구니목록페이지 (장바구니보기 ex:shop-cart-info.php)
- 장바구니 Cart_In제품정보시작(Loop) start -
i=0 부터 시작합니다.
 -->

<!-- AceCounter eCommerce (Cart_Inout) v7.5 Start -->
<!-- Function and Variables Definition Block Start -->
<script language="javascript">
var _JV="AMZ2017020801";//script Version
var _UD='undefined';var _UN='unknown';
var _ace_countvar = 0;
function _IDV(a){return (typeof a!=_UD)?1:0}
var _CRL='http://'+'gtc19.acecounter.com:8080/';
var _GCD='AS4A41393170138';
if( document.URL.substring(0,8) == 'https://' ){ _CRL = 'https://gtc19.acecounter.com/logecgather/' ;};
if(!_IDV(_A_i)) var _A_i = new Image() ;if(!_IDV(_A_i0)) var _A_i0 = new Image() ;if(!_IDV(_A_i1)) var _A_i1 = new Image() ;if(!_IDV(_A_i2)) var _A_i2 = new Image() ;if(!_IDV(_A_i3)) var _A_i3 = new Image() ;if(!_IDV(_A_i4)) var _A_i4 = new Image() ;
function _RP(s,m){if(typeof s=='string'){if(m==1){return s.replace(/[#&^@,]/g,'');}else{return s.replace(/[#&^@]/g,'');} }else{return s;} };
function _RPS(a,b,c){var d=a.indexOf(b),e=b.length>0?c.length:1; while(a&&d>=0){a=a.substring(0,d)+c+a.substring(d+b.length);d=a.indexOf(b,d+e);}return a};
function AEC_F_D(pd,md,cnum){var i=0,amt=0,num=0;var cat='',nm='';num=cnum;md=md.toLowerCase();if(md=='b'||md=='i'||md=='o'){for(i=0;i<_A_pl.length;i++){if(_A_pl[i]==pd){nm=_RP(_A_pn[i]);amt=(parseInt(_RP(_A_amt[i],1))/parseInt(_RP(_A_nl[i],1)))*num;cat=_RP(_A_ct[i]);var _A_cart=_CRL+'?cuid='+_GCD;_A_cart+='&md='+md+'&ll='+_RPS(escape(cat+'@'+nm+'@'+amt+'@'+num+'^&'),'+','%2B');break;};};if(_A_cart.length>0)_A_i.src=_A_cart+"rn="+String(new Date().getTime());setTimeout("",2000);};};
function AEC_D_A(){ var i = 0,_AEC_str= ''; var ind = 0; for( i = 0 ; i < _A_pl.length ; i ++ ){ _AEC_str += _RP(_A_ct[i])+'@'+_RP(_A_pn[i])+'@'+_RP(_A_amt[i],1)+'@'+_RP(_A_nl[i],1)+'^'; if(  escape(_AEC_str).length > 800 ){ if(ind > 4) ind = 0; _AEC_str = _RPS(escape(_AEC_str),'+','%2B')+'&cmd=on' ; AEC_S_F(_AEC_str , 'o', ind) ; _AEC_str = '' ; ind++; }; }; if( _AEC_str.length > 0 ){ if(ind+1 > 4) ind = 0; AEC_S_F(_RPS(escape(_AEC_str),'+','%2B'), 'o', ind+1) ; }; };
function AEC_B_A(){var i=0,_AEC_str='',_A_cart='';var ind = 0;_A_cart = _CRL+'?cuid='+_GCD+'&md=b';for( i = 0 ; i < _A_pl.length ; i ++ ){ _AEC_str += ACE_REPL(_A_ct[i])+'@'+ACE_REPL(_A_pn[i])+'@'+ACE_REPL(_A_amt[i],1)+'@'+ACE_REPL(_A_nl[i],1)+'^';if(escape(_AEC_str).length > 800 ){if(ind > 4) ind = 0;_AEC_str = _RPS(escape(_AEC_str),'+','%2B')+'&cmd=on';AEC_S_F(_AEC_str,'b',ind); _AEC_str = '' ;ind++;};}; if( _AEC_str.length > 0 ){if(ind+1 > 4) ind = 0; AEC_S_F(_RPS(escape(_AEC_str),'+','%2B'),'b',ind+1);};};
function AEC_U_V(pd,bnum){ var d_cnt = 0 ; var A_amt = 0 ; var A_md = 'n' ;var _AEC_str = '' ; for( j = 0 ; j < _A_pl.length; j ++ ){ if( _A_pl[j] == pd ){ d_cnt = 0; if( _A_nl[j] != bnum ){ d_cnt = bnum - parseInt(_RP(_A_nl[j],1)) ; A_amt = Math.round( parseInt(_RP(_A_amt[j],1)) / parseInt(_RP(_A_nl[j],1))); if( d_cnt > 0 ){ A_md = 'i' ; }else{ A_md = 'o' ;};_A_amt[j] = A_amt*Math.abs(d_cnt) ; _A_nl[j] = Math.abs(d_cnt);_AEC_str += _RP(_A_ct[j])+'@'+_RP(_A_pn[j])+'@'+_RP(_A_amt[j],1)+'@'+_RP(_A_nl[j],1)+'^';};};};if( _AEC_str.length > 0 ){ AEC_S_F(_RPS(escape(_AEC_str),'+','%2B') ,A_md, 0);};};
function AEC_S_F(str,md,idx){var i=0,_A_cart='';var k=eval('_A_i'+idx);if(md=='I')md='i';if(md=='O')md='o';if(md=='B')md='b';if(md=='b'||md=='i'||md=='o'){_A_cart=_CRL+'?cuid='+_GCD;_A_cart+='&md='+md+'&ll='+(str)+'&';k.src=_A_cart+"rn="+String(new Date().getTime());window.setTimeout('',2000);};};
if(!_IDV(_A_pl)) var _A_pl = Array(1) ;
if(!_IDV(_A_nl)) var _A_nl = Array(1) ;
if(!_IDV(_A_ct)) var _A_ct = Array(1) ;
if(!_IDV(_A_pn)) var _A_pn = Array(1) ;
if(!_IDV(_A_amt)) var _A_amt = Array(1) ;
</script>
<!-- Function and Variables Definition Block End-->

<!-- AceCounter eCommerce (Product_Detail) v7.5 Start -->
<!-- Data Allocation (Product_Detail) -->
<script language="javascript">

	_A_amt[0]='19800';
	_A_nl[0]='1';
	_A_pl[0]='721131';
	_A_pn[0]='베이직 오버핏 셔츠';
	_A_ct[0]='SHIRTS';

</script>
<!-- 에이스카운트 종료 -->


<div id="layerPopOption" style="position:relative; width:455px; height:300px; z-index:200; display:none;"></div>

     
<script type="" language="JavaScript">
<!--

function layerPopOpen(seq)
{
	url = "Cart_Option.asp?seq=" + seq;
	$('#layerPopOption').bPopup({
		position: ['auto', 'auto'],
		content:'iframe',
		iframeAttr:"name='hidden_iframe' id='hidden_iframe' frameborder='0' width='100%' height='100%'",
		loadUrl:url
//		contentContainer:'.scrollable',
	});
}

function layerPopClose()
{
	$('#layerPopOption').bPopup().close();
	$('#layerPopOption').html('');
}



function chg_cart_ea(obj,str,idx)
{

	if (obj.length) obj = obj[idx];

	var step =1;
	var min = 1;
	var max = 99;

	if (isNaN(obj.value) || obj.value == '') {
		alert ("구매수량은 숫자만 가능합니다");
		obj.value=step;
		obj.focus();
	} else {

		var ea = parseInt(obj.value);

		if (str=='up') {
			ea = ea + step
		}
		else if (str == 'set') {
			// nothing to do.
		}
		else {
			ea = ea - step
		}

		if (ea < min) {
			ea = min;
		}
		else if (max && ea > max) {
			ea = max;
		}

		var remainder = ea % step

		if (remainder > 0) {
			ea = ea - remainder;
		}

		if (ea < 0) ea=step;

		obj.value = ea;

	}
}-->	

</script>      
<!-- footer -->
<%@include file="footer.jsp" %>

    </center>


</body></html>