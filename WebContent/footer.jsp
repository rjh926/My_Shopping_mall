<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<table width="100%" border="0"
												cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td style="border-top: solid 2PX #000000;" align="center">
															<table width="1185" border="0" cellspacing="0"
																cellpadding="0">
																<tbody>
																	<tr>
																		<td height="52"
																			style="border-bottom: solid 1PX #878787;">
																			<table width="1185" border="0" cellspacing="0"
																				cellpadding="0">
																				<tbody>
																					<tr>
																						<td align="left" style="padding-left: 18px;">
																							<table border="0" cellspacing="0" cellpadding="0">
																								<tbody>
																									<tr>
																										<td style="cursor: pointer; cursor: hand;">
																										<span class="ht_11 ct_6 bt_40">주문조회</span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;">
																										<span class="ht_11 ct_b bt_60">개인정보 취급방침 </span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;"><span
																											class="ht_11 ct_6 bt_40">이용약관</span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;"><span
																											class="ht_11 ct_6 bt_40">공지사항</span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;">
																										<span
																											class="ht_11 ct_6 bt_40">개인결제</span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;">
																										<span
																											class="ht_11 ct_6 bt_40">채용공고</span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;"><span
																											class="ht_11 ct_6 bt_40">모델지원</span></td>
																										<td
																											style="padding-left: 15px; padding-right: 15px;"><span
																											class="ht_12 ct_g2">|</span></td>
																										<td style="cursor: pointer; cursor: hand;"><span
																											class="ht_11 ct_6 bt_40">제휴문의</span></td>
																										<!-- 크리마레 -->

																										<!-- 크리마레 -->
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td align="left" style="padding-left: 18px;">
																				
																						</td>
																						<td align="right">
																							<table border="0" cellspacing="0" cellpadding="0">
																								<tbody>
																									<tr>
																										<td>
																											<!-- 디스카운트 

 디스카운트 --> <!-- 다홍 --> <!-- 다홍 --> <!-- 모노모리 

 모노모리 --> <!-- 세컨드리즈 sns다시만듬

세컨드리즈 --> <!-- 지니프 -->

																											<table border="0" cellspacing="0"
																												cellpadding="0">
																												<tbody>
																													<tr>
																														<td><a
																															href="https://www.facebook.com/zinif"
																															target="_blank"
																															onclick="_gaq.push([_trackEvent, SNS, Click, Facebook_zinif]);"><img
																																src="${ctx}/skin/images/bottom_sns01.jpg"
																																border="0"></a></td>
																														<td><a
																															href="https://www.instagram.com/zinif_homme/"
																															target="_blank"
																															onclick="_gaq.push([_trackEvent, SNS, Click, Instagram_zinif]);"><img
																																src="${ctx}/skin/images/bottom_sns02.jpg"
																																border="0"></a></td>
																														<td><a
																															href="https://story.kakao.com/ch/zinif"
																															target="_blank"
																															onclick="_gaq.push([_trackEvent, SNS, Click, Kakao_zinif]);"><img
																																src="${ctx}/skin/images/bottom_sns03.jpg"
																																border="0"></a></td>
																													</tr>
																												</tbody>
																											</table> <!-- 지니프 --> <!-- 크리마레 --> <!-- 크리마레 -->
																										</td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																				</tbody>
																			</table>

																		</td>
																	</tr>
																</tbody>
															</table>

														</td>
													</tr>
													<tr>
														<td style="padding-top: 30px; padding-bottom: 40px;"
															align="center">

															<table width="1185" border="0" cellspacing="0"
																cellpadding="0">
																<tbody>
																	<tr>
																		<td colspan="2" align="left" valign="top"
																			style="padding-right: 60px; padding-left: 18px;"><img
																				src="${ctx}/skin/images/logo.PNG" border="0"></td>
																	</tr>
																	<tr>
																		<td align="left" valign="top"
																			style="padding: 25px 0px 0px 16px;">
																			<table border="0" cellspacing="0" cellpadding="0">
																				<tbody>
																					<tr>
																						<td><span class="ht_11 ct_9 tl_20">
																								Linc+웹응용실무<br> 대표자 : 류재형 &nbsp;&nbsp;&nbsp;
																									<br>
																						</span></td>
																					</tr>
																					<tr>
																						<td>
																							<table border="0" cellspacing="0" cellpadding="0">
																								<tbody>
																									<tr>
																										<td style="padding-top: 5px;">
																											<table border="0" cellspacing="0"
																												cellpadding="0">
																												<tbody>
																													<tr>
																														<td style="padding-right: 30px;"><span
																															class="et_13 ct_9 bt_40">bu.ac.kr
																																ⓒ all rights reserved.</span></td>
																														<td><img
																															src="${ctx}/skin/images/bottom_important.jpg"
																															width="200" height="27"></td>
																													</tr>
																												</tbody>
																											</table>
																										</td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																				</tbody>
																			</table>

																		</td>
																		<td align="left" valign="top"
																			style="padding-top: 25px;"><span
																			class="ht_13 bt_60 tl_20"> / 교환반품주소 <br></span>
																			<span class="ht_12 bt_60 tl_20 "> 충남 천안시 동남구 문암로76 백석대학교
																				<br> <br>
																		</span> <span class="ht_13 bt_60 tl_20"> / 콜센터</span> <span
																			class="ht_16 bt_60 tl_20">1111-1111<br></span> <span
																			class="ht_11 ct_3 bt_40 tl_20"> 평일. 10:00 ~
																				17:00 / 점심. 12:00 ~ 13:00 / 주말,공휴일 휴무 </span></td>
																	</tr>
																</tbody>
															</table>

														</td>
													</tr>
													<tr>
														<td align="center" height="50"></td>
													</tr>
												</tbody>
											</table>
</body>
</html>