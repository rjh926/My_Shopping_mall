<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx"> ${pageContext.request.contextPath }</c:set>
<!-- saved from url=(0053)https://member.dscount.com/member/RegisterFormJob.asp -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>회원가입 화면</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script>

    function sample6_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('member_zipcode').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('member_addr1').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('member_addr2').focus();
            }
        }).open();
    }
</script>
 
<script src="${ctx}/skin/js/AceCounter_AW.js.다운로드"></script><script async="" src="${ctx}/skin/js/analytics.js.다운로드"></script><script src="${ctx}/skin/js/top_javascript.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.cookie.js.다운로드">
</script><script src="${ctx}/skin/js/cookie.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.bpopup.min.js.다운로드"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$("#member_id").keyup(function(){
		if($(this).val().length>5){
		var id = $(this).val();
		
		$.ajax({
			type : 'POST',
			url : 'idCheck.do',
			data :
			{
				id : id
			},
			success : function(result){
				if($.trim(result)=='ok'){
					$("#div-memberid").html('사용가능한 ID입니다.');
					$("#id_checked").val('Y');
					
					
				}else{
					$("#div-memberid").html('사용중인 ID입니다');
					$("#id_checked").val('N');
				}
			},
			error : function(){
				alert("에러!");
			}
			
		})
		
		}
		else{
			$("#div-memberid").html("ID는 6자 이상입니다.");
			$("#id_checked").val('N');
		}
		
	});
	
});

</script>
<script language="JavaScript">
<!--
$(document).ready(function() {
	//숫자만 입력
	$(".telnumber").keyup(function(){
		$(this).val($(this).val().replace(/[^0-9]/g,""));
	});

	$('#hidden_iframe').load(function() {
	  $(this)
		.contents()
		.find('#searchAddr')
		.bind('focus', function() {
			//  do stuff
			$('#zipcodeLayer').css({position:'absolute'});
			$(window).scrollTop(0);
		});
	  $(this)
		.contents()
		.find('#searchAddr')
		.bind('blur', function() {
			$('#zipcodeLayer').css({position:'fixed'});
		});
	});
});

function openZipcodelayer(mode)
{
	url = "/lib/zipcode_api.asp?mode=" + mode;
	$('#zipcodeLayer').bPopup({
		content:'iframe',
		iframeAttr:"name='hidden_iframe' id='hidden_iframe' frameborder='0' width='100%' height='100%'",
		loadUrl:url
	});
}

function closeZipcodelayer()
{
	$('#zipcodeLayer').bPopup().close();
	$('#zipcodeLayer').html('');
}



function isValidMemberID()
{

	var objForm = document.member;
	var idReg = /^[a-z0-9]{4,16}$/g;
	if( !idReg.test(objForm.member_id.value)) {
		document.getElementById("div-memberid").innerHTML = "아이디는 영문(소문자)/숫자로 4~16자 이내로 입력해 주세요.";
		return false;
	}
	else
	{
		document.getElementById("div-memberid").innerHTML = "";
		return true;
	}
}

function isValidEmail1()
{
	var objForm = document.member;
	document.getElementById("div-email").innerHTML = "";

	var str_id = $.trim($("#member_email1").val());
	var str_addr = $.trim($("#member_email2").val());	

	// 1. 아이디가 없을때
	// 2. 도메인이 없을때
	// 3. 모두 있을때

	// 1. 아이디가 없을때
	if (str_id=="" || str_addr=="")
	{
		objForm.email_checked.value = "N";
		objForm.member_email.value = "";
	}
	else
	{
		if(str_id != "" && str_id.indexOf("@") > -1)
		{			
			alert("@의 앞부분만 입력해 주시고,\n뒷부분은 선택해 주세요.");
			document.member.member_email1.focus();
			document.member.member_email3.options[0].selected = true; 
			return;		
		}	

		isDupEmail();

		if(objForm.email_checked.value == "Y")
		{
			var mailsum = objForm.member_email1.value + "@" + objForm.member_email2.value;
			objForm.member_email.value = mailsum;
		}
		else
		{
			objForm.email_checked.value = "N";
			objForm.member_email.value = "";
		}
	}

}

function isValidCphone()
{

	var objForm = document.member;
	document.getElementById("div-cphone").innerHTML = "";
	objForm.cphone_checked.value = "N";

}

function isDupPhone()
{
	var objForm = document.member;

	if (trim(objForm.cp_phone1.value) != null)
		{
			if (trim(objForm.cp_phone2.value) == null || objForm.cp_phone2.length < 3) 
			{
				document.getElementById("div-cphone").innerHTML = "이동전화 국번을 3자리 이상 입력해주세요";
				objForm.cp_phone2.focus();
				return ;
			}
		
			if (trim(objForm.cp_phone3.value) == null || objForm.cp_phone3.length < 4) 
			{
				document.getElementById("div-cphone").innerHTML = "이동전화 번호를 4자리 이상 입력해주세요";
				objForm.cp_phone3.focus();
				return ;
			}

			innerJob.location = "Phone_Check.asp?cphone1="+objForm.cp_phone1.value+"&cphone2="+objForm.cp_phone2.value+"&cphone3="+objForm.cp_phone3.value;	 
		}	
	else
	{
		document.getElementById("div-cphone").innerHTML = "통신사를 선택해주세요";
		objForm.cp_phone1.focus();
		return ;
	}
}

function isDupEmail()
{
	var objForm = document.member;
//	innerJob.location = "Email_Check.asp?email="+objForm.member_email.value;	 
	innerJob.location = "Email_Check.asp?email="+objForm.member_email1.value + "@" + objForm.member_email2.value;

}

function ID_Check()
{
	var objForm = document.member;


	if (!isValidMemberID()) 
	{
		objForm.member_id.focus();
//      objForm.member_id.value="";
		document.getElementById("div-memberid").innerHTML = "아이디는 영문(소문자)/숫자로 4~16자 이내로 입력해 주세요.";
     }		
	 else
	 {	
		
		window.open("${pageContext.request.contextPath}//idCheck.do?id="+objForm.member_id.value,"중복검사",'location=no,toolbar=no,width=200,height=150,resizable=no');
		
		var id = $("#member_id")
	 }

}

function isValidPassword()
{

	var objForm = document.member;
	var idReg = /^[a-z]+[a-z0-9]{5,15}$/;


	if( !idReg.test(objForm.member_pass.value)) {
		document.getElementById("div-password1").innerHTML = "비밀번호는 영문자로 시작하는 6~16자 영문자 또는 숫자이어야 합니다.";
//		objForm.member_pass.value=""
		return false;
	}
	else
	{
		document.getElementById("div-password1").innerHTML = ""
		return true;
	}

}

function confirmPassword()
{
	var objForm = document.member;
	if(objForm.member_pass.value != objForm.member_repass.value) 
	{
		document.getElementById("div-password2").innerHTML = "비밀번호와 비밀번호 확인란의 값이 틀립니다. 다시 한번 입력해 주세요.";
//		objForm.member_pass.value=""
//		objForm.member_repass.value=""
//		objForm.member_pass.focus();
		return false;
	}
	else
	{
		document.getElementById("div-password2").innerHTML = ""
		return true;
	}
}

function Guest2Member_Phone() 
{
	var objForm = document.member;
				
	innerJob.location="GetGuestInfoPhone.asp?guest_name="+objForm.guest_name.value+"&guest_cphone="+objForm.guest_cphone.value;
}


function email_popup() {
  	var urlname = "popup_email.asp?Email_ID="+document.member.member_email1.value;
	addr_etc = window.open(urlname, '',"top=100px,left=100px,status=no,resizable=no,menubar=no,scrollbars=no");
	addr_etc.focus();
}


function mailAddr()
{  
	var str_id = $.trim($("#member_email1").val());
	var str_addr = $.trim($("#member_email3").val());	

	if( str_id == "" )
	{
		alert("빈칸을 채워주세요.");
		$("#member_email1").focus();
		$("#member_email3 option:eq(0)").prop("selected", "selected");
		return;
	}

	// 직접입력일때와 아닐때로 구분해서 처리.
	if ( str_addr != 'etc' )
	{
		$("#member_email2").val(str_addr);
		$("#member_email").val(str_id+"@"+str_addr);
		isDupEmail();
	}
	else // 직접입력
	{
		$("#email_checked").val("N");
		$("#member_email").val("");
		$("#div-email").html("");
		$("#member_email2").prop("readonly", false);
		$("#member_email2").val("");
		$("#member_email2").focus();
	}

/*
	if(str_id == "" && str_addr != ""){
		alert("빈칸을 채워주세요.");
		$("#member_email1").focus();
		$("#member_email3 option:eq(1)").prop("selected", "selected");
		return;
	}else if(str_id != "" && str_id.indexOf("@") > -1){			
		alert("@의 앞부분만 입력해 주시고,\n뒷부분은 선택해 주세요.");
		document.member.member_email1.focus();
		document.member.member_email2.options[0].selected = true; 
		return;		
	}	
	if ( str_addr == "etc" ){
		 email_popup();	
	}else {
		var mailsum = document.member.member_email1.value + "@" + document.member.member_email2.value;
		document.member.member_email.value = mailsum;
	}	 
*/
//	isDupEmail();
}

function Agreement_All()
{
	if ( $('#agreement_all').is(':checked') )
	{
		$("#agreement1").prop("checked","checked");
		$("#agreement2").prop("checked","checked");
	}
	else
	{
		$("#agreement1").prop("checked","");
		$("#agreement2").prop("checked","");
	}

}		
	
function FormOK() 
{		
	// 회원약관 체크
	if ( !$('#agreement1').is(':checked') )
	{
		alert("회원약관에 동의해 주시기 바랍니다");
		$("#agreement1").focus();
		return ;
	}

	if ( !$('#agreement2').is(':checked') )
	{
		alert("개인정보 수집 및 이용에 동의해 주시기 바랍니다");
		$("#agreement2").focus();
		return ;
	}

	// 개인정보 수집 체크

	// 이름 체크
	if ( $.trim($("#member_name").val()) == "" )
	{
		alert("이름을 입력해 주세요.");
		$("#member_name").focus();
		return ;
	}

	// 아이디 체크
	if ( $.trim($("#member_id").val()) == "" )
	{
		alert("아이디를 입력해 주세요.");		
		$("#member_id").focus();
		return ;
	}

	if ( $.trim($("#id_checked").val()) == "N" )
	{

		alert("아이디가 유효하지 않습니다.");		
		$("#id_checked").focus();
		return ;
	}

	// 비밀번호 체크
	if ( $.trim($("#member_pass").val()) == "" )
	{
		alert("비밀번호를 입력해 주세요.");
		$("#member_pass").focus();
		return ;
	}

	if ( $.trim($("#member_pass").val()) == "" )
	{
		alert("비밀번호를 입력해 주세요.");
		$("#member_pass").focus();
		return ;
	}	

	if ( $.trim($("#member_pass").val()) != $.trim($("#member_repass").val()) )
	{
		alert("비밀번호와 비밀번호 확인란의 값이 틀립니다. 다시 한번 입력해 주세요.");
		$("#member_pass").focus();
		return ;
	}

	// 생년월일 체크
	if ( $.trim($("#birth_year").val()) == "" )
	{
		alert("생년월일을 입력해 주세요.");
		$("#birth_year").focus();
		return ;
	}	

	if ( $.trim($("#birth_month").val()) == "" )
	{
		alert("생년월일을 입력해 주세요.");
		$("#birth_month").focus();
		return ;
	}			

	if ( $.trim($("#birth_day").val()) == "" )
	{
		alert("생년월일을 입력해 주세요.");
		$("#birth_day").focus();
		return ;
	}

	// 우편번호 체크
	if ( $.trim($("#member_zipcode").val()) == "" )
	{
		alert("우편번호를 검색하여 해당주소를 선택해 주세요.");
		$("#member_zipcode").focus();
		return ;
	}

	if ( $.trim($("#member_addr1").val()) == "" )
	{
		alert("우편번호를 검색하여 해당주소를 선택해 주세요.");
		$("#member_addr1").focus();
		return ;
	}
			
	if ( $.trim($("#member_addr2").val()) == "" )
	{
		alert("우편번호를 검색하여 해당주소를 선택해 주세요.");
		$("#member_addr2").focus();
		return ;
	}				
			
	// 이메일주소 체크
	if ( $.trim($("#member_email1").val()) == "" )
	{
		alert(" 이메일 주소를 입력해 주세요.");
		$("#member_email1").focus();
		return ;
	}

	if ( $.trim($("#member_email2").val()) == "" )
	{
		alert(" 이메일 주소를 입력해 주세요.");
		$("#member_email2").focus();
		return ;
	}

	if ( $.trim($("#email_checked").val()) == "" )
	{
		alert("이메일 중복확인이 유효하지 않습니다.");
		$("#email_checked").focus();
		return ;
	}				
				
	

	// 휴대폰 체크
	if ( $.trim($("#cp_phone1").val()) == "" )
	{
		alert("휴대폰 번호를 선택해주세요.");
		$("#cp_phone1").focus();
		return ;
	}

	if ( $.trim($("#cp_phone2").val()) == "" || $.trim($("#cp_phone2").val()).length < 3)
	{
		alert("이동전화 국번을 3자리 이상 입력해주세요");
		$("#cp_phone2").focus();
		return ;
	}

	if ( $.trim($("#cp_phone3").val()) == "" || $.trim($("#cp_phone3").val()).length < 4)
	{
		alert("이동전화 번호를 4자리 이상 입력해주세요");
		$("#cp_phone3").focus();
		return ;
	}

	$("#member").attr("action","/HomeShopping/register.do").submit();

/* 휴대폰 중복확인은 최근 번호이동으로 인한 휴대폰 번호의 교체로 확인할 방법이 없다.
		else if(trim(objForm.cphone_checked.value) == "N") {
			alert("휴대폰 중복확인이 유효하지 않습니다.");		
			objForm.cp_phone1.focus();
			return ;
		}	
*/

}


function FormCancel() 
{
	document.member.id_checked.value = "N";
	document.member.member_guest.value = "N";
	document.member.reset();
	location.href="${ctx}"
}
//-->
</script>


<script src="${ctx}/skin/js/f.txt"></script><script src="${ctx}/skin/js/f(1).txt"></script><script src="${ctx}/skin/js/f(2).txt"></script></head><body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>


  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td align="center">
        <script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>


<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>

<script language="javascript">


function gotoItemMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
	
}

function gotoConceptGoodView(Gserial)
{
	location = "../Shopping/GoodView_Concept.asp?Gserial="+Gserial;	
	
}

function gotoCoordiView(Gserial)
{
	location = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	
}

function openCoordiView(Gserial)
{
	url = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
	
}

function gotoItemGood(arg)
{
	location = "../Shopping/ItemShopping_detail.asp?b="+arg;	
	
}

function gotoMDGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoCOSGoodView(Gserial)
{
	location = "../NShopping/GoodView_CItem.asp?Gserial="+Gserial;	
}

function gotoZinifGoodView(Gserial)
{
	location = "../NShopping/GoodView_ZItem.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotoMDGabalGoodView(Gserial, Gseq)
{
	location = "../Shopping/GoodView_Gabal.asp?Gserial="+Gserial+"&gseq=" + Gseq;	
}

function gotoNormalGoodView(Gserial)
{
	location = "../Shopping/GoodView_NItem.asp?Gserial="+Gserial;	
}


function gotoDahongGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
}

function openCatGoodView(Gserial)
{
	url =  "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function gotoBrandGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Brand.asp?Gserial="+Gserial;	
}

function gotoKeywordGoodView(Gserial)
{
	location = "../Nshopping/GoodView_Keyword.asp?Gserial="+Gserial;	
}


function gotoSaleGoodView(Gserial)
{
	location = "../Shopping/GoodView_Sale.asp?Gserial="+Gserial;	
}


function gotoItemGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoSummerGoodView(Gserial)
{
	location = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
}

function gotoCatGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotomonoGoodView(Gserial)
{
	location = "../Shopping2/GoodView_monomori.asp?Gserial="+Gserial;	
}

function gotoCatCoordiView(Gserial)
{
	location = "../Nshopping/GoodView_StyleBook.asp?Gserial="+Gserial;	
	
}


function gotoBigGoodView(Gserial)
{
	location = "../Shopping/GoodView_Big.asp?Gserial="+Gserial;	
}

function gotoItemShoppingMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
}


function gotoBigShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_BigDetail.asp?b="+arg;	
}


function gotoSaleShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_SaleDetail.asp?a="+arg;	
}



function openItemGoodView(Gserial)
{
	url = "/Nshopping/GoodView_Item.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function openDahongGoodView(Gserial)
{
	url = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function openSummerGoodView(Gserial)
{
	url = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function zoomPicture(arg)
{
   target = "../Shopping/zoomPicture.asp?Gserial=" + arg;
   window.open(target,"Picture","status=no,toolbar=no,scrollbars=no,resizable=no,width=780,height=780")
}

function gotoAlert()
{
	alert("경고창")
}

function addBookMark(){
window.external.AddFavorite('http://dahong.dscount.com', '디스카운트 [다홍/지니프/크리마레 통합멤버쉽]')
}


function bookmarksite(title,url) {
	if (window.sidebar) // firefox 
		window.sidebar.addPanel(title, url, ""); 
	else if(window.opera && window.print) // opera 
	{ 
		var elem = document.createElement('a'); 
		elem.setAttribute('href',url); 
		elem.setAttribute('title',title); 
		elem.setAttribute('rel','sidebar'); 
		elem.click(); 
	} 
	else if(window.external && ('AddFavorite' in window.external)) // ie
		window.external.AddFavorite(url, title);
	else if(window.chrome) // chrome
	{ 
		alert('ctrl+D 를 눌러서 북마크에 추가해주세요!'); 
	}
}


</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/images6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  

<script src="${ctx}/skin/js/banners.js.다운로드"></script>
<script src="${ctx}/skin/js/lnb.js.다운로드"></script>




<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->


<script language="javascript">
function div_promotion_onoff(state)
	{
		if (state==1)
		{
			document.getElementById('div_promotion').style.display='';
		}
		else
		{
			document.getElementById('div_promotion').style.display='none';
		}
	}

	function goto_promotion()
	{
		document.location.href='http://dahong.dscount.com/Nshopping/promotion_list.asp';
	}

</script>


    <script type="text/JavaScript">
<!--
function div_my_onoff(state)
{
	if (state==1)
	{
		$("#div_my").css('z-index','3000');
		document.getElementById('div_my').style.display='';
	}
	else
	{
		document.getElementById('div_my').style.display='none';
		$("#div_my").css('z-index','1000');
	}
}

function div_cs_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_cs').style.display='';
	}
	else
	{
		document.getElementById('div_cs').style.display='none';
	}
}

function div_dscountlayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dscountlayer').style.display='';
	}
	else
	{
		document.getElementById('div_dscountlayer').style.display='none';
	}
}


function div_dahonglayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dahonglayer').style.display='';
	}
	else
	{
		document.getElementById('div_dahonglayer').style.display='none';
	}
}

function div_monomorilayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_monomorilayer').style.display='';
	}
	else
	{
		document.getElementById('div_monomorilayer').style.display='none';
	}
}


function div_secondleedslayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_secondleedslayer').style.display='';
	}
	else
	{
		document.getElementById('div_secondleedslayer').style.display='none';
	}
}

function div_ziniflayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_ziniflayer').style.display='';
	}
	else
	{
		document.getElementById('div_ziniflayer').style.display='none';
	}
}

function div_creemarelayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_creemarelayer').style.display='';
	}
	else
	{
		document.getElementById('div_creemarelayer').style.display='none';
	}
}

function searchform_act()
{
	if (document.getElementById("layersearchform").style.display == 'none')
	{
		document.getElementById("layersearchform").style.display = '';
	}
	else
	{
		document.getElementById("layersearchform").style.display ='none';
	}
}

function div_menu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_menu').style.display='';
	}
	else
	{
		document.getElementById('div_menu').style.display='none';
	}
}

function div_allmenu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_allmenu').style.display='';
	}
	else
	{
		document.getElementById('div_allmenu').style.display='none';
	}
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>




<script language="JavaScript" type="TEXT/JAVASCRIPT">

	function SearchOk() 
	{
		if ( $.trim( $("#textfield").val() ) == "" )
		{
			alert("검색할 상품명을 입력해 주세요"); 
			$("#textfield").focus();
			return;
		}

		document.search.submit();
	}

	function isSerchEnter()
	{
		if (event.keyCode == 13) SearchOk()
	}


</script>
<%@include file="header.jsp" %>

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style=" padding:70px 0px 0px 0px;"><span class="ht_30 ct_b bt_20">회원가입</span></td>
  </tr>
  <tr>
    <td align="left" style="padding:15px 0px 40px 0px;">
    <span class="ht_15 ct_b bt_40 tl_20">환영합니다<br>회원가입 화면입니다. </span>
    </td>
  </tr>
</tbody></table>


<form name="member" id="member" method="post">
<input type="hidden" name="member_guest" id="member_guest" value="N">
<input type="hidden" name="guest_name" id="guest_name" value="">
<input type="hidden" name="guest_cphone" id="guest_cphone" value="">
<input type="hidden" name="id_checked" id="id_checked" value="N">
<input type="hidden" name="email_checked" id="email_checked" value="N">
<input type="hidden" name="cphone_checked" id="cphone_checked" value="N">
<input type="hidden" name="GuestUserID" id="GuestUserID" value="">
<input type="hidden" name="tempiduse" id="tempiduse" value="">
<!-- 약관-->
<table width="1185" border="0" cellspacing="0" cellpadding="0">
<tbody><tr><td height="80" align="left"><span class="ht_18 ct_b bt_60"> 1. 약관 동의</span></td>
</tr>
  <tr>
    <td align="center">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding:30px;" align="center" bgcolor="#f4f4f4">
      <table width="1100" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td align="left" valign="top"><table border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td align="left" style="padding-bottom:10px;"><span class="ht_13 ct_b bt_60">I 회원약관</span></td>
            </tr>
            <tr>
              <td><textarea name="textarea2" style="border:solid 1px #d2d2d2; color: #777777; FONT-FAMILY: 돋움; FONT-SIZE: 9pt; padding:15; width:500px; height:120px; padding:15px;">
              
              회원 약관
    </textarea>              
    </td>
            </tr>
            <tr>
              <td align="left" style="padding-top:18px;vertical-align:middle;"><input type="checkbox" name="agreement1" id="agreement1" value="Y"> <span class="ht_11 ct_b">회원약관에 동의함</span></td>
            </tr>
        </tbody></table></td>
        <td align="right" valign="top" style="padding-left:15px;"><table border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td align="left" style="padding-bottom:10px;"><span class="ht_13 ct_b bt_60">I 개인정보를 위한 이용자 동의사항</span></td>
            </tr>
            <tr>
              <td><textarea name="textarea" style="border:solid 1px #d2d2d2; color: #777777; FONT-FAMILY: 돋움; FONT-SIZE: 9pt; padding:15; width:500px; height:120px; padding:15px;">■ 수집하는 개인정보 항목

ο 수집항목 : 이름 , 생년월일 , 로그인ID , 비밀번호 , 자택 전화번호 , 자택 주소 , 휴대전화번호 , 이메일 , 신체정보 , 서비스 이용기록 , 접속 로그 , 쿠키 , 접속 IP 정보 , 결제기록
ο 개인정보 수집방법 : 홈페이지(회원가입,상담게시판) , 배송 요청 

■ 개인정보의 수집 및 이용목적
 ο 서비스 제공에 관한 계약 이행 및 서비스 제공에 따른 요금정산
콘텐츠 제공 , 구매 및 요금 결제 , 물품배송 또는 청구지 등 발송
 ο 회원 관리
회원제 서비스 이용에 따른 본인확인 , 개인 식별 , 불만처리 등 민원처리 , 고지사항 전달
 ο 마케팅 및 광고에 활용
신규 서비스(제품) 개발 및 특화 , 이벤트 등 광고성 정보 전달 , 인구통계학적 특성에 따른 서비스 제공 및 광고 게재 , 접속 빈도 파악 또는 회원의 서비스 이용에 대한 통계

■ 개인정보의 보유 및 이용기간

원칙적으로, 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 파기합니다. 단, 관계법령의 규정에 의하여 보존할 필요가 있는 경우 "몰"은 아래와 같이 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다.


 보존 항목 : 서비스 이용기록 , 결제기록
 보존 근거 : 전자상거래등에서의 소비자보호에 관한 법률
 보존 기간 : 5년

대금결제 및 재화 등의 공급에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)

    </textarea>              </td>
            </tr>
            <tr>
              <td align="left" style="padding-top:18px; vertical-align:middle;"><input type="checkbox" name="agreement2" id="agreement2" value="Y"> <span class="ht_11 ct_b">개인정보 수집 및 이용에 동의함</span></td>
            </tr>
        </tbody></table></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td height="60" align="left" style="vertical-align:middle;"><input type="checkbox" name="agreement_all" id="agreement_all" value="Y" style="width:24px; height:24px; vertical-align:middle" onclick="Agreement_All()"><span class="ht_14 ct_b bt_60"> 회원약관 및 개인정보 수집 및 이용에 모두 동의합니다.</span></td>
  </tr>
</tbody></table>
    
    </td>
  </tr>
  <tr>
    <td height="50"></td>
  </tr>
</tbody></table>
<!-- 약관-->
<table width="1185" border="0" cellspacing="0" cellpadding="0">
<tbody><tr><td height="80" align="left"><span class="ht_18 ct_b bt_60"> 2. 정보 입력</span></td>
</tr>
  <tr>
    <td>
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
 


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="border-bottom:solid 1px #e0e0e0;border-top:solid 1px #e0e0e0; " align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">이름 </span></td>
    <td align="left">
    <input type="text" name="member_name" id="member_name" maxlength="10" style="border:1px solid #CCCCCC; height:30px;width:100px;line-height:30px; vertical-align:middle; padding-left:10px;">
    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">아이디 </span></td>
    <td>
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td width="200"><input type="text" name="member_id" id="member_id" value="" maxlength="16" style="width:200px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px; text-transform: lowercase;" autocomplete="off" onblur="isValidMemberID();"></td>
   
    <td style="padding-left:20px;"><span class="ht_12 ct_g9">영문소문자/숫자, 4~16자</span></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td align="left" style="padding-top:5px">
<span class="m_title31"><div id="div-memberid" class="alertmessage"></div></span> 
    </td>
  </tr>
</tbody></table>


    
    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">비밀번호 </span></td>
    <td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><span class="m_title31"><input type="password" name="member_pass" id="member_pass" value="" maxlength="16" style=" width:200px; height:30px;line-height:30px; vertical-align:middle; padding-left:10px; border:1px solid #CCCCCC;" autocomplete="off" onblur="isValidPassword()"></span> </td>
    <td style="padding-left:20px;"><span class="ht_12 ct_g9">영문 대/소문자+숫자 조합, 6~16자</span></td>
  </tr>
</tbody></table>
</td>
  </tr>
  <tr>
    <td align="left" style="padding-top:5px"><span class="m_title31"><div id="div-password1" class="alertmessage"></div></span></td>
  </tr>
</tbody></table>

    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">비밀번호 확인 </span></td>
    <td align="left">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><input type="password" name="member_repass" id="member_repass" value="" maxlength="16" style="width:200px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;" autocomplete="off" onblur="isValidPassword();confirmPassword();"></td>
  </tr>
  <tr>
    <td align="left" style="padding-top:5px"><span class="m_title31"><div id="div-password2" class="alertmessage"></div></span></td>
  </tr>
</tbody></table>

    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">생년월일 </span></td>
    <td style="vertical-align:middle;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="vertical-align:middle;">
	<select name="birth_year" id="birth_year" style="height:30px; width:60px; line-height:30px; vertical-align:middle; border:1px solid #CCCCCC;">
			<option value="">-</option>
			
			<option value="2018">2018</option>	
			
			<option value="2017">2017</option>	
			
			<option value="2016">2016</option>	
			
			<option value="2015">2015</option>	
			
			<option value="2014">2014</option>	
			
			<option value="2013">2013</option>	
			
			<option value="2012">2012</option>	
			
			<option value="2011">2011</option>	
			
			<option value="2010">2010</option>	
			
			<option value="2009">2009</option>	
			
			<option value="2008">2008</option>	
			
			<option value="2007">2007</option>	
			
			<option value="2006">2006</option>	
			
			<option value="2005">2005</option>	
			
			<option value="2004">2004</option>	
			
			<option value="2003">2003</option>	
			
			<option value="2002">2002</option>	
			
			<option value="2001">2001</option>	
			
			<option value="2000">2000</option>	
			
			<option value="1999">1999</option>	
			
			<option value="1998">1998</option>	
			
			<option value="1997">1997</option>	
			
			<option value="1996">1996</option>	
			
			<option value="1995">1995</option>	
			
			<option value="1994">1994</option>	
			
			<option value="1993">1993</option>	
			
			<option value="1992">1992</option>	
			
			<option value="1991">1991</option>	
			
			<option value="1990">1990</option>	
			
			<option value="1989">1989</option>	
			
			<option value="1988">1988</option>	
			
			<option value="1987">1987</option>	
			
			<option value="1986">1986</option>	
			
			<option value="1985">1985</option>	
			
			<option value="1984">1984</option>	
			
			<option value="1983">1983</option>	
			
			<option value="1982">1982</option>	
			
			<option value="1981">1981</option>	
			
			<option value="1980">1980</option>	
			
			<option value="1979">1979</option>	
			
			<option value="1978">1978</option>	
			
			<option value="1977">1977</option>	
			
			<option value="1976">1976</option>	
			
			<option value="1975">1975</option>	
			
			<option value="1974">1974</option>	
			
			<option value="1973">1973</option>	
			
			<option value="1972">1972</option>	
			
			<option value="1971">1971</option>	
			
			<option value="1970">1970</option>	
			
			<option value="1969">1969</option>	
			
			<option value="1968">1968</option>	
			
			<option value="1967">1967</option>	
			
			<option value="1966">1966</option>	
			
			<option value="1965">1965</option>	
			
			<option value="1964">1964</option>	
			
			<option value="1963">1963</option>	
			
			<option value="1962">1962</option>	
			
			<option value="1961">1961</option>	
			
			<option value="1960">1960</option>	
			
			<option value="1959">1959</option>	
			
			<option value="1958">1958</option>	
			
			<option value="1957">1957</option>	
			
			<option value="1956">1956</option>	
			
			<option value="1955">1955</option>	
			
			<option value="1954">1954</option>	
			
			<option value="1953">1953</option>	
			
			<option value="1952">1952</option>	
			
			<option value="1951">1951</option>	
			
			<option value="1950">1950</option>	
			
			<option value="1949">1949</option>	
			
			<option value="1948">1948</option>	
			
			<option value="1947">1947</option>	
			
			<option value="1946">1946</option>	
			
			<option value="1945">1945</option>	
			
			<option value="1944">1944</option>	
			
			<option value="1943">1943</option>	
			
			<option value="1942">1942</option>	
			
			<option value="1941">1941</option>	
			
			<option value="1940">1940</option>	
			
			<option value="1939">1939</option>	
			
			<option value="1938">1938</option>	
			
			<option value="1937">1937</option>	
			
			<option value="1936">1936</option>	
			
			<option value="1935">1935</option>	
			
			<option value="1934">1934</option>	
			
			<option value="1933">1933</option>	
			
			<option value="1932">1932</option>	
			
			<option value="1931">1931</option>	
			
			<option value="1930">1930</option>	
			
			<option value="1929">1929</option>	
			
			<option value="1928">1928</option>	
			
			<option value="1927">1927</option>	
			
			<option value="1926">1926</option>	
			
			<option value="1925">1925</option>	
			
			<option value="1924">1924</option>	
			
			<option value="1923">1923</option>	
			
			<option value="1922">1922</option>	
			
			<option value="1921">1921</option>	
			
			<option value="1920">1920</option>	
			
			<option value="1919">1919</option>	
			
			<option value="1918">1918</option>	
			
			<option value="1917">1917</option>	
			
			<option value="1916">1916</option>	
			
			<option value="1915">1915</option>	
			
			<option value="1914">1914</option>	
			
			<option value="1913">1913</option>	
			
			<option value="1912">1912</option>	
			
			<option value="1911">1911</option>	
			
			<option value="1910">1910</option>	
			
			<option value="1909">1909</option>	
			
			<option value="1908">1908</option>	
			
			<option value="1907">1907</option>	
			
			<option value="1906">1906</option>	
			
			<option value="1905">1905</option>	
			
			<option value="1904">1904</option>	
			
			<option value="1903">1903</option>	
			
			<option value="1902">1902</option>	
			
			<option value="1901">1901</option>	
			
			<option value="1900">1900</option>	
			
	</select>
    </td>
    <td align="center" width="20px" style="vertical-align:middle;">년</td>
    <td style="vertical-align:middle;">
	  <select name="birth_month" id="birth_month" style="height:30px; width:60px; line-height:30px; vertical-align:middle; border:1px solid #CCCCCC;">
		<option value="">-</option>
		
		<option value="01">01</option>
		
		<option value="02">02</option>
		
		<option value="03">03</option>
		
		<option value="04">04</option>
		
		<option value="05">05</option>
		
		<option value="06">06</option>
		
		<option value="07">07</option>
		
		<option value="08">08</option>
		
		<option value="09">09</option>
		
		<option value="10">10</option>
		
		<option value="11">11</option>
		
		<option value="12">12</option>
		
	  </select>
    </td>
    <td align="center" width="20px" style="vertical-align:middle;">월</td>
    <td style="vertical-align:middle;">
	  <select name="birth_day" id="birth_day" style="height:30px; width:60px; line-height:30px; vertical-align:middle; border:1px solid #CCCCCC;">
		<option value="">-</option>
		
		<option value="01">01</option>
		
		<option value="02">02</option>
		
		<option value="03">03</option>
		
		<option value="04">04</option>
		
		<option value="05">05</option>
		
		<option value="06">06</option>
		
		<option value="07">07</option>
		
		<option value="08">08</option>
		
		<option value="09">09</option>
		
		<option value="10">10</option>
		
		<option value="11">11</option>
		
		<option value="12">12</option>
		
		<option value="13">13</option>
		
		<option value="14">14</option>
		
		<option value="15">15</option>
		
		<option value="16">16</option>
		
		<option value="17">17</option>
		
		<option value="18">18</option>
		
		<option value="19">19</option>
		
		<option value="20">20</option>
		
		<option value="21">21</option>
		
		<option value="22">22</option>
		
		<option value="23">23</option>
		
		<option value="24">24</option>
		
		<option value="25">25</option>
		
		<option value="26">26</option>
		
		<option value="27">27</option>
		
		<option value="28">28</option>
		
		<option value="29">29</option>
		
		<option value="30">30</option>
		
		<option value="31">31</option>
		
	  </select>
    </td>
    <td align="center" width="20px" style="vertical-align:middle;">일</td>
  </tr>
</tbody></table>

    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">성별 </span></td>
    <td align="left">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><input type="radio" name="sex" value="0" checked=""></td>
    <td style="padding-left:10px;"><span class="ht_12 ct_b">여성</span></td>
    <td style="padding-left:20px;"><input type="radio" name="sex" value="1"></td>
    <td style="padding-left:10px;"><span class="ht_12 ct_b">남성</span></td>
  </tr>
</tbody></table>

    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">우편번호 </span></td>
    <td align="left">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><input type="text" name="member_zipcode" id="member_zipcode" value="" readonly="readonly" style="width:200px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;"></td>
    <td style="padding-left:20px;">
    <a href="javascript:sample6_execDaumPostcode();"> <img src="${ctx }/skin/images/join_bt06.gif" border="0" ></a></td>
  </tr>

</tbody></table>

    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">주소 </span></td>
    <td align="left">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><input type="text" name="member_addr1" id="member_addr1" maxlength="100" value="" readonly="" style="width:400px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;"></td>
  </tr>
  <tr>
    <td height="6"></td>
  </tr>
  <tr>
    <td><input type="text" name="member_addr2" id="member_addr2" maxlength="100" value="" style="width:400px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;"><span class="ht_12 ct_g9"> 나머지주소입력</span></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>

</tbody></table></td>
  </tr>
  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">E-MAIL </span></td>
    <td align="left">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
    <table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td>
<input type="hidden" name="member_email" id="member_email" value=""> 
      <input type="text" name="member_email1" id="member_email1" value="" style="width:150px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;" maxlength="100" onchange="isValidEmail1();">
        </td>
        <td width="20" align="center">@</td>
        <td><input type="text" name="member_email2" id="member_email2" value="" style="width:150px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;" onchange="isValidEmail1();" maxlength="100" readonly=""></td>
        <td style="padding-left:20px;">
	<select name="member_email3" id="member_email3" onchange="mailAddr();" style="width:150px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC;">
	<option selected="" value="">선택하세요 
	</option><option value="naver.com">네이버/naver.com 
	</option><option value="daum.net">다음/daum.net
	</option><option value="nate.com">네이트/nate.com 
	</option><option value="hotmail.com">핫메일/hotmail.com
	</option><option value="yahoo.com">야후(com)/yahoo.com  
	</option><option value="empas.com">엠파스(엠팔)/empas.com 
	</option><option value="korea.com">코리아닷컴/korea.com  
	</option><option value="dreamwiz.com">드림위즈/dreamwiz.com                                       
	</option><option value="gmail.com">지메일/gmail.com 
	</option><option value="etc">직접입력 </option></select>
        </td>
      </tr>
    </tbody></table>
    </td>
  </tr>
  <tr>
	<td align="left" style="padding-top:5px;">
	<span class="m_title31"><div id="div-email"></div></span> 
   </td>
  </tr>
</tbody></table>

    </td>
  </tr>

</tbody></table></td>
  </tr>

  <tr>
    <td style="border-bottom:solid 1px #e0e0e0;" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="9">
  <tbody><tr>
    <td width="150" align="left" style="padding-left:9px;"><span class="ht_12 ct_b">휴대폰 </span></td>
    <td align="left">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>

<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
		<select name="cp_phone1" id="cp_phone1" style="  height:30px; width:80px; line-height:30px; vertical-align:middle; border:1px solid #CCCCCC;" onchange="isValidCphone()">
				<option selected="" value="">번호선택 
				</option><option value="010">010 
				</option><option value="011">011 
				</option><option value="016">016 
				</option><option value="017">017 
				</option><option value="018">018 
				</option><option value="019">019</option>
		</select>
    </td>
    <td width="20" align="center">-</td>
    <td><input type="text" name="cp_phone2" id="cp_phone2" maxlength="4" value="" style="width:100px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;" class="telnumber"></td>
    <td width="20" align="center">-</td>
    <td><input type="text" name="cp_phone3" id="cp_phone3" maxlength="4" value="" style="width:100px; height:30px;line-height:30px; vertical-align:middle; border:1px solid #CCCCCC; padding-left:10px;" class="telnumber"></td>
  </tr>
</tbody></table>    
    
    </td>
  </tr>
  <tr>
    <td align="left" style="padding-top:5px"><span class="m_title31"><div id="div-cphone"></div></span> </td>
  </tr>
</tbody></table>

    
    </td>
  </tr>

</tbody></table></td>
  </tr>

 

</tbody></table></td>
  </tr>
</tbody></table>

    
        
<table border="0" cellspacing="0" cellpadding="0">
<tbody><tr><td height="40" colspan="3"></td></tr>
  <tr>
        <td><a href="javascript:FormCancel();"><img src="${ctx}/skin/images/m_06.jpg" border="0"></a></td>
        <td width="20"></td>
        <td><a href="javascript:FormOK();"><img src="${ctx}/skin/images/m_07.jpg" border="0"></a></td>
  </tr>
</tbody></table>

			     </td>
  </tr>

  <tr>
    <td height="200">
    
    </td>
  </tr>
</tbody></table>    </td>
    </tr>
</tbody></table>

 </form> 

 






<!-- 우편번호 레이어 -->
<div id="zipcodeLayer" style="width:455px; height:500px; background:#fff; z-index:9999; display:none; border:7px solid #ededed;"></div>

</center>

<%@include file="footer.jsp" %>

</body></html>