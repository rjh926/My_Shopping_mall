<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<c:set var="ctx"> ${pageContext.request.contextPath }</c:set>
<!-- saved from url=(0113)http://pay.dscount.com/order/BuyCartOrder_Member.asp?Mode=C&Kind=1&DelayCheck=&ExpressDelivery=N&cartseq=28106639 -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>디스카운트 [다홍/지니프/크리마레 통합멤버쉽]</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/AceCounter_AW.js.다운로드"></script><script async="" src="${ctx}/skin/js/analytics.js.다운로드"></script><script src="${ctx}/skin/js/top_javascript.js.다운로드"></script><script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script><script src="${ctx}/skin/js/jquery.cookie.js.다운로드"></script><script src="${ctx}/skin/js/cookie.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.bpopup.min.js.다운로드"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>

    function sample6_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('r_zipcode').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('r_addr1').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('r_addr2').focus();
            }
        }).open();
    }
</script>

<script type="text/javascript">

function checkDelivery(){
	var selectValue = document.getElementById('shipping').value;

	
	if(selectValue=='1'){
	
		document.getElementById('r_name').value = document.getElementById('member_name').value;	
		document.getElementById('r_zipcode').value = document.getElementById('member_zipcode').value;
		document.getElementById('r_addr1').value = document.getElementById('member_addr1').value;
		document.getElementById('r_addr2').value = document.getElementById('member_addr2').value;
		document.getElementById('r_cphone1').value = document.getElementById('member_cphone1').value;
	}

	else if(selectValue=='2')
		{
		
		document.getElementById('r_name').value = "";	
		document.getElementById('r_zipcode').value = "";
		document.getElementById('r_addr1').value = "";
		document.getElementById('r_addr2').value = "";
		document.getElementById('r_cphone1').value ="";
		}
	
	
	
}
</script>

<script>

    function sample7_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('member_zipcode').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('member_addr1').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('member_addr2').focus();
            }
        }).open();
    }
</script>

<script language="JavaScript">
<!--
function formatNumber(str)
{
	number = numOffMask(str.value);

	if (isNaN(number))
		str.value = "";
	else
		str.value = numOnMask(number);
}

function numOffMask(str)
{
	var tmp = str.split(",");
	tmp = tmp.join("");
	return tmp;
}

function numOnMask(str)
{

	if (str < 0) {
		str = Math.abs(str);
		sign = "-";
	} else {
		sign = "";
	}

	str = str + "";
	var idx = str.indexOf(".");

	if (idx < 0) {
		var txtInt = str;
		var txtFloat = "";
	} else {
		var txtInt = str.substring(0,idx);
		var txtFloat = str.substring(idx);
	}

	if (txtInt.length > 3) {
		var c=0;
		var myArray = new Array();
			for(var i=txtInt.length; i>0; i=i-3) {
			myArray[c++] = txtInt.substring(i-3,i);
 			}
		myArray.reverse();
 		txtInt = myArray.join(",");
 		}
 		str = txtInt + txtFloat;

	return sign + str;
}


function onlyNumber() 
{                          
	if((event.keyCode<48)||(event.keyCode>57) ) event.returnValue=false; 
}   

// 배송지 체크 


function set_recent_shipping(name,zipcode,addr1,addr2,hphone1,hphone2,hphone3,cphone1,cphone2,cphone3)
{
	$("#r_name").val(name);
	$("#r_zipcode").val(zipcode);
	$("#r_addr1").val(addr1);
	$("#r_addr2").val(addr2);
	$("#r_hphone1").val(hphone1);
	$("#r_hphone2").val(hphone2);
	$("#r_hphone3").val(hphone3);
	$("#r_cphone1").val(cphone1);
	$("#r_cphone2").val(cphone2);
	$("#r_cphone3").val(cphone3);
}

// ##### 상세정보 펼치는 스크립트 시작 #######

// 총 주문금액 펼치거나 닫기
function slide_div_orderamount_info()
{
    $("#orderinfo_arrow").attr("src", "//cdn.dscount.com/images_2015/rows_" + ($("#div_orderamount_info").is(":visible") ? "down" : "up") + ".png");
	$("#div_orderamount_info").slideToggle('fast');
}

// 총 주문금액 펼치거나 닫기
function slide_div_orderdiscount_info()
{
    $("#discountinfo_arrow").attr("src", "//cdn.dscount.com/images_2015/rows_" + ($("#div_orderdiscount_info").is(":visible") ? "down" : "up") + ".png");
	$("#div_orderdiscount_info").slideToggle('fast');
}


// ##### 상세정보 펼치는 스크립트 종료 #######

	
function FormOK() 
{		
	// 국내배송
	if ($('#shipping').val()=='1' || $('#shipping').val()=='2' || $('#shipping').val()=='3')
	{
		if($.trim($('#r_name').val()) == '') // 성명
		{
			alert('성명을 입력해 주세요');
			$('#r_name').focus();
			return;
		}
		if($.trim($('#r_zipcode').val()) == '') // 우편번호
		{
			alert('우편번호를 입력해 주세요');
			$('#r_zipcode').focus();
			return;
		}
		if($.trim($('#r_addr1').val()) == '') // 주소1
		{
			alert('주소를 입력해 주세요');
			$('#r_addr1').focus();
			return;
		}
		
		
		if($.trim($('#r_cphone1').val()) == '') // 이동전화1
		{
			alert('휴대폰 번호를 입력해 주세요');
			$('#r_cphone1').focus();
			return;
		}
	
	}
	else //해외배송
	{
		if($.trim($('#f_name').val()) == '') // 성명
		{
			alert('해외 수취인명을 입력해 주세요');
			$('#f_name').focus();
			return;
		}
		if($.trim($('#f_nation').val()) == '') // 국가명
		{
			alert('국가명을 입력해 주세요');
			$('#f_nation').focus();
			return;
		}
		if($.trim($('#f_zipcode').val()) == '') // 우편번호
		{
			alert('우편번호를 입력해 주세요');
			$('#f_zipcode').focus();
			return;
		}
		if($.trim($('#f_addr').val()) == '') // 영문주소
		{
			alert('영문주소를 입력해 주세요');
			$('#f_addr').focus();
			return;
		}
		if($.trim($('#f_phone').val()) == '') // 해외연락처
		{
			alert('해외연락처를 입력해 주세요');
			$('#f_phone').focus();
			return;
		}		
	}

	// 사은품 확인
	if ( $("#giftdisplay").val()=="Y" )
	{

		if( $("input[name='pserial']:checked").length==0)
		{
			alert("사은품을 선택해주세요");
			return;
		}
	
	}

	// 적립금 확인. 
	if ( $("#check_emoney").is(":checked")==true && $("#emoney").val() <= 0 ) 
	{
		alert("적립금을 입력해주세요");
		$("#emoney").focus();
		return;
	}

	

	// 결제정보 확인 체크
	if ( $("#confirm").is(":checked")==false )
	{
		alert("결제정보 확인 및 구매진행에 동의하셔야 합니다.");
		$("#confirm").focus();
		return;
	}
	

	$("#orderform").attr("action","${ctx}/order/regist.do").submit();
}


function DoFloor(objBox)
{
	pVal = objBox.value;
	objBox.value=Math.floor(pVal/100)*100;
}

// -->
</script>
<script>

function init(mode)
{
	var f = document.orderform;


	var DeliveryFee=0;
	var settle = 0;

	var basic_cash = 0;
	var basic_emoney = 0;
	var cash = 0;
	var emoney = 0;
	var LuckQty = 0;
	var CouponQty = 0;
	var SpecialQty = 0;
	var DeliveryQty = 0
	var totSaleAmount = 0;
	var GoodAmount=0;

	var remain_cash = 0;
	var remain_emoney = 0;
	var temp = 0;
	
	if (mode == "C")
		f.CouponChecked.value="N";
	
	GoodAmount= f.GoodAmount.value;
	LuckQty = numOffMask(f.LuckQty.value);
	CouponQty = numOffMask(f.CouponQty.value);
	SpecialQty = numOffMask(f.SpecialQty.value);
	DeliveryQty = numOffMask(f.DeliveryQty.value);
	OrderMoney = numOffMask(f.TotalMoney.value);

	basic_cash = numOffMask(f.basic_cash.value);
	basic_emoney = numOffMask(f.basic_emoney.value);

	totSaleAmount = eval(LuckQty)+eval(SpecialQty)+eval(CouponQty)+eval(DeliveryQty);
	settle = eval(OrderMoney-emoney-cash-totSaleAmount);
	f.SumMoney.value =  eval(OrderMoney-totSaleAmount);

	f.remain_emoney.value = eval(basic_emoney);	
	f.remain_cash.value = eval(basic_cash);	

	f.emoney.value = emoney;
	f.cash.value = cash;

	f.check_card.checked = true;
	f.check_cash.checked = false;
	f.check_emoney.checked = false;
	f.check_bank.checked = false;
	f.check_online.checked = false;

	var basic_field_name = new Array('card','mobile','bank','online','emoney','cash');

	for ( var i = 2 ; i < 5 ; i++ ) 
	{
		document.orderform['check_' + basic_field_name[i]].checked = false;
		document.orderform[basic_field_name[i]].disabled = true;
		document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
	}

	for ( var i = 0 ; i < 4 ; i++ ) 
	{
		if(document.orderform['check_' + basic_field_name[i]].checked) 
			checkPayment = true;
	}				
	
	

	i = 0
	document.orderform['check_' + basic_field_name[i]].checked = true;
	document.orderform[basic_field_name[i]].disabled = false;
	document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 								

	f.DeliveryFee.value = f.oriDeliveryFee.value;
	f.cs_code.value = 0;
	formatNumber(f.SumMoney);
	formatNumber(f.remain_emoney);
	formatNumber(f.remain_cash);
	formatNumber(f.card);
}

function calc_emoney()
{
    var f = document.orderform;

	var basic_cash = 0;
	var cash = 0;
	var temp = 0;
	var SumMoney = 0;
	var emoney = 0;
	var OrderMoney = 0;
	var LuckQty = 0;
	var CouponQty = 0;
	var SpecialQty = 0;
	var DeliveryQty = 0
	var totSaleAmount = 0;

	checkPayment = false;	
	
	basic_emoney = numOffMask(f.basic_emoney.value);
	emoney = numOffMask(f.emoney.value);
	SumMoney = numOffMask(f.SumMoney.value);
	cash = numOffMask(f.cash.value);
	OrderMoney = numOffMask(f.TotalMoney.value);
	LuckQty = numOffMask(f.LuckQty.value);
	CouponQty = numOffMask(f.CouponQty.value);
	SpecialQty = numOffMask(f.SpecialQty.value);
	DeliveryQty = numOffMask(f.DeliveryQty.value);
	DeliveryFee = numOffMask(f.DeliveryFee.value);
	RealGoodAmount = numOffMask(f.GoodAmount.value);

	totSaleAmount = eval(LuckQty)+eval(SpecialQty)+eval(CouponQty)+eval(DeliveryQty);

	var basic_field_name = new Array('card','mobile','bank','online','emoney','cash');

	if(eval(emoney - basic_emoney) > 0)
	{
		alert("사용가능한 적립금을 초과 했습니다");
		f.remain_emoney.value = f.basic_emoney.value;
		f.emoney.value = 0;
		f.emoney.focus();
		return;
	}
	else	
	{
		f.remain_emoney.value = eval(basic_emoney - emoney);	
	}

//	temp = eval(OrderMoney-emoney-cash-totSaleAmount)
	temp = eval(RealGoodAmount)-eval(cash)-eval(emoney)-eval(CouponQty)+eval(DeliveryFee);
	if(temp <= 0)
	{
//		f.emoney.value = eval(OrderMoney-cash-totSaleAmount);
		f.emoney.value = eval(RealGoodAmount)-eval(cash)-eval(CouponQty)+eval(DeliveryFee);
		f.remain_emoney.value = eval(basic_emoney-f.emoney.value);

		
		for ( var i = 0 ; i < 4 ; i++ ) 
		{
			document.orderform['check_' + basic_field_name[i]].checked = false;
			document.orderform[basic_field_name[i]].disabled = true;
			document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
		}
	}		
	else
	{
	
		for ( var i = 0 ; i < 4 ; i++ ) 
		{
			if(document.orderform['check_' + basic_field_name[i]].checked) 
				checkPayment = true;
		}				
		
		if(!checkPayment) 
		{
			i = 0
			document.orderform['check_' + basic_field_name[i]].checked = true;
			document.orderform[basic_field_name[i]].disabled = false;
			document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 								
		}

	}


	formatNumber(f.emoney);
	formatNumber(f.remain_emoney);	

	formatNumber(f.remain_emoney);			
	calc_total();
}

function calc_cash()
{
    var f = document.orderform;

	var basic_cash = 0;
	var cash = 0;
	var temp = 0;
	var SumMoney = 0;
	var emoney = 0;
	var OrderMoney = 0;
	var LuckQty = 0;
	var SpecialQty = 0;
	var CouponQty = 0;
	var DeliveryQty = 0;
	var DeliveryFee = 0;

	var totSaleAmount = 0;

	checkPayment = false;	
	
	basic_cash = numOffMask(f.basic_cash.value);
	cash = numOffMask(f.cash.value);
	SumMoney = numOffMask(f.SumMoney.value);
	emoney = numOffMask(f.emoney.value);

	OrderMoney = numOffMask(f.TotalMoney.value);
	LuckQty = numOffMask(f.LuckQty.value);
	SpecialQty = numOffMask(f.SpecialQty.value);
	CouponQty = numOffMask(f.CouponQty.value);
	DeliveryQty = numOffMask(f.DeliveryQty.value);
	DeliveryFee = numOffMask(f.DeliveryFee.value);
	RealGoodAmount = numOffMask(f.GoodAmount.value);

	
	totSaleAmount = eval(LuckQty)+eval(SpecialQty)+eval(CouponQty)+eval(DeliveryQty);


	var basic_field_name = new Array('card','mobile','bank','online','emoney','cash');

	if(eval(cash - basic_cash) > 0)
	{
		alert("사용가능한 적립금을 초과 했습니다");
		f.remain_cash.value = f.basic_cash.value;
		f.cash.value = 0;
		f.cash.focus();
		return;
	}
	else	
	{
		f.remain_cash.value = eval(basic_cash) - eval(cash);	
	}

	temp = eval(RealGoodAmount)-eval(cash)-eval(emoney)-eval(CouponQty)+eval(DeliveryFee);
	if( temp <= 0)
	{
		f.cash.value = eval(RealGoodAmount)-eval(emoney)-eval(CouponQty)+eval(DeliveryFee);
		f.remain_cash.value = eval(basic_cash)-eval(f.cash.value);	

		for ( var i = 0 ; i < 4 ; i++ ) 
		{
			document.orderform['check_' + basic_field_name[i]].checked = false;
			document.orderform[basic_field_name[i]].disabled = true;
			document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
		}
	}		
	else
	{

		for ( var i = 0 ; i < 4 ; i++ ) 
		{
			if(document.orderform['check_' + basic_field_name[i]].checked) 
				checkPayment = true;
		}				
		
		if(!checkPayment) 
		{
			i = 0
			document.orderform['check_' + basic_field_name[i]].checked = true;
			document.orderform[basic_field_name[i]].disabled = false;
			document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 								
		}

	}

	formatNumber(f.cash);
	formatNumber(f.remain_cash);		
	calc_total();
}

function calc_total()
{
    var f = document.orderform;

	var basic_cash = 0;
	var basic_emoney = 0;
	var SumMoney = 0;
	var OrderMoney = 0;
	var card = 0;
	var online = 0;
	var bank = 0;
	var cash = 0;
	var emoney = 0;
	var LuckQty = 0;
	var SpecialQty = 0;
	var CouponQty = 0;
	var DeliveryQty = 0
	var totSaleAmount = 0;
	var DeliveryFee = 0;
	var jumun_sum = 0;
	var jumun_good = 0;
	var jumun_delivery = 0;
	
	SumMoney = numOffMask(f.SumMoney.value);
	OrderMoney = numOffMask(f.TotalMoney.value);
	LuckQty = numOffMask(f.LuckQty.value);
	SpecialQty = numOffMask(f.SpecialQty.value);
	CouponQty = numOffMask(f.CouponQty.value);
	DeliveryQty = numOffMask(f.DeliveryQty.value);

	totSaleAmount = eval(LuckQty)+eval(SpecialQty)+eval(CouponQty)+eval(DeliveryQty);

	basic_cash = numOffMask(f.basic_cash.value);
	cash = numOffMask(f.cash.value);
	basic_emoney = numOffMask(f.basic_emoney.value);
	emoney = numOffMask(f.emoney.value);
	DeliveryFee = numOffMask(f.DeliveryFee.value);
	RealGoodAmount = numOffMask(f.GoodAmount.value);

	var basic_field_name = new Array('card','mobile','bank','online','emoney','cash');

	for ( var i = 0 ; i < 4 ; i++ ) 
	{
		if ( document.orderform['check_' + basic_field_name[i]].checked) 
		{		
			document.orderform[basic_field_name[i]].value = eval(RealGoodAmount) - eval(cash) - eval(emoney)-eval(CouponQty)+eval(DeliveryFee);
			formatNumber(document.orderform[basic_field_name[i]]);

		}
	}

	f.SumMoney.value = eval(RealGoodAmount) - eval(cash) - eval(emoney) - eval(CouponQty) + eval(DeliveryFee);

	escrowPayment = "N"
	for ( var i = 2 ; i < 4 ; i++ ) 
	{
		if ( document.orderform['check_' + basic_field_name[i]].checked) 
		{		
			escrowPayment = "Y";
		}
	}

	if (escrowPayment == "Y")
		document.getElementById('div_input_escrow').style.display = ""
	else
		document.getElementById('div_input_escrow').style.display = "none"

//	f.SumMoney.value = eval(OrderMoney - totSaleAmount);
	jumun_good = numOffMask(f.jumun_good.value);
	jumun_delivery = DeliveryFee;
	jumun_sum = eval(jumun_good) + eval(jumun_delivery);

	f.totSaleAmount.value = eval(totSaleAmount)+eval(emoney);

	f.jumun_sum.value = jumun_sum;
	f.jumun_delivery.value = jumun_delivery;

	formatNumber(f.jumun_sum);
	formatNumber(f.jumun_delivery);

	formatNumber(f.totSaleAmount);	
	formatNumber(f.SumMoney);	

	
// 전체 금액 재 산정후 필요한 값들을 다시 설정
	$("#discount_emoney").val($("#emoney").val());
	$("#discount_coupon").val($("#CouponQty").val());
//	$("#totSaleAmount").val(eval(totSaleAmount)+eval(emoney));
}



function checkCoupon(name, isBasic ) {

    var f = document.orderform;

	// 쿠폰값이 체크되었을때 다른 체크값을 없앤다.
	var basic_gubun_name = new Array('discount_gubun1','discount_gubun2');
	var basic_coupon_name = new Array('coupon_code1','coupon_code2');


	// 회원할인 체크시, 쿠폰할인 체크 disable
	// 쿠폰할인 체크시, 회원할인 체크 disable
	// 
	for ( var i = 0 ; i <= 1 ; i++ ) 
	{
			if (typeof(document.orderform[basic_gubun_name[i]]) == "object") 
			{

				if ( document.orderform[basic_gubun_name[i]].checked && name == basic_gubun_name[i] ) 
				{
				//	document.orderform[basic_gubun_name[i]].disabled = false;
					document.orderform[basic_coupon_name[i]].disabled = false;

				} 
				else 
				{
				//	document.orderform[basic_gubun_name[i]].checked = false;
				//	document.orderform[basic_gubun_name[i]].disabled = true;
					document.orderform[basic_gubun_name[i]].checked = false;
					document.orderform[basic_coupon_name[i]].disabled = true;
				}

			}
		
	}

	CancelCoupon();
	
	var basic_cash = 0;
	var SumMoney = 0;
	var settle_sum = 0;
	var coupon_price = 0
	var OrderMoney = 0;
	var LuckQty = 0;
	var SpecialQty = 0;
	var CouponQty = 0;
	var DeliveryQty = 0
	var totSaleAmount = 0;
	var card = 0
	var online = 0
	var bank = 0
	var cash = 0
	var j = 0;
	var emoney = 0;
	

	cash = numOffMask(f.cash.value);	
	SumMoney = numOffMask(f.SumMoney.value);	
	LuckQty = numOffMask(f.LuckQty.value);
	SpecialQty = numOffMask(f.SpecialQty.value);
	CouponQty = numOffMask(f.CouponQty.value);
	DeliveryQty = numOffMask(f.DeliveryQty.value);
	
	totSaleAmount = eval(LuckQty)+eval(SpecialQty)+eval(CouponQty)+eval(DeliveryQty);

	emoney = numOffMask(f.emoney.value);

	OrderMoney = numOffMask(f.TotalMoney.value);
	basic_cash = numOffMask(f.basic_cash.value);

	var basic_field_name = new Array('card','mobile','bank','online','emoney','cash');
	
	var check_1 = 0;

	for ( var i = 0 ; i < 4 ; i++ ) 
	{
		if ( document.orderform['check_' + basic_field_name[i]].checked ) 
		{
			check_1 ++;
			j = i;
		}
	}	

	
	//alert(eval(basic_cash-SumMoney));

	//if(check_1 == 0 && eval(basic_cash-SumMoney)<=0)
	temp = eval(OrderMoney - cash - emoney - totSaleAmount);

	if(check_1 == 0 && temp == 0)
	{
		document.orderform['check_' + basic_field_name[j]].checked = true;
		document.orderform[basic_field_name[j]].disabled = false;
		document.getElementById('div_input_' + basic_field_name[j]).style.display = ""; 	

		formatNumber(f.remain_cash);	
	}

	

	if(isBasic == true)
	{
		for ( var i = 0 ; i < 4 ; i++ ) 
		{
			if ( document.orderform['check_' + basic_field_name[i]].checked && name == basic_field_name[i] ) 
			{
				document.orderform[basic_field_name[i]].disabled = false;
				document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 
			} 
			else 
			{
				document.orderform['check_' + basic_field_name[i]].checked = false;
				document.orderform[basic_field_name[i]].disabled = true;
				document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
			}
			
		}
	}
	else
	{
		for ( var i = 4 ; i < 6 ; i++ ) 
		{
			if ( document.orderform['check_' + basic_field_name[i]].checked  ) 
			{
				document.orderform[basic_field_name[i]].disabled = false;
				document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 
			} 
			else 
			{
				document.orderform['check_' + basic_field_name[i]].checked = false;
				document.orderform[basic_field_name[i]].disabled = true;
				document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
				
				if(i==4)
				{
				f.remain_emoney.value = f.basic_emoney.value;
				formatNumber(f.remain_emoney);
				f.emoney.value = 0;
				}
				else
				{
				f.remain_cash.value = f.basic_cash.value;
				formatNumber(f.remain_cash);
				f.cash.value = 0;
				}
			}
		}

	}

	calc_total();
 }

function checkAmt(name, isBasic ) {

    var f = document.orderform;

	var basic_cash = 0;
	var SumMoney = 0;
	var settle_sum = 0;
	var coupon_price = 0
	var OrderMoney = 0;
	var LuckQty = 0;
	var SpecialQty = 0;
	var CouponQty = 0;
	var DeliveryQty = 0
	var totSaleAmount = 0;
	var card = 0
	var online = 0
	var bank = 0
	var cash = 0
	var j = 0;
	var emoney = 0;
	

	cash = numOffMask(f.cash.value);	
	SumMoney = numOffMask(f.SumMoney.value);	
	LuckQty = numOffMask(f.LuckQty.value);
	SpecialQty = numOffMask(f.SpecialQty.value);
	CouponQty = numOffMask(f.CouponQty.value);
	DeliveryQty = numOffMask(f.DeliveryQty.value);
	DeliveryFee = numOffMask(f.DeliveryFee.value);
	RealGoodAmount = numOffMask(f.GoodAmount.value);
	
	totSaleAmount = eval(LuckQty)+eval(SpecialQty)+eval(CouponQty)+eval(DeliveryQty);

	emoney = numOffMask(f.emoney.value);

	OrderMoney = numOffMask(f.TotalMoney.value);
	basic_cash = numOffMask(f.basic_cash.value);

	var basic_field_name = new Array('card','mobile','bank','online','emoney','cash');
	
	var check_1 = 0;

	for ( var i = 0 ; i < 4 ; i++ ) 
	{
		if ( document.orderform['check_' + basic_field_name[i]].checked ) 
		{
			check_1 ++;
			j = i;
		}
	}	

	
	//alert(eval(basic_cash-SumMoney));

	//if(check_1 == 0 && eval(basic_cash-SumMoney)<=0)
//	temp = eval(OrderMoney - cash - emoney - totSaleAmount);
	temp = eval(RealGoodAmount) - eval(cash) - eval(emoney)-eval(CouponQty)+eval(DeliveryFee)

	if(check_1 == 0 && temp == 0)
	{
		document.orderform['check_' + basic_field_name[j]].checked = true;
		document.orderform[basic_field_name[j]].disabled = false;
		document.getElementById('div_input_' + basic_field_name[j]).style.display = ""; 	
		formatNumber(f.remain_cash);	
	}

	

	if(isBasic == true)
	{
		for ( var i = 0 ; i < 4 ; i++ ) 
		{
			if ( document.orderform['check_' + basic_field_name[i]].checked && name == basic_field_name[i] ) 
			{
				document.orderform[basic_field_name[i]].disabled = false;
//				document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 
				$("#div_input_" + basic_field_name[i]).show();
			} 
			else 
			{
				document.orderform['check_' + basic_field_name[i]].checked = false;
				document.orderform[basic_field_name[i]].disabled = true;
				$("#div_input_" + basic_field_name[i]).hide();
//				document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
			}
			
		}
	}
	else
	{
		for ( var i = 4 ; i < 6 ; i++ ) 
		{
			if ( document.orderform['check_' + basic_field_name[i]].checked  ) 
			{
				document.orderform[basic_field_name[i]].disabled = false;
				$("#div_input_"+basic_field_name[i]).slideDown('fast');
//				alert(i);
//				alert(basic_field_name[i]);

//				document.getElementById('div_input_' + basic_field_name[i]).style.display = ""; 
			} 
			else 
			{
				document.orderform['check_' + basic_field_name[i]].checked = false;
				document.orderform[basic_field_name[i]].disabled = true;
				$("#div_input_"+basic_field_name[i]).slideUp('fast');
				//document.getElementById('div_input_' + basic_field_name[i]).style.display = "none"; 
				
				if(i==4)
				{
				f.remain_emoney.value = f.basic_emoney.value;
				formatNumber(f.remain_emoney);
				f.emoney.value = 0;
				}
				else
				{
				f.remain_cash.value = f.basic_cash.value;
				formatNumber(f.remain_cash);
				f.cash.value = 0;
				}
			}
		}

	}

	calc_total();
 }

function CancelCoupon()
{
	document.orderform.CouponChecked.value="N";
	document.orderform.CouponQty.value=0;
	init('C');
	calc_total();

	$("#div_cancelcoupon").hide();

	if ( $(":input:radio[name='cs_code']:checked").length >= 1 ) 
	{
		$(":input:radio[name='cs_code']").prop({checked: false});
	}

}

function selBasicAddressInfo(seq)
{
	$("#Deliveryaddress_Seq").val(seq);
	$("#addressinfo").load("BasicAddressInfo.asp");

}

function selRecentlyAddressInfo(seq)
{
	$("#Deliveryaddress_Seq").val(seq);
	$("#addressinfo").load("RecentlyAddressInfo.asp");
}

function newDeliveryAddressInfo()
{
	$("#Deliveryaddress_Seq").val(0);

	$( "#addressinfo" ).load( "newAddressInfo.asp", function( response, status, xhr ) {
	  if ( status == "success" ) {
		$("#r_name").focus();
	  }
	});	

}

function selectAddressData(seq,rname,addr1,addr2,cphone)
{	
	$("#Deliveryaddress_Seq").val(seq);
	$("#divRecentlyName").html("<span class='ht_14'>"+rname+" / "+cphone+"</span>");
	$("divRecentlyAddress").html("<span class='ht_14'>"+addr1+" "+addr2+"</span>");
}

function openZipcodelayer()
{
	$("#zipcodeLayer").show();
}

function closeZipcodelayer()
{
	$("#zipcodeLayer").hide();
}

function openADeliveryddrlayer()
{
	$("#div_delivery_addr").show();
}

function closeDeliveryAddrlayer()
{
	$("#div_delivery_addr").hide();
}

function show_addr()
{
	$("#Deliveryaddress_Seq").val(0);

	$( "#addressinfo" ).load( "newAddressInfo.asp", function( response, status, xhr ) {
	  if ( status == "success" ) {
		$("#div_delivery_addr").show();
	  }
	});	
}

function test()
{
	alert($("#Domestic_Delivery_Selected").val())
}

//-->	
</script>
<script src="${ctx}/skin/js/f.txt"></script><script src="${ctx}/skin/js/f(1).txt"></script><script src="${ctx}/skin/js/f(2).txt"></script></head>



<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>
<%@include file="header.jsp" %>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td align="center"><script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>




<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>

<script language="javascript">


function gotoItemMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
	
}

function gotoConceptGoodView(Gserial)
{
	location = "../Shopping/GoodView_Concept.asp?Gserial="+Gserial;	
	
}

function gotoCoordiView(Gserial)
{
	location = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	
}

function openCoordiView(Gserial)
{
	url = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
	
}

function gotoItemGood(arg)
{
	location = "../Shopping/ItemShopping_detail.asp?b="+arg;	
	
}

function gotoMDGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoCOSGoodView(Gserial)
{
	location = "../NShopping/GoodView_CItem.asp?Gserial="+Gserial;	
}

function gotoZinifGoodView(Gserial)
{
	location = "../NShopping/GoodView_ZItem.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotoMDGabalGoodView(Gserial, Gseq)
{
	location = "../Shopping/GoodView_Gabal.asp?Gserial="+Gserial+"&gseq=" + Gseq;	
}

function gotoNormalGoodView(Gserial)
{
	location = "../Shopping/GoodView_NItem.asp?Gserial="+Gserial;	
}


function gotoDahongGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
}

function openCatGoodView(Gserial)
{
	url =  "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function gotoBrandGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Brand.asp?Gserial="+Gserial;	
}

function gotoKeywordGoodView(Gserial)
{
	location = "../Nshopping/GoodView_Keyword.asp?Gserial="+Gserial;	
}


function gotoSaleGoodView(Gserial)
{
	location = "../Shopping/GoodView_Sale.asp?Gserial="+Gserial;	
}


function gotoItemGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoSummerGoodView(Gserial)
{
	location = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
}

function gotoCatGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotomonoGoodView(Gserial)
{
	location = "../Shopping2/GoodView_monomori.asp?Gserial="+Gserial;	
}

function gotoCatCoordiView(Gserial)
{
	location = "../Nshopping/GoodView_StyleBook.asp?Gserial="+Gserial;	
	
}


function gotoBigGoodView(Gserial)
{
	location = "../Shopping/GoodView_Big.asp?Gserial="+Gserial;	
}

function gotoItemShoppingMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
}


function gotoBigShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_BigDetail.asp?b="+arg;	
}


function gotoSaleShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_SaleDetail.asp?a="+arg;	
}



function openItemGoodView(Gserial)
{
	url = "/Nshopping/GoodView_Item.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function openDahongGoodView(Gserial)
{
	url = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function openSummerGoodView(Gserial)
{
	url = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function zoomPicture(arg)
{
   target = "../Shopping/zoomPicture.asp?Gserial=" + arg;
   window.open(target,"Picture","status=no,toolbar=no,scrollbars=no,resizable=no,width=780,height=780")
}

function gotoAlert()
{
	alert("경고창")
}

function addBookMark(){
window.external.AddFavorite('http://dahong.dscount.com', '디스카운트 [다홍/지니프/크리마레 통합멤버쉽]')
}


function bookmarksite(title,url) {
	if (window.sidebar) // firefox 
		window.sidebar.addPanel(title, url, ""); 
	else if(window.opera && window.print) // opera 
	{ 
		var elem = document.createElement('a'); 
		elem.setAttribute('href',url); 
		elem.setAttribute('title',title); 
		elem.setAttribute('rel','sidebar'); 
		elem.click(); 
	} 
	else if(window.external && ('AddFavorite' in window.external)) // ie
		window.external.AddFavorite(url, title);
	else if(window.chrome) // chrome
	{ 
		alert('ctrl+D 를 눌러서 북마크에 추가해주세요!'); 
	}
}


</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/images6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  

<script src="${ctx}/skin/js/banners.js.다운로드"></script>
<script src="${ctx}/skin/js/lnb.js.다운로드"></script>




<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->


<script language="javascript">
function div_promotion_onoff(state)
	{
		if (state==1)
		{
			document.getElementById('div_promotion').style.display='';
		}
		else
		{
			document.getElementById('div_promotion').style.display='none';
		}
	}

	function goto_promotion()
	{
		document.location.href='http://dahong.dscount.com/Nshopping/promotion_list.asp';
	}

</script>


    <script type="text/JavaScript">
<!--
function div_my_onoff(state)
{
	if (state==1)
	{
		$("#div_my").css('z-index','3000');
		document.getElementById('div_my').style.display='';
	}
	else
	{
		document.getElementById('div_my').style.display='none';
		$("#div_my").css('z-index','1000');
	}
}

function div_cs_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_cs').style.display='';
	}
	else
	{
		document.getElementById('div_cs').style.display='none';
	}
}

function div_dscountlayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dscountlayer').style.display='';
	}
	else
	{
		document.getElementById('div_dscountlayer').style.display='none';
	}
}


function div_dahonglayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dahonglayer').style.display='';
	}
	else
	{
		document.getElementById('div_dahonglayer').style.display='none';
	}
}

function div_monomorilayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_monomorilayer').style.display='';
	}
	else
	{
		document.getElementById('div_monomorilayer').style.display='none';
	}
}


function div_secondleedslayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_secondleedslayer').style.display='';
	}
	else
	{
		document.getElementById('div_secondleedslayer').style.display='none';
	}
}

function div_ziniflayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_ziniflayer').style.display='';
	}
	else
	{
		document.getElementById('div_ziniflayer').style.display='none';
	}
}

function div_creemarelayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_creemarelayer').style.display='';
	}
	else
	{
		document.getElementById('div_creemarelayer').style.display='none';
	}
}

function searchform_act()
{
	if (document.getElementById("layersearchform").style.display == 'none')
	{
		document.getElementById("layersearchform").style.display = '';
	}
	else
	{
		document.getElementById("layersearchform").style.display ='none';
	}
}

function div_menu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_menu').style.display='';
	}
	else
	{
		document.getElementById('div_menu').style.display='none';
	}
}

function div_allmenu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_allmenu').style.display='';
	}
	else
	{
		document.getElementById('div_allmenu').style.display='none';
	}
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>




<!-- 내용 테이블 시작-->

<!-- 고객센터-->


<!-- 상품 검색-->

<!-- 상품검색-->

<!-- 로그인메뉴종료-->


<!-- 공통상단 종료 -->


			<!-- 플로팅 메뉴 - 최근 본 상품 시작 -->

				<!--최근본상품 테이블 시작-->
				
					  <!-- 상품 루프 시작-->
					
					  <!-- 상품 루프 종료-->
				
				  <!--최근본상품 테이블 종료-->

			<!-- 플로팅 메뉴 - 최근 본 상품 종료 -->

    </td>
  </tr>
</tbody></table>
</td>
    </tr>
  </tbody></table>

<!-- 타이틀-->
<table width="1185" height="140" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" valign="top" style="padding:50px 0px 0px 0px;"><span class="et_30 ct_b bt_50">DELIVERY / PAYMENT</span></td>
    <td align="right" valign="top" style="padding:30px 0px 0px 0px;"><img src="${ctx}/skin/images/obar_02.jpg"></td>
  </tr>
</tbody></table>
<!-- 타이틀-->


<form method="post" name="orderform" id="orderform" >
  <input type="hidden" name="basic_cash" value="0">
  <input type="hidden" name="basic_emoney" id="basic_emoney" value="0">
  <input type="hidden" name="CartCount" value="1">
  <input type="hidden" name="Division" value="M">
  <input type="hidden" name="Kind" value="1">
  <input type="hidden" name="GoodAmount" value="45800">
  <input type="hidden" name="BasicRate" value="0">
  <input type="hidden" name="SpecialRate" value="0">
  <input type="hidden" name="cartseq" value="28106639">
  <input type="hidden" name="ExpressDelivery" value="N">
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style=" padding:0px 0px 15px 0px;"><span class="ht_18 ct_b bt_50">주문상품 확인 (${total})</span></td>
  </tr>
  <tr>
    <td align="center" style="background:#f3f3f3;padding:17px 0px 17px 0px; border-top:solid 1px #dedede; border-bottom:solid 1px #dedede;">
      <table width="1185" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>                            
         <td align="center" width="525"><span class="ht_13 bt_50">상품정보</span></td> 
         <td align="center" width="220"><span class="ht_13 bt_50">판매가</span></td>                                
         <td align="center" width="220"><span class="ht_13 bt_50">수량</span></td>                                
         <td align="center" width="220"><span class="ht_13 bt_50">적립금</span></td>                                                             
        </tr>
      </tbody></table>
    </td>
  </tr>
	
  <tr>
    <td style="padding-top:18px; padding-bottom:18px; border-bottom:solid 1px #e2e2e2;">
    
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td width="525" align="left" style="padding-left:20px;">
<!-- 상품옵션테이블-->
<table border="0" cellspacing="0" cellpadding="0">
  <tbody>
  <c:forEach items="${orderList}" var="product">
  <tr>
    <td style="padding-right:20px;"><img src="${ctx}/skin/images/${product.product.image}" width="80"></td>
    <td align="left" valign="middle">
<table border="0" cellspacing="0" cellpadding="0">

  <tbody><tr>
    <td align="left"><span class="ht_14 ct_b bt_50">${product.product.productName}</span></td>
  </tr>

  <tr>
    <td align="left" style="padding-top:15px;"><span class="ht_13 ct_br">옵션 : [${product.product.color } / ${product.product.productSize } ]</span></td>
  </tr>
</tbody></table>

    </td>
  </tr>
 
</tbody></table>
<!-- 상품옵션테이블-->
    </td>
    <td width="220" align="center">    
	
	<span class="ht_13 ct_b">${product.product.price }원</span> 
	
    </td>
    <td width="220" align="center"><span class="ht_13 ct_b">${product.amount }개</span></td>
    <td width="220" align="center"><span class="ht_13 ct_b">${product.product.price /100}원</span></td>
  </tr>
</tbody></table>

 </c:forEach>

    
    </td>
  </tr>
                  
  <tr>
    <td height="30"></td>
  </tr>
  <tr>
    <td style="border:solid 1px #3f4248; padding-top:40px; padding-bottom:40px;" align="center">
<!--결제금액-->
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="26"><span class="ht_16 ct_g10" style="padding-right:20px;">총상품금액</span><span class="ht_18"><b>${totalMoney}</b>원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_12.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">할인금액</span><span class="ht_18 ct_p3"><b>0</b>원</span></td>
        <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_11.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">배송비</span><span class="ht_18"><b>2,500</b>원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_13.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">결제예정금액</span><span class="ht_18"><b>${totalMoney+2500}</b>원</span></td>
  </tr>
</tbody></table>
<!--결제금액-->
    </td>
</tr>
  <!-- 당일배송문구-->
	
  <!-- 당일배송문구-->
</tbody></table>
    </td>
  </tr>
  <tr>
    <td height="50"></td>
  </tr>
  <tr>
    <td>
<!-- 주문자 정보 시작-->

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="background:#f3f3f3; padding: 15px 0px 15px 25px; border:solid 1px #e0e0e0;"><span class="ht_14 bt_60">주문자 정보</span></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center" style="padding:20px 0px 20px 0px; border-bottom:solid 1px #e0e0e0; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">성 명 </span></td>
    <td align="left"><input type="text" name="member_name" id="member_name" value="${user.name}"  size="10" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC; FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">우편번호 </span></td>
    <td align="left"><input type="text" name="member_zipcode" id="member_zipcode" value="${user.zipCode }"  size="7" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
    <a href="javascript:sample7_execDaumPostcode();"><img src="${ctx}/skin/images/join_bt06.gif" border="0" ></a></td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">주소</span></td>
    <td align="left"><input type="text" name="member_addr1" id="member_addr1" value="${user.address }"  size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC; FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">나머지 주소</span></td>
    <td align="left"><input type="text" name="member_addr2" id="member_addr2" value=""  size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    </td>
  </tr>
                      
  
                      
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">핸드폰</span></td>
    <td align="left">
<input type="text" name="member_cphone1" id="member_cphone1"  size="15" maxlength="15" value="${user.phone }" onkeypress="onlyNumber()" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                             </td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">이메일 주소</span></td>
    <td align="left"><input type="text" name="member_email" id="member_email" value="${user.email }"  style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    
</td>
  </tr>
</tbody></table>    

   </td>
  </tr>
</tbody></table>
<!-- 주문자 정보 종료-->
    </td>
  </tr>
 
    

  <tr>
    <td style="padding-top:40px;">    
    
 <!--배송지 정보 시작-->   
<div id="delivery_info">
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="background:#f3f3f3; padding: 15px 0px 15px 25px; border:solid 1px #e0e0e0;"><span class="ht_14 bt_60">배송지 정보</span></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center" style="padding:20px 0px 20px 0px; border-bottom:solid 1px #e0e0e0; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;">
    
    <table width="1185" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td bgcolor="#FFFFFF" align="center">
        
  <table width="94%" border="0" cellspacing="0" cellpadding="5">
    <tbody><tr>
      <td width="150" align="left"><span class="ht_14">배송지 선택</span></td>
      <td align="left">
        <label>
          <select name="shipping" id="shipping" style=" width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;" onchange="checkDelivery()">
            <option value="0" selected="">====선택====</option>
            <option value="1">주문자 정보와 동일
              </option><option value="2">직접입력
                </option><option value="3">최근배송지 
                  </option><option value="4">해외배송
                  </option></select>
          </label>
        </td>
      </tr>
  </tbody></table>
        
        
        </td>
    </tr>
 
 
 <!-- 직업입력시 시작-->
  <tr>
    <td bgcolor="#FFFFFF" align="center" id="div_shipping_info">
		
			<table width="1185" border="0" cellspacing="0" cellpadding="0">
			  <tbody><tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">성 명</span></td>
				<td align="left"><input type="text" name="r_name" id="r_name" value="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">우편번호</span></td>
				<td width="60" align="left">
				<input type="text" name="r_zipcode" id="r_zipcode" value="" readonly="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                </td>
				<td align="left">
			<table width="90" border="0" cellspacing="0" cellpadding="0">
			  <tbody><tr>
				<td height="29" align="center" style="border:1 solid #CCCCCC; cursor:pointer;cursor:hand;" bgcolor="#E3E3E3"><a href="javascript:sample6_execDaumPostcode();"><span class="ht_12 bt_60">우편번호 검색</span></a></td>
			  </tr>
			</tbody></table>

				</td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">주소</span></td>
				<td align="left"><input type="text" name="r_addr1" id="r_addr1" value="" size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;" readonly=""></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">나머지 주소</span></td>
				<td align="left"><input type="text" name="r_addr2" id="r_addr2" value="" size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			

				
				</td>
			  </tr>
			  
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">핸드폰</span></td>
				<td align="left">
			<input type="text" name="r_cphone1" id="r_cphone1" maxlength="15" onkeypress="onlyNumber()" size="15" value="" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
									</td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">배송메세지</span></td>
				<td align="left"><textarea name="memo" rows="5" style=" width:450px; border:1px solid #CCCCCC; 굴림; padding: 5px 5px 5px 5px; FONT-SIZE: 9pt;"></textarea></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			</tbody></table>
    
    </td>
  </tr>  
  <!-- 직접입력 종료-->
</tbody></table>
  </td>
  </tr>
</tbody></table>
</div> 
 <!--배송지 정보 종료-->

    </td>
  </tr>
<input type="hidden" name="Deliveryaddress_Seq" id="Deliveryaddress_Seq" value="0">   
<input type="hidden" name="regBasicDeliveryAddressSeq" id="regBasicDeliveryAddressSeq" value="0">   
<input type="hidden" name="basicDeliveryAddressSeq" id="basicDeliveryAddressSeq" value=""> 
<input type="hidden" name="recentlyDeliveryAddressSeq" id="recentlyDeliveryAddressSeq" value=""> 

      <tr>
        <td height="20"></td>
      </tr>
  <tr>
    
    <td height="20"></td>
  </tr>
<input type="hidden" name="TotalMoney" value="48300">
<input type="hidden" name="DeliveryFee" value="2500">
<input type="hidden" name="SpecialQty" value="0">
<input type="hidden" name="SpecialDay" value="N">
<input type="hidden" name="CouponChecked" value="N">
<input type="hidden" name="LuckQty" value="0">
<input type="hidden" name="CouponCode" value="">
<input type="hidden" name="cs_code" value="0">

  <tr>
    <td>
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">할인/적립정보 입력</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">

    </td>
  </tr>  
  
  <tr>
    <td bgcolor="ffffff" align="center">

<!-- 적립금 테이블 시작-->
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="100%" align="left">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tbody><tr>
			<td align="left">
			
			<input type="checkbox" name="check_emoney" id="check_emoney" value="1" onclick="checkAmt(emoney,false);" disabled=""> <span class="ht_13">적립금 [ <span class="ct_p2 bt_60">${user.mileage }</span>원 ]</span></td>
		  </tr>
		</tbody></table>
<!-- 적립금 선택시 나타나는 테이블 시작-->
        <div id="div_input_emoney" style="display:none; padding-top:10px; padding-left:20px;">
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
			<td align="left"><span class="ht_13" style="padding-right:5px;">사용가능 적립금 :</span><input type="text" style="width:100px;height:22px;text-align:right;vertical-align:middle;font-family: Nanum Gothic, sans-serif;font-size:13px;line-height:13px;color:#000000;font-weight:bold; padding: 0px 0px 0px 0px;background-color:transparent; border:0px;" class="logbox" name="remain_emoney" id="remain_emoney" value="0" onchange="formatNumber(this)" onkeypress="onlyNumber()" readonly=""><span class="ht_13" style="padding-right:20px;">원</span></td>
			<td align="left"><input type="text" class="logbox" name="emoney" id="emoney" readonly="" onchange="DoFloor(this);formatNumber(this)" onblur="calc_emoney()" onfocus="this.select()" value="0" onkeypress="onlyNumber()" style="width:100px;height:22px;text-align:right;vertical-align:middle;font-family: Nanum Gothic, sans-serif;font-size:13px;line-height:13px;color:#F92B82;font-weight:bold; padding:0px 0px 0px 0px;background-color:#ffffff; border:1px solid #CCCCCC;"><span class="ht_13">원</span></td>

		  </tr>
		</tbody></table>
       </div>
<!-- 적립금 선택시 나타나는 테이블 종료-->
    </td>
  </tr>
</tbody></table>
<!-- 적립금 테이블 종료-->


    </td>
  </tr> 
 
  
</tbody></table>    

    
    </td>
  </tr>

 			    <input type="hidden" name="DeliveryQty" value="0">
				<input type="hidden" name="DeliveryCoupon" value="N">

                  <!-- 쿠폰적용을 위해 기본값 보존 -->
                  <input type="hidden" name="oriTotalMoney" value="">
                  <input type="hidden" name="oriLuckQty" value="">
                  <input type="hidden" name="oriSumMoney" value="48300">	
				  <input type="hidden" name="oriDeliveryFee" value="2500">	
      <tr>
        <td height="20"></td>
      </tr>
  <tr>
    <td>
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">주요 결제수단 선택</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td style="padding-left:21px;" align="left">
    <span class="ht_13">
    
    <input type="radio" name="payment" value="Card" checked="" onclick="checkAmt(card,true);">신용카드&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" name="payment" value="Bank" onclick="checkAmt(bank,true);">실시간계좌이체&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" name="payment" value="deposit" onclick="checkAmt(online,true);">무통장입금&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" name="payment" value="Phone" onclick="checkAmt(mobile,true);">휴대폰결제
    </span>
    </td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr>
    <td bgcolor="ffffff" align="center">

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="15"></td>
  </tr>
  
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td style="padding-left:23px;" align="left">
    


    
   </td>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
</tbody></table>



    </td>
  </tr>
</tbody></table>

    </td>
  </tr>

      <tr>
        <td height="20"></td>
      </tr>
		  
<!--
      <tr>
        <td>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="7">
  <tr>
    <td><img src="http://cdn.dscount.com/images_2014/shopping/s_coupon_.jpg" width="35%"></td>   
  </tr>
<tr><td><span class="m_title04"><strong>만원이상 구매하시면 다음 구매시 이용가능한 % 할인쿠폰이 발급됩니다.</strong><br>상품수령후 “수령확인”을 체크해주시면 15일후 자동발급됩니다</span></td></tr>
</table>

        
        </td>
      </tr>
      <tr>
        <td height="15"></td>
      </tr>
-->
		  
  <tr>
    <td>
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">총 결제예정 금액</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center" onclick="slide_div_orderamount_info()">
  
  
  <table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="30%" height="35" class="td01" style="padding-left:25px;" align="left"><span class="ht_13 bt_60">총 주문금액</span></td>
    <td width="65%" height="35" align="right">
      <input type="text" name="jumun_sum" id="jumun_sum" readonly="" value="48,300" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
      </td>
    <td width="5%" align="center"><img src="${ctx}/skin/images/rows_down.png" id="orderinfo_arrow"></td>
  </tr>
</tbody></table>

    </td>
 </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
   <div id="div_orderamount_info" style="display:none">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style=" border-bottom: 1px solid #e0e0e0" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" style="padding-left:26px;" align="left"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">상품금액</span></td>
    <td width="65%" align="right">
			  <input type="text" name="jumun_good" id="jumun_good" size="15" readonly="" value="45,800" style="width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span>
    </td>
    <td width="5%">&nbsp;</td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
  <tr>
    <td align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" style="padding-left:26px;" align="left"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">배송비</span></td>
    <td width="65%" align="right">
			  <input type="text" name="jumun_delivery" id="jumun_delivery" size="15" readonly="" value="2,500" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span>

    </td>
    <td width="5%">&nbsp;</td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>
</div>

    </td>
  </tr>
  <tr>
    <td bgcolor="ffffff" align="center" onclick="slide_div_orderdiscount_info()">
  
  
  <table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="30%" height="35" class="td01" style="padding-left:25px;" align="left"><span class="ht_13 bt_60">총 할인금액</span></td>
    <td width="65%" height="35" align="right">
      <input type="text" name="totSaleAmount" id="totSaleAmount" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
      </td>
    <td width="5%" align="center"><img src="${ctx}/skin/images/rows_down.png" id="discountinfo_arrow"></td>
  </tr>
</tbody></table>

    </td>
 </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
   <div id="div_orderdiscount_info" style="display:none">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style=" border-bottom: 1px solid #e0e0e0" align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" style="padding-left:26px;" align="left"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">쿠폰할인</span></td>
    <td width="65%" align="right">
			  <input type="text" name="discount_coupon" id="discount_coupon" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td style=" border-bottom: 1px solid #e0e0e0" align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">상품할인</span></td>
    <td width="65%" align="right">
			  <input type="text" name="discount_good" id="discount_good" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">적립금</span></td>
    <td width="65%" align="right">
			  <input type="text" name="discount_emoney" id="discount_emoney" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>
</div>
    </td>
  </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
  
  
  <table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="30%" height="35" class="td01" style="padding-left:25px;" align="left"><span class="ht_13 bt_60">총 결제하실 금액</span></td>
    <td width="65%" height="35" align="right">
      <input type="text" name="SumMoney" readonly="" value="48,300" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
      </td>
    <td width="5%" align="center"></td>
  </tr>
</tbody></table>

    </td>
 </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td>
              <!--신용카드-->
      
               <div id="div_input_card"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">신용카드</span></td>
    <td align="right">
      <input type="text" class="logbox" name="card" readonly="" onfocus="this.select()" value="48,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>  
              </div>
              <!--신용카드-->
             
               <!--휴대폰결제-->
      
               <div id="div_input_mobile" style="display:none"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">휴대폰결제</span></td>
    <td align="right">

      <input type="text" class="logbox" name="mobile" size="15" readonly="" onfocus="this.select()" value="48,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>  
              </div>
<!--              <휴대폰결제> -->            
             
              <!--계좌이체-->
              <div id="div_input_bank" style="display:none"> 
           
                
<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">실시간계좌이체</span></td>
    <td align="right">

    <input type="text" class="logbox" name="bank" size="15" readonly="" onfocus="this.select()" value="0" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span> 

    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>  


              </div>
              <!--계좌이체-->
              <!--무통장입금-->
              <div id="div_input_online" style="display:none"> 
                   
<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">무통장입금</span></td>
    <td align="right">

    <input type="text" class="logbox" name="online" size="15" readonly="" onfocus="this.select()" value="0" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14">원</span> 

    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
  <input type="hidden" name="cashreceipt" id="cashreceipt" value="N">
  <input type="hidden" name="cashreceipt_info" id="cashreceipt_info" value="">
</tbody></table>  


              </div>
              <!--무통장입금-->

              <!--신용카드-->
      
               <div id="div_input_naver" style="display:none"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">네이버페이</span></td>
    <td align="right">
      <input type="text" class="logbox" name="naver" readonly="" onfocus="this.select()" value="48,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: Nanum Gothic, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"> <span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>  
              </div>
              <!--신용카드-->

              <!--에스크로-->
              <div id="div_input_escrow" style="display:none"> 
                
 <table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>                 
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
  <tbody><tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_14 bt_60">에스크로 결제</span></td>
    <td align="left">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="ffffff">
  <tbody><tr>
    <td align="left"><span class="ht_14"><input name="escrowflag" type="radio" value="N" checked=""> 미신청</span> &nbsp;&nbsp;<span class="ht_14"><input name="escrowflag" type="radio" value="Y">신청</span></td>
  </tr>
  <tr>
    <td align="left" style="padding-top:10px;"><span class="ht_14">(배송확인시 공인인증서가 필요합니다.)</span></td>
  </tr>
</tbody></table>
    
    </td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>
              
                  
              </div>
              <!--에스크로-->


    </td>
  </tr>
  
		  <!-- 기프트 시작-->

	<input type="hidden" name="giftdisplay" id="giftdisplay" value="N">
	<input type="hidden" name="giftcheck" id="giftcheck" value="E">
	<input type="hidden" name="giftstate" id="giftstate" value="해당없음">

		  <!-- 기프트 종료-->
      <tr>
        <td height="40"></td>
      </tr>
      <tr>
        <td align="center"><input type="checkbox" name="confirm" id="confirm" value="Y" style="width:22px; height:22px;"><span class="ht_18"> 결제정보를 확인하였으며, 구매진행에 동의합니다.</span></td>
      </tr>
      <tr>
        <td height="50"></td>
      </tr>
  <tr>
    <td align="center">
    
<table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td style="cursor:pointer;cursor:hand;" onclick="javascript:history.go(-1)"><img src="${ctx}/skin/images/o_01.jpg"></td>
        <td width="10"></td>
        <td style="cursor:pointer;cursor:hand;" onclick="javascript:FormOK()"><img src="${ctx}/skin/images/o_02.jpg"></td>
      </tr>
    </tbody></table>
    
    </td>
  </tr>
      <tr>
        <td height="150"></td>
      </tr>
</tbody></table>

    
    
  



<script type="" language="JavaScript">
<!--
function openZipcodelayer(mode)
{
	url = "/lib/zipcode_api.asp?mode=" + mode;
	$('#zipcodeLayer').bPopup({
		content:'iframe',
		iframeAttr:"name='hidden_iframe' id='hidden_iframe' frameborder='0' width='100%' height='100%'",
		loadUrl:url
//		contentContainer:'.scrollable',
	});
	
//	document.getElementById('hidden_iframe').src=url;
//	document.all['zipcodeLayer'].style.display = ""; 
//	document.getElementById("zipcodeLayer").style.zIndex = "9999";
}

function closeZipcodelayer()
{
	$('#zipcodeLayer').bPopup().close();
	$('#zipcodeLayer').html('');
//	document.getElementById("zipcodeLayer").style.display = "none";
}

function opencouponlayer()
{
	url = "coupon.asp?RealGoodAmount=45800&argCartSeq=28106639&t=80353.48"
	$('#couponlayer').bPopup({
		content:'iframe',
		iframeAttr:"frameborder='0' width='100%' height='100%'",
		loadUrl:url
//		contentContainer:'.scrollable',
	});

//	document.getElementById("couponlayer").style.display = "";
//	document.getElementById("couponlayer").style.zIndex = "9999";
}

function closecouponlayer()
{
	$('#couponlayer').bPopup().close();
	$('#couponlayer').html('');
//	document.getElementById("couponlayer").style.display = "none";
}


//-->	
</script> 

<!-- 우편번호 레이어 -->
<div id="zipcodeLayer" style="width:455px; height:500px; background:#fff; z-index:9999; display:none; border:7px solid #ededed;"></div>
<!-- 쿠폰 레이어 -->
<div id="couponlayer" style="width:500px; height:500px; background:#fff; z-index:9999; display:none; border:7px solid #ededed;"></div>

</form>


 



</body></html>