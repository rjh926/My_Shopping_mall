<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%@include file="header.jsp" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<table class="table">
<tr>
<th >사진</th>
<th >상품</th>
<th >상품가격</th>
<th>변경/삭제</th>
</tr>

<c:forEach var="product" items="${products }">
<script type="text/javascript">
function ConfirmDelete(id) {
	var id = id;
	var con_firm = confirm("삭제하시겠습니까?");
	
	if(con_firm == true){
		alert('${ctx}/product/delete.do?id=+id');
		location.replace('${ctx}/product/delete.do?id='+id);
	}
	
}
</script>
<tr >
<td width="60px" rowspan="2">
<img src="${ctx}/skin/images/${product.image}" width="60px" height="70px">
</td>
<td><p class="text-center">${product.productName }</p></td>
<td rowspan="2" width="120px"><p class="text-center" > ${product.price }원</p> </td>
<td rowspan="2" align="left" width="200px">
<button type="button" class="btn btn-warning" onclick="location.href='${ctx}/product/modify.do?id=${product.productId }'">수정</button>

<!-- Indicates a dangerous or potentially negative action -->
<button type="button" class="btn btn-danger" onclick="ConfirmDelete(${product.productId});">삭제</button>
</td>
</tr>
<tr><td><p class="text-center">${product.productGroup} / ${product.detailGroup} / ${product.detail }</p></td></tr>
</c:forEach>


</table>


</body>
</html>