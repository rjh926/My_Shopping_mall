<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<!-- saved from url=(0040)https://member.dscount.com/my/missid.asp -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>디스카운트 [다홍/지니프/크리마레 통합멤버쉽]</title>
<link rel="stylesheet" type="text/css"
	href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/top_javascript.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script>
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/jquery.cookie.js.다운로드"></script>
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/cookie.js.다운로드"></script>
<script language="JavaScript">
<!--
	function MissID() {
		if (document.miss.Name.value.length == 0) {
			alert("성명을 입력해 주십시오");
			document.miss.Name.focus();
			return;
		} else if (document.miss.email.value.length == 0) {
			alert("이메일 주소를 입력해 주십시오");
			document.miss.email.focus();
			return;
		}

		document.miss.submit();
	}
// -->
</script>

</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
	<center>

		<table width="100%" height="361" border="0" align="center"
			cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td align="center" valign="top">


						<table width="700" border="0" cellspacing="0" cellpadding="0"
							style="border: solid 2px #92ced9;">
							<tbody>
								<tr>
									<td align="center"
										style="background: #92ced9; padding-top: 20px; padding-bottom: 20px;">
										<table border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td align="center" style="padding-bottom: 15px;"><span
														class="ht_22 ct_b bt_60">회원 아이디 찾기</span></td>
												</tr>
												<tr>
													<td align="center"><span class="ht_13 ct_b bt_50">
															아이디나 비밀번호가 생각나지 않으세요?<br> 회원님의 개인정보를 안전하게 되찾으실 수 있도록
															도와드리겠습니다.
													</span></td>
												</tr>
											</tbody>
										</table>

									</td>
								</tr>
								<tr>
									<td height="5"
										style="background: url(//cdn.dscount.com/images_2016/member/id_bg.png) repeat-x;"></td>
								</tr>
								<tr>
									<td align="center"
										style="padding-top: 40px; padding-bottom: 40px;">
										<form method="Post" action="${ctx}/findId.do" name="miss">
											<table border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td align="center">
															<table border="0" cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td><span class="ht_15 ct_b bt_40">이름</span></td>
																		<td style="padding-left: 20px;"><input
																			type="text" name="Name"
																			style="border: 1px solid #d9d9d9; color: #616161; FONT-FAMILY: 돋움; FONT-SIZE: 13px; width: 390px; height: 45px; line-height: 45px; vertical-align: middle; padding: 0px 0px 0px 10px;"></td>
																	</tr>
																	<tr>
																		<td height="12" colspan="2">&nbsp;</td>
																	</tr>
																	<tr>
																		<td><span class="ht_15 ct_b bt_40">이메일</span></td>
																		<td style="padding-left: 20px;"><input
																			type="text" name="email"
																			style="border: 1px solid #d9d9d9; color: #616161; FONT-FAMILY: 돋움; FONT-SIZE: 13px; width: 390px; height: 45px; line-height: 45px; vertical-align: middle; padding: 0px 0px 0px 10px;"></td>
																	</tr>
																</tbody>
															</table>

														</td>
													</tr>
													<tr>
														<td align="center"
															style="padding-top: 40PX; padding-bottom: 40PX;"><a
															href="javascript:MissID();"><img
																src="${ctx}/skin/images/m_08.jpg" border="0"></a></td>
													</tr>
													<tr>
														<td align="center"><span class="ht_13 ct_b bt_50">※
																아이디 찾기가 되지 않는 경우에는 고객센터(1577-6654)로 문의해 주시기 바랍니다.</span></td>
													</tr>
												</tbody>
											</table>
										</form>

									</td>
								</tr>
							</tbody>
						</table>


					</td>
				</tr>
			</tbody>
		</table>

	</center>

</body>
</html>