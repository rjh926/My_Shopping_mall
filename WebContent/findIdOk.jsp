<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>디스카운트 [다홍/지니프/크리마레 통합멤버쉽]</title>
<link rel="stylesheet" type="text/css"
	href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/top_javascript.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script>
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/jquery.cookie.js.다운로드"></script>
<script src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/cookie.js.다운로드"></script>


</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
	<center>
		<table width="100%" height="361" border="0" align="center"
			cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td align="center" valign="top">
						<table width="700" border="0" cellspacing="0" cellpadding="0"
							style="border: solid 2px #92ced9;">
							<tbody>
								<tr>
									<td align="center"
										style="background: #92ced9; padding-top: 20px; padding-bottom: 20px;">
										<table border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td align="center" style="padding-bottom: 15px;"><span
														class="ht_22 ct_b bt_60">회원 아이디 찾기</span></td>
												</tr>
												<tr>
													<td align="center"><span class="ht_13 ct_b bt_50">
															아이디나 비밀번호가 생각나지 않으세요?<br> 회원님의 개인정보를 안전하게 되찾으실 수 있도록
															도와드리겠습니다.
													</span></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td height="5"
										style="background: url(//cdn.dscount.com/images_2016/member/id_bg.png) repeat-x;"></td>
								</tr>
								<tr>
									<td align="center"
										style="padding-top: 100px; padding-bottom: 100px;">
										<table border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
												<c:choose>
													<c:when test="${userId eq null }">
													<td align="center"><span class="ht_18 ct_b">입력하신
															정보와 일치하는 회원아이디가 없습니다.</span><br></td>
													
													</c:when>
													
													<c:otherwise>
													<td align="center"><span class="ht_18 ct_b">
														회원님의 ID는 <b>${userId }</b>입니다
													</span><br></td>
													</c:otherwise>	
												</c:choose>
												</tr>
												<tr>
													<td style="padding-top: 50px;" align="center"><a
														href="javascript:Onclick=window.close()"><img
															src="${ctx}/skin/images/m_10.jpg" border="0"></a></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>
</html>