<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<!-- saved from url=(0051)http://pay.dscount.com/Order/BuyCartOrder_Guest.asp -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>상품주문</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/AceCounter_AW.js.다운로드"></script><script async="" src="${ctx}/skin/js/analytics.js.다운로드"></script><script src="${ctx}/skin/js/top_javascript.js.다운로드"></script><script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script><script src="${ctx}/skin/js/jquery.cookie.js.다운로드"></script><script src="${ctx}/skin/js/cookie.js.다운로드"></script>

<script src="${ctx}/skin/js/jquery.bpopup.min.js.다운로드"></script>


<script language="JavaScript">
<!--

$(document).ready(function() {
	$('#hidden_iframe').load(function() {
	  $(this)
		.contents()
		.find('#searchAddr')
		.bind('focus', function() {
			//  do stuff
			$('#zipcodeLayer').css({position:'absolute'});
			$(window).scrollTop(0);
		});
	  $(this)
		.contents()
		.find('#searchAddr')
		.bind('blur', function() {
			$('#zipcodeLayer').css({position:'fixed'});
		});
	});
});

function formatNumber(str)
{
	number = numOffMask(str.value);

	if (isNaN(number))
		str.value = "";
	else
		str.value = numOnMask(number);
}

function numOffMask(str)
{
	var tmp = str.split(",");
	tmp = tmp.join("");
	return tmp;
}

function numOnMask(str)
{

	if (str < 0) {
		str = Math.abs(str);
		sign = "-";
	} else {
		sign = "";
	}

	str = str + "";
	var idx = str.indexOf(".");

	if (idx < 0) {
		var txtInt = str;
		var txtFloat = "";
	} else {
		var txtInt = str.substring(0,idx);
		var txtFloat = str.substring(idx);
	}

	if (txtInt.length > 3) {
		var c=0;
		var myArray = new Array();
			for(var i=txtInt.length; i>0; i=i-3) {
			myArray[c++] = txtInt.substring(i-3,i);
 			}
		myArray.reverse();
 		txtInt = myArray.join(",");
 		}
 		str = txtInt + txtFloat;

	return sign + str;
}


function onlyNumber() 
{                          
	if((event.keyCode<48)||(event.keyCode>57) ) event.returnValue=false; 
}   


function onoff(nObj){
	if (nObj==1)
	{
		document.all.div_cashreceipt.style.display="block";
	}
	else
	{
		document.all.div_cashreceipt.style.display="none";
	}
} 
/*
function escrowonoff(nObj)
{
    var f = document.member;

	var SumMoney = 0;
	SumMoney = numOffMask(f.SumMoney.value);

	if (nObj==1)
	{
		document.all.div_escrow.style.display="block";
	}
	else
	{
		document.all.div_escrow.style.display="none";
	}
} 
*/

function payment_show(payment)
{
	if ( payment == 1 )
	{
		$("#div_input_card").show();
		$("#div_input_mobile").hide();
		$("#div_input_bank").hide();
		$("#div_input_online").hide();
		$("#div_input_escrow").hide();
	}
	else if ( payment == 2 )
	{
		$("#div_input_card").hide();
		$("#div_input_mobile").hide();
		$("#div_input_bank").show();
		$("#div_input_online").hide();
		$("#div_input_escrow").show();
	}
	else if ( payment == 3 )
	{
		$("#div_input_card").hide();
		$("#div_input_mobile").hide();
		$("#div_input_bank").hide();
		$("#div_input_online").show();
		$("#div_input_escrow").show();
	}
	else if ( payment == 9 )
	{
		$("#div_input_naver").show();
		$("#div_input_card").hide();
		$("#div_input_mobile").hide();
		$("#div_input_bank").hide();
		$("#div_input_online").hide();
		$("#div_input_escrow").hide();
	}
	else if ( payment == 10 )
	{
		$("#div_input_card").hide();
		$("#div_input_mobile").show();
		$("#div_input_bank").hide();
		$("#div_input_online").hide();
		$("#div_input_escrow").hide();
	}

}

function cashreceipt_info_onoff(nObj){
	if (nObj==1)
	{
		document.all.div_cashreceipt_info.style.display="block";
	}
	else
	{
		document.all.div_cashreceipt_info.style.display="none";
	}
} 

function cardCheck()
{
	alert("카드결제는 70,000원 이상에서만 가능합니다!");
	document.member.OrderFlag[1].checked = true;	
}

function senddata()
{
  document.member.r_name.value=document.member.member_name.value;
  document.member.r_zipcode.value=document.member.member_zipcode.value;
  document.member.r_addr1.value=document.member.member_addr1.value;
  document.member.r_addr2.value=document.member.member_addr2.value;
  document.member.r_hphone1.value=document.member.member_hphone1.value;
  document.member.r_hphone2.value=document.member.member_hphone2.value;
  document.member.r_hphone3.value=document.member.member_hphone3.value;
  document.member.r_cphone1.value=document.member.member_cphone1.value;
  document.member.r_cphone2.value=document.member.member_cphone2.value;
  document.member.r_cphone3.value=document.member.member_cphone3.value;
}

function deldata()
{
  document.getElementById('div_input_recieve').style.display = ""; 
  document.getElementById('div_input_foreign').style.display = "none"; 

  document.member.r_name.value="";
  document.member.r_zipcode.value="";
  document.member.r_addr1.value="";
  document.member.r_addr2.value="";
  document.member.r_hphone1.value="";
  document.member.r_hphone2.value="";
  document.member.r_hphone3.value="";
  document.member.r_cphone1.value="";
  document.member.r_cphone2.value="";
  document.member.r_cphone3.value="";
}
function checkmemo(arg)
{
	if(arg=="N")
	{
		document.all['div_input_memo'].style.display = "none"; 	
	}
	else
	{
		document.all['div_input_memo'].style.display = ""; 	
	}
}
	
function isNumeric(str) 
{	
		strNewName = str
		
		if(strNewName != null) {			
			for(intLoop = 0; intLoop < strNewName.length; intLoop++) {
				if(strNewName.charAt(intLoop) < "0" || strNewName.charAt(intLoop) > "9") {						
					return false;
				}				
			}			
			return true;
		}
		else
			return false;
}	
		
function FormOK(form) 
{		
	if ($('#shipping').val()=='1' || $('#shipping').val()=='2' || $('#shipping').val()=='3')
	{

		// 주문자 정보 체크 시작
		if ( $("input:radio[name='agree']:checked").val()== "N")
		{
			alert("비회원 정보 수집에 동의하셔야 합니다.");
			$("#agree1").focus();
			return;
		}

		if ( $.trim($("#member_name").val()) == "" )
		{
			alert("[주문자] 성명을 입력해 주십시요.");
			$("#member_name").focus();
			return;
		}

		if ( $.trim($("#member_zipcode").val()) == "" )
		{
			alert("[주문자] 우편번호를 검색하여 해당주소를 선택해 주십시요.");
			$("#member_zipcode").focus();
			return;
		}

		if ( $.trim($("#member_zipcode").val()) == "" )
		{
			alert("[주문자] 우편번호를 검색하여 해당주소를 선택해 주십시요.");
			$("#member_zipcode").focus();
			return;
		}

		if ( $.trim($("#member_addr2").val()) == "" )
		{
			alert("[주문자] 상세주소를 입력해 주십시요.");
			$("#member_addr2").focus();
			return;
		}

		if ( $.trim($("#member_hphone1").val()) == "" )
		{
			alert("[주문자] 일반전화 지역번호를 입력해 주십시요.");
			$("#member_hphone1").focus();
			return;
		}


		if ( $.trim($("#member_hphone2").val()) == "" )
		{
			alert("[주문자] 일반전화 지역번호를 입력해 주십시요.");
			$("#member_hphone2").focus();
			return;
		}

		if ( $.trim($("#member_hphone3").val()) == "" )
		{
			alert("[주문자] 일반전화 지역번호를 입력해 주십시요.");
			$("#member_hphone3").focus();
			return;
		}

		if ( $.trim($("#member_cphone1").val()) == "" )
		{
			alert("[주문자] 이동전화번호를 입력해 주십시요.");
			$("#member_cphone1").focus();
			return;
		}		
		
		if ( $.trim($("#member_cphone2").val()) == "" )
		{
			alert("[주문자] 이동전화번호를 입력해 주십시요.");
			$("#member_member_cphone2").focus();
			return;
		}

		if ( $.trim($("#member_cphone3").val()) == "" )
		{
			alert("[주문자] 이동전화번호를 입력해 주십시요.");
			$("#member_member_cphone3").focus();
			return;
		}

		if ( $.trim($("#member_email").val()) == "" )
		{
			alert("[주문자] 이메일 주소를 입력해 주십시요.");
			$("#member_email").focus();
			return;
		}	
		// 주문자 정보 체크 종료
		// 수취인 정보 체크 시작
		if($.trim($('#r_name').val()) == '') // 성명
		{
			alert('성명을 입력해 주세요');
			$('#r_name').focus();
			return;
		}
		if($.trim($('#r_zipcode').val()) == '') // 우편번호
		{
			alert('우편번호를 입력해 주세요');
			$('#r_zipcode').focus();
			return;
		}
		if($.trim($('#r_addr1').val()) == '') // 주소1
		{
			alert('주소를 입력해 주세요');
			$('#r_addr1').focus();
			return;
		}
		if($.trim($('#r_addr2').val()) == '') // 주소2
		{
			alert('주소를 입력해 주세요');
			$('#r_addr2').focus();
			return;
		}
		if($.trim($('#r_hphone1').val()) == '') // 전화번호1
		{
			alert('전화번호를 입력해 주세요');
			$('#r_hphone1').focus();
			return;
		}
		if($.trim($('#r_hphone2').val()) == '') // 전화번호2
		{
			alert('전화번호를 입력해 주세요');
			$('#r_hphone2').focus();
			return;
		}
		if($.trim($('#r_hphone3').val()) == '') // 전화번호3
		{
			alert('전화번호를 입력해 주세요');
			$('#r_hphone3').focus();
			return;
		}
		if($.trim($('#r_cphone1').val()) == '') // 이동전화1
		{
			alert('휴대폰 번호를 입력해 주세요');
			$('#r_cphone1').focus();
			return;
		}
		if($.trim($('#r_cphone2').val()) == '') // 이동전화2
		{
			alert('휴대폰 번호를 입력해 주세요');
			$('#r_cphone2').focus();
			return;
		}
		if($.trim($('#r_cphone3').val()) == '') // 이동전화3
		{
			alert('휴대폰 번호를 입력해 주세요');
			$('#r_cphone3').focus();
			return;
		}
		// 수취인 정보 체크 종료

	}
	else //해외배송
	{

		if($.trim($('#f_name').val()) == '') // 성명
		{
			alert('해외 수취인명을 입력해 주세요');
			$('#f_name').focus();
			return;
		}
		if($.trim($('#f_nation').val()) == '') // 국가명
		{
			alert('국가명을 입력해 주세요');
			$('#f_nation').focus();
			return;
		}
		if($.trim($('#f_zipcode').val()) == '') // 우편번호
		{
			alert('우편번호를 입력해 주세요');
			$('#f_zipcode').focus();
			return;
		}
		if($.trim($('#f_addr').val()) == '') // 영문주소
		{
			alert('영문주소를 입력해 주세요');
			$('#f_addr').focus();
			return;
		}
		if($.trim($('#f_phone').val()) == '') // 해외연락처
		{
			alert('해외연락처를 입력해 주세요');
			$('#f_phone').focus();
			return;
		}
		
	}

	// 사은품 확인
	if ( $("#giftdisplay").val()=="Y" )
	{

		if( $("input[name='pserial']:checked").length==0)
		{
			alert("사은품을 선택해주세요");
			return;
		}
	
	}

	// 결제정보 확인 체크
	if ( $("#confirm").is(":checked")==false )
	{
		alert("결제정보 확인 및 구매진행에 동의하셔야 합니다.");
		$("#confirm").focus();
		return;
	}

	$("#member").attr("action","http://pay.dscount.com/order/BranchOrder_Guest.asp").submit();


}


// 배송지 체크 
function check_shipping()
{
/*
  1. 주문자정보와 동일
  2. 직접 입력
  3. 최근 배송지
  4. 해외배송
*/

	var shipping_option;
	shipping_option=$("#shipping option:selected").val();

	switch (shipping_option)
	{
	case "1" :
		$("#div_shipping_info").load("shipping_info1.asp", function(responseTxt, statusTxt, xhr){
			if (statusTxt=="success")
			{
				$("#r_name").val($("#member_name").val());
				$("#r_zipcode").val($("#member_zipcode").val());
				$("#r_addr1").val($("#member_addr1").val());
				$("#r_addr2").val($("#member_addr2").val());
				$("#r_hphone1").val($("#member_hphone1").val());
				$("#r_hphone2").val($("#member_hphone2").val());
				$("#r_hphone3").val($("#member_hphone3").val());
				$("#r_cphone1").val($("#member_cphone1").val());
				$("#r_cphone2").val($("#member_cphone2").val());
				$("#r_cphone3").val($("#member_cphone3").val());
			}
		});
		break;
	case "2" :
		$("#div_shipping_info").load("shipping_info2.asp");
		break;
	case "4" :
		$("#div_shipping_info").load("shipping_info4.asp");
		break;
	}

}

// ##### 상세정보 펼치는 스크립트 시작 #######

// 총 주문금액 펼치거나 닫기
function slide_div_orderamount_info()
{
    $("#orderinfo_arrow").attr("src", "//cdn.dscount.com/images_2015/rows_" + ($("#div_orderamount_info").is(":visible") ? "down" : "up") + ".png");
	$("#div_orderamount_info").slideToggle('fast');
}

// 총 주문금액 펼치거나 닫기
function slide_div_orderdiscount_info()
{
    $("#discountinfo_arrow").attr("src", "//cdn.dscount.com/images_2015/rows_" + ($("#div_orderdiscount_info").is(":visible") ? "down" : "up") + ".png");
	$("#div_orderdiscount_info").slideToggle('fast');
}

// ##### 상세정보 펼치는 스크립트 종료 #######

function foreigndata()
{
  document.getElementById('div_input_recieve').style.display = "none"; 
  document.getElementById('div_input_foreign').style.display = ""; 
  
  document.member.f_name.value="";
  document.member.f_nation.value="";
  document.member.f_zipcode.value="";
  document.member.f_addr.value="";
  document.member.f_phone.value="";
}

function FormCancel() 
{
	document.member.reset();
}

// -->
</script>

<script src="${ctx}/skin/js/f.txt"></script><script src="${ctx}/skin/js/f(1).txt"></script><script src="${ctx}/skin/js/f(2).txt"></script></head><body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td align="center"><script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>


<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>

<script language="javascript">


function gotoItemMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
	
}

function gotoConceptGoodView(Gserial)
{
	location = "../Shopping/GoodView_Concept.asp?Gserial="+Gserial;	
	
}

function gotoCoordiView(Gserial)
{
	location = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	
}

function openCoordiView(Gserial)
{
	url = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
	
}

function gotoItemGood(arg)
{
	location = "../Shopping/ItemShopping_detail.asp?b="+arg;	
	
}

function gotoMDGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoCOSGoodView(Gserial)
{
	location = "../NShopping/GoodView_CItem.asp?Gserial="+Gserial;	
}

function gotoZinifGoodView(Gserial)
{
	location = "../NShopping/GoodView_ZItem.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotoMDGabalGoodView(Gserial, Gseq)
{
	location = "../Shopping/GoodView_Gabal.asp?Gserial="+Gserial+"&gseq=" + Gseq;	
}

function gotoNormalGoodView(Gserial)
{
	location = "../Shopping/GoodView_NItem.asp?Gserial="+Gserial;	
}


function gotoDahongGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
}

function openCatGoodView(Gserial)
{
	url =  "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function gotoBrandGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Brand.asp?Gserial="+Gserial;	
}

function gotoKeywordGoodView(Gserial)
{
	location = "../Nshopping/GoodView_Keyword.asp?Gserial="+Gserial;	
}


function gotoSaleGoodView(Gserial)
{
	location = "../Shopping/GoodView_Sale.asp?Gserial="+Gserial;	
}


function gotoItemGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoSummerGoodView(Gserial)
{
	location = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
}

function gotoCatGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotomonoGoodView(Gserial)
{
	location = "../Shopping2/GoodView_monomori.asp?Gserial="+Gserial;	
}

function gotoCatCoordiView(Gserial)
{
	location = "../Nshopping/GoodView_StyleBook.asp?Gserial="+Gserial;	
	
}


function gotoBigGoodView(Gserial)
{
	location = "../Shopping/GoodView_Big.asp?Gserial="+Gserial;	
}

function gotoItemShoppingMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
}


function gotoBigShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_BigDetail.asp?b="+arg;	
}


function gotoSaleShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_SaleDetail.asp?a="+arg;	
}



function openItemGoodView(Gserial)
{
	url = "/Nshopping/GoodView_Item.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function openDahongGoodView(Gserial)
{
	url = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function openSummerGoodView(Gserial)
{
	url = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function zoomPicture(arg)
{
   target = "../Shopping/zoomPicture.asp?Gserial=" + arg;
   window.open(target,"Picture","status=no,toolbar=no,scrollbars=no,resizable=no,width=780,height=780")
}

function gotoAlert()
{
	alert("경고창")
}

function addBookMark(){
window.external.AddFavorite('http://dahong.dscount.com', '디스카운트 [다홍/지니프/크리마레 통합멤버쉽]')
}


function bookmarksite(title,url) {
	if (window.sidebar) // firefox 
		window.sidebar.addPanel(title, url, ""); 
	else if(window.opera && window.print) // opera 
	{ 
		var elem = document.createElement('a'); 
		elem.setAttribute('href',url); 
		elem.setAttribute('title',title); 
		elem.setAttribute('rel','sidebar'); 
		elem.click(); 
	} 
	else if(window.external && ('AddFavorite' in window.external)) // ie
		window.external.AddFavorite(url, title);
	else if(window.chrome) // chrome
	{ 
		alert('ctrl+D 를 눌러서 북마크에 추가해주세요!'); 
	}
}


</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/images6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  

<script src="${ctx}/skin/js/banners.js.다운로드"></script>
<script src="${ctx}/skin/js/lnb.js.다운로드"></script>




<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->


<script language="javascript">
function div_promotion_onoff(state)
	{
		if (state==1)
		{
			document.getElementById('div_promotion').style.display='';
		}
		else
		{
			document.getElementById('div_promotion').style.display='none';
		}
	}

	function goto_promotion()
	{
		document.location.href='http://dahong.dscount.com/Nshopping/promotion_list.asp';
	}

</script>


    <script type="text/JavaScript">
<!--
function div_my_onoff(state)
{
	if (state==1)
	{
		$("#div_my").css('z-index','3000');
		document.getElementById('div_my').style.display='';
	}
	else
	{
		document.getElementById('div_my').style.display='none';
		$("#div_my").css('z-index','1000');
	}
}

function div_cs_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_cs').style.display='';
	}
	else
	{
		document.getElementById('div_cs').style.display='none';
	}
}

function div_dscountlayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dscountlayer').style.display='';
	}
	else
	{
		document.getElementById('div_dscountlayer').style.display='none';
	}
}


function div_dahonglayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dahonglayer').style.display='';
	}
	else
	{
		document.getElementById('div_dahonglayer').style.display='none';
	}
}

function div_monomorilayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_monomorilayer').style.display='';
	}
	else
	{
		document.getElementById('div_monomorilayer').style.display='none';
	}
}


function div_secondleedslayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_secondleedslayer').style.display='';
	}
	else
	{
		document.getElementById('div_secondleedslayer').style.display='none';
	}
}

function div_ziniflayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_ziniflayer').style.display='';
	}
	else
	{
		document.getElementById('div_ziniflayer').style.display='none';
	}
}

function div_creemarelayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_creemarelayer').style.display='';
	}
	else
	{
		document.getElementById('div_creemarelayer').style.display='none';
	}
}

function searchform_act()
{
	if (document.getElementById("layersearchform").style.display == 'none')
	{
		document.getElementById("layersearchform").style.display = '';
	}
	else
	{
		document.getElementById("layersearchform").style.display ='none';
	}
}

function div_menu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_menu').style.display='';
	}
	else
	{
		document.getElementById('div_menu').style.display='none';
	}
}

function div_allmenu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_allmenu').style.display='';
	}
	else
	{
		document.getElementById('div_allmenu').style.display='none';
	}
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>




 
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#eeeeee;">
  <tbody><tr>
    <td align="center" height="49">
<div style="position:relative; z-index:720; width:1185px;"> 
  <div style="position:absolute;top:49px;left:0px; width:100%;display:none" id="div_dscountlayer" onmouseout="div_dscountlayer_onoff(0)" onmouseover="div_dscountlayer_onoff(1)">
<!-- 내용 테이블 시작-->
<div style="position:relative; width:1185px; z-index:720; box-sizing: border-box;">
    <div width:1185px;="" style="position:absolute;top:0px;left:19px;box-sizing: border-box;">
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td><img src="${ctx}/skin/images/ta_back2.png"></td>
          </tr>
        </tbody></table>
    </div>
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td valign="top" style="box-sizing: border-box; width:1185px; background:#FFFFFF; border-left:1px solid #c4c4c4; border-right:1px solid #c4c4c4; border-bottom:1px solid #c4c4c4;">
<table style="width:1183px; box-sizing: border-box;" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" valign="top" style="padding:0px 0px 30px 32px;">
<!-- 다홍 카테고리-->
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:30px 0px 13px 0px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://dahong.dscount.com&#39;"><img src="${ctx}/skin/images/top_smalllogo_dahong.jpg" border="0"></td>
  </tr>
  <tr>
    <td><a href="http://dahong.dscount.com/Ncommunity/celeb_list.asp"><img src="${ctx}/skin/images/20171215_sban_d.gif" border="0"></a></td>
  </tr>
  <tr>
    <td align="left" style="padding:18px 0px 0px 0px;">
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/itemShopping_New.asp&#39;"><span class="tm_layer"><a><b>신상10%</b></a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" align="left" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/itemShopping_weekly.asp&#39;"><span class="tm_layer"><a><b>위클리베스트</b></a></span></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>

  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=41&#39;"><span class="tm_layer"><a>SUMMER&nbsp;&nbsp;<span class=" bt_60"><font color="#3399ff">NEW</font></span></a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=1&#39;"><span class="tm_layer"><a>TOP</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=318&#39;"><span class="tm_layer"><a>BLOUSE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=3&#39;"><span class="tm_layer"><a>OUTER</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=321&#39;"><span class="tm_layer"><a>SKIRT</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=4&#39;"><span class="tm_layer"><a>DRESS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=5&#39;"><span class="tm_layer"><a>PANTS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=6&#39;"><span class="tm_layer"><a>BAG / SHOES </a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=8&#39;"><span class="tm_layer"><a>ACC</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=9&#39;"><span class="tm_layer"><a>UNDERWEAR</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=751&#39;"><span class="tm_layer"><a>FITNESS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://dahong.dscount.com/Nshopping/ItemShopping_main.asp?a=719&#39;"><span class="tm_layer"><a>VAVI MELLO&nbsp;&nbsp;<span class="ct_p2 bt_60">NEW</span></a></span></td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
<!-- 다홍 카테고리-->
    </td>
    
    <td align="left" valign="top" style="padding:0px 0px 30px 32px;">
<!--  세컨드 리즈 카테고리-->
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:30px 0px 13px 0px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://secondleeds.dscount.com/&#39;"><img src="${ctx}/skin/images/top_smalllogo_secondleeds.jpg" border="0"></td>
  </tr>
  <tr>
    <td><a href="http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=108"><img src="${ctx}/skin/images/20180116_sban_se.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td align="left" style="padding:18px 0px 0px 0px;">
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/itemShopping_New.asp&#39;"><span class="tm_layer"><a><b>신상10%</b></a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" align="left" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/itemShopping_weekly.asp&#39;"><span class="tm_layer"><a><b>위클리베스트</b></a></span></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=104&#39;"><span class="tm_layer"><a>TOP</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=108&#39;"><span class="tm_layer"><a>BLOUSE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=111&#39;"><span class="tm_layer"><a>OUTER</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=116&#39;"><span class="tm_layer"><a>DRESS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=820&#39;"><span class="tm_layer"><a>SKIRT</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=118&#39;"><span class="tm_layer"><a>PANTS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=123&#39;"><span class="tm_layer"><a>BAG / SHOES </a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=126&#39;"><span class="tm_layer"><a>ACC</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://secondleeds.dscount.com/Nshopping/ItemShopping_main.asp?a=324&#39;"><span class="tm_layer"><a>SUMMER</a></span></td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
<!--  세컨드 리즈 카테고리-->
    </td>
    
    <td align="left" valign="top" style="padding:0px 0px 30px 32px;">
<!-- 지니프 카테고리-->
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:30px 0px 13px 0px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://zinif.dscount.com&#39;"><img src="${ctx}/skin/images/top_smalllogo_zinif.jpg" border="0"></td>
  </tr>
  <tr>
    <td><a href="http://zinif.dscount.com/NShopping/GoodView_Item.asp?Gserial=688905"><img src="${ctx}/skin/images/20180411_sban_z.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td style="padding:18px 0px 0px 0px;">
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/itemShopping_New.asp&#39;"><span class="tm_layer"><a><b>신상10%</b></a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/itemShopping_weekly.asp&#39;"><span class="tm_layer"><a><b>위클리베스트</b></a></span></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=139&#39;"><span class="tm_layer"><a>OUTER</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=144&#39;"><span class="tm_layer"><a>T-SHIRTS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=148&#39;"><span class="tm_layer"><a>SHIRTS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=151&#39;"><span class="tm_layer"><a>KNIT / CARDIGAN </a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=155&#39;"><span class="tm_layer"><a>PANTS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=161&#39;"><span class="tm_layer"><a>SHOES</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=170&#39;"><span class="tm_layer"><a>BAG</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_main.asp?a=175&#39;"><span class="tm_layer"><a>ACCESSORY</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/Shopping_specialsale.asp&#39;"><span class="tm_layer"><a>SALE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://zinif.dscount.com/Nshopping/ItemShopping_Keyword.asp?a=20&#39;"><span class="tm_layer"><a>BIGSIZE</a></span></td>
  </tr>
</tbody></table>


    </td>
  </tr>
</tbody></table>


<!-- 지니프 카테고리--> 
    </td>
    <td align="left" valign="top" style="padding:0px 32px 30px 32px;">
<!-- 크리마레 카테고리-->
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:30px 0px 13px 0px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://creemare.dscount.com&#39;"><img src="${ctx}/skin/images/top_smalllogo_cree.jpg" border="0"></td>
  </tr>
  <tr>
    <td><a href="http://creemare.dscount.com/Nshopping/ItemShopping_brand.asp?brand_seq=229"><img src="${ctx}/skin/images/20180201_sban_c.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td style="padding:18px 0px 0px 0px;">
<table width="187" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/brandshop_list.asp&#39;"><span class="tm_layer"><a><b>브랜드 리스트</b></a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/itemShopping_weekly.asp&#39;"><span class="tm_layer"><a><b>위클리베스트</b></a></span></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=509&#39;"><span class="tm_layer"><a>MAKEUP</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=518&#39;"><span class="tm_layer"><a>SKINCARE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=527&#39;"><span class="tm_layer"><a>HAIRCARE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=523&#39;"><span class="tm_layer"><a>BODYCARE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=530&#39;"><span class="tm_layer"><a>TOOL</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=611&#39;"><span class="tm_layer"><a>MENS</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=603&#39;"><span class="tm_layer"><a>PERFUME / CANDLE</a></span></td>
  </tr>
  <tr>
    <td class="tm_layer_pa01" onclick="location.href=&#39;http://creemare.dscount.com/Nshopping/ItemShopping_main.asp?a=632&#39;"><span class="tm_layer"><a>LIFE</a></span></td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>


<!-- 크리마레 카테고리-->
    </td>
    <td width="100%" align="center" valign="top" style="padding:0px 0px 30px 0px; background:#f8f8f8;">
<!--통합멤버쉽    -->
<table width="205" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:32px 0px 14px 0px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/my/myCouponBook_guest.asp&#39;"><span class="ht_14 ct_t01 bt_60">디스카운트 통합멤버쉽</span></td>
  </tr>
  <tr>
    <td align="left"><img src="${ctx}/skin/images/member_coupon.jpg" border="0"></td>
  </tr>
  <tr>
    <td style="padding:15px 0px 0px 0px;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left">
    <span class="ht_11 ct_4 bt_20 tl_15">
    신규회원 가입시 즉시 사용가능한<br>할인쿠폰 4장 지급
    </span></td>
  </tr>
  <tr>
    <td align="left" style="padding:15px 0px 5px 0px;">
    <span class="ht_13 ct_b bt_60">회원등급별 특별혜택</span> <br>
    </td>
  </tr>
  <tr>
    <td align="left">  
      <span class="ht_11 ct_4 bt_20 tl_15">
      등급별 <b>1~8%</b> 즉시할인<br>
      무료배송<br>
      10%생일쿠폰
      </span>
   </td>
  </tr><tr>
  </tr><tr>
    <td align="left" style="padding:15px 0px 0px 0px;">
    <span class="ht_11 ct_4 bt_20 tl_15">
    / 상시 할인쿠폰<br>
    / 구매금액별 쿠폰<br>
    / 포토리뷰 최대10만원
    </span>
    </td>
  </tr><tr>
    <td align="left" style="padding:18px 0px 0px 0px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/my/myCouponBook_guest.asp&#39;"><img src="${ctx}/skin/images/member_bt.jpg" border="0"></td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
<!--통합멤버쉽    -->
    </td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
</div>
<!-- 내용 테이블 종료-->
  </div>
<table width="1185" height="49" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td width="300" align="left">
    

<table border="0" cellspacing="0" cellpadding="0" height="49" onmouseover="div_dscountlayer_onoff(1)" onmouseout="div_dscountlayer_onoff(0)" style="cursor:pointer;cursor:hand;">
  <tbody><tr>
    <td onclick="location.href=&#39;http://www.dscount.com/&#39;"><img src="${ctx}/skin/images/ntop_all02.jpg" border="0"></td>
    <td><a href="http://www.dscount.com/my/myCouponBook_guest.asp"><img src="${ctx}/skin/images/btn_dscoupon02.jpg" border="0"></a></td>
  </tr>
</tbody></table>

    </td>

    <td width="885" align="right">
<!-- 로그인메뉴시작-->
<table height="49" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    
    <td style="padding:0px 10px 0px 0px; cursor:pointer;cursor:hand;" onclick="location.href=&#39;https://member.dscount.com/member/loginform.asp&#39;"><span class="ht_11 ct_b bt_60">로그인</span></td>
    <td style="padding:0px 10px 0px 10px; cursor:pointer;cursor:hand;" onclick="location.href=&#39;https://member.dscount.com/member/RegisterFormJob.asp&#39;"><span class="ht_11 ct_b bt_60">회원가입</span></td>
    
    <td>
    
<!-- 마이페이지-->
<div style="position:relative; z-index:740;">
  <div style="position:absolute;left:-15px;top:49px;display:none" id="div_my" onmouseout="div_my_onoff(0)" onmouseover="div_my_onoff(1)">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center"><img src="${ctx}/skin/images/top_arr2.png"></td>
  </tr>
  <tr>
    <td>
<table border="0" cellspacing="0" cellpadding="0" width="110" style="background:#ffffff; border:solid 1px #e2e2e2;">
  <tbody><tr>
    <td onclick="location.href=&#39;https://pay.dscount.com/customer/order_search.asp&#39;" align="center" style="padding-top:15px; padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">주문/배송조회</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/my/myMessage_qna.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">내 게시글 조회</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;https://member.dscount.com/my/ModifyForm.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">개인정보 수정</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/my/myCash.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">예치금 조회</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/my/myEmoney.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">적립금 조회</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/my/myCoupon.asp&#39;" align="center" style="padding-bottom:15px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">할인쿠폰 조회</span></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>
  </div>
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="49" style="padding:0px 10px 0px 10px;cursor:pointer;cursor:hand;" onmouseover="div_my_onoff(1)" onmouseout="div_my_onoff(0)" onclick="javascript:location=&#39;https://pay.dscount.com/my/MyOrder.asp&#39;"><span class="ht_11 ct_b bt_60">마이페이지</span><span class="ht_6 ct_b"> ▼</span></td>
  </tr>
</tbody></table>
</div>
<!-- 마이페이지-->
    
    </td>
    <td>
<!-- 고객센터-->
<div style="position:relative; z-index:750; ">
  <div style="position:absolute;left:-21px;top:49px;display:none" id="div_cs" onmouseout="div_cs_onoff(0)" onmouseover="div_cs_onoff(1)">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center"><img src="${ctx}/skin/images/top_arr2.png"></td>
  </tr>
  <tr>
    <td>
<table border="0" cellspacing="0" cellpadding="0" width="110" style="background:#FFFFFF; border:solid 1px #e2e2e2;">
  <tbody><tr>
    <td onclick="location.href=&#39;http://www.dscount.com/Ncommunity/dstory_list.asp&#39;" align="center" style="padding-top:15px; padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">이벤트</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/my/myCouponBook_guest.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">할인혜택</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://pay.dscount.com/customer/order_search.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">주문/배송조회</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/Ncs/message1.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">질문과답변</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/Ncs/FaqList.asp?Select=1&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">자주묻는질문</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/Ncs/notice.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">공지사항</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://pay.dscount.com/Nshopping/Shopping_OnlyYou.asp&#39;" align="center" style="padding-bottom:10px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">개인결제</span></td>
  </tr>
  <tr>
    <td onclick="location.href=&#39;http://www.dscount.com/NShopping/itemshopping_today.asp&#39;" align="center" style="padding-bottom:15px;cursor:pointer;cursor:hand;"><span class="ht_13 ct_g5">최근본상품</span></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>                  
  </div>
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="49" style="padding:0px 8px 0px 8px;;cursor:pointer;cursor:hand;" onmouseover="div_cs_onoff(1)" onmouseout="div_cs_onoff(0)" onclick="javascript:location=&#39;http://www.dscount.com/Ncs/message1.asp&#39;"><span class="ht_11 ct_b bt_60">고객센터</span><span class="ht_6 ct_b"> ▼</span></td>
  </tr>
</tbody></table>
</div>
<!-- 고객센터-->
    </td>
    <td style="padding:0px 6px 0px 10px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://pay.dscount.com/cart/mycart.asp&#39;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><img src="${ctx}/skin/images/ntop_001.png"></td>
    <td style="padding:0px 0px 0px 2px;"><span class="ht_13 bt_60"> (<span id="div_CartCnt" style="color:#b24545;">1</span>)</span></td>
  </tr>
</tbody></table>
    </td>
    <td style="padding:0px 0px 0px 10px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/my/myzzim2.asp&#39;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><img img src="${ctx}/skin/images/ntop_002.png"></td>
    <td style="padding:0px 0px 0px 2px;"><span class="ht_13 bt_60">(<span id="div_FavoritesCnt" style="color:#b24545;">0</span>)</span></td>
  </tr>
</tbody></table>
    </td>
<td align="right" valign="top" style="padding:9px 0px 0px 20px;"><script src="${ctx}/skin/js/top_search.js.다운로드"></script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">

	function SearchOk() 
	{
		if ( $.trim( $("#textfield").val() ) == "" )
		{
			alert("검색할 상품명을 입력해 주세요"); 
			$("#textfield").focus();
			return;
		}

		document.search.submit();
	}

	function isSerchEnter()
	{
		if (event.keyCode == 13) SearchOk()
	}


</script>


<!-- 상품 검색-->
<form name="search" id="search" method="post" action="http://www.dscount.com/Nshopping/reference_products.asp" style=" margin:0px 0px;">

<div class="main_title">
        <div class="searching_box" id="search_query">
            <div id="searching_select" class="searching_select">
                <a href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#" id="toggle_select" class="ss_default">통합검색</a>
                <ul id="toggle_option" class="ss_option">
                    <li><a name="T" href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#" onclick="return false;">통합검색</a></li>
                    <li><a name="D" href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#" onclick="return false;">다홍패션몰</a></li>
                    <!--<li><a name="O" href="#" onclick="return false;">모노모리</a></li>-->
                    <li><a name="L" href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#" onclick="return false;">세컨리즈</a></li>
                    <li><a name="Z" href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#" onclick="return false;">지니프</a></li>
                    <li><a name="R" href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#" onclick="return false;">크리마레</a></li>
                </ul>
            </div>

            <div>
                <input type="text" name="textfield" id="textfield" value="" class="inp_txt" style=" width:74px;ime-mode:active; background:#ffffff;" autocomplete="off">
                <input type="hidden" id="FilterSiteNo" name="FilterSiteNo" value="T">
            </div>
            <div class="bt">
                <input type="image" id="query-btn" class="query_bnt" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/sc_bt.png" alt="검색">
            </div>
        </div>
</div>

</form>
<!-- 상품검색--></td>
<!--
    <td>
<div style="position:relative;">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="cursor:pointer;cursor:hand; padding:0px 0px 0px 3px;" onclick="searchform_act()"><img src="//cdn.dscount.com/images_2016/top/ntop_003.png" /></td>
  </tr>
</table>
<div id="layersearchform" style=" position:absolute; right:0px; top:31px; z-index:730; display:none;">

</div>
</div>
    </td>				
-->
  </tr>
</tbody></table>
<!-- 로그인메뉴종료-->
    </td>
  </tr>
</tbody></table>
</div>
    </td>
  </tr>
</tbody></table>




<!-- 공통상단 종료 -->
    </td>
  </tr>
  <tr>
    <td align="center">

			<!-- 플로팅 메뉴 - 최근 본 상품 시작 -->
<div style="position:relative; width:1185px;">
  <div class="r_fl_menu" style="top: 10px;">







<table border="0" cellspacing="0" cellpadding="0" style="background:#FFFFFF; padding:15px;">
  <tbody><tr>
    <td align="center">
		<div id="div_rfmenu_info">
			<table border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td style="padding-bottom:10px;"><a href="http://creemare.dscount.com/Ncommunity/beautyspoiler_list.asp" onfocus="this.blur();"><img src="${ctx}/skin/images/fly_cree20180518.jpg" border="0"></a></td>
              </tr>
              
			  <tr>
				<td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/NShopping/itemshopping_today.asp&#39;">
					<div class="history_wrap">
						<div class="history_header" align="center" style=" padding-top:12px; padding-bottom:8px;">
							<span class="ht_12 ct_8 bt_50">오늘본상품</span><span class="ht_14 bt_b ct_3 tp_5">1</span>
						</div>
					</div>
				</td>
			  </tr>
			  <tr>
				<td>
				<!--최근본상품 테이블 시작-->
					<table border="0" cellspacing="0" cellpadding="0">
					  <tbody><tr>
						<td style="border-bottom:solid 1px #e2e2e2;"><img src="${ctx}/skin/images/show_up.jpg" class="prev" style="cursor: pointer;"></td>
					  </tr>
					  <!-- 상품 루프 시작-->
					  <tr>
						<td align="center" style="padding-top:6px; padding-bottom:6px;">
							<div class="recently_view_wrap">
								<ul>
								
										<li class="recently_view displayOn"><a href="javascript:GetGoodUrl(&#39;1&#39;, &#39;727301&#39;)"><img src="${ctx}/skin/images/727301_16631982_k3.jpg"></a></li>
									
								</ul>
							</div>
						</td>
					  </tr>
					  <!-- 상품 루프 종료-->
					  <tr>
						<td style="border-top:solid 1px #e2e2e2;"><img src="${ctx}/skin/images/show_down.jpg" class="next" style="cursor: pointer;"></td>
					  </tr>
					</tbody></table>
				  <!--최근본상품 테이블 종료-->
				</td>
			  </tr>
			  <tr>
				<td style="padding-top:15px;">
				  <a href="http://www.dscount.com/my/myCouponBook_guest.asp"><img src="${ctx}/skin/images/show_banner.jpg" border="0"></a>        </td>
			  </tr>
			</tbody></table>
		</div>
    </td>
  </tr>
  <tr>
    <td align="center" style="padding-top:12px;">
	<a href="http://pay.dscount.com/Order/BuyCartOrder_Guest.asp#top"><img src="${ctx}/skin/images/show_top.jpg" border="0"></a>
	</td>
  </tr>
  <tr>
    <td style="padding-top:10px;" align="center" onclick="slide_div_rfmenu_info()">
	
		<img src="${ctx}/skin/images/flyban_close.jpg" id="rfmenu_arrow" border="0" class="hands">
	
    </td>
  </tr>
</tbody></table>


</div>
</div>

			<script>
				$(".r_fl_menu").css('top','10px'); // fl_fixed 클래스에 important 를 넣어줘서 후에 변경된 top 42px가 우선이 되어야 하지만 fl_fixed의 top 0px가 우선시 되도록 변경처리함.
			</script>

			<!-- 플로팅 메뉴 - 최근 본 상품 종료 -->

    </td>
  </tr>
</tbody></table>
</td>
    </tr>
  </tbody></table>

<!-- 타이틀 -->
<table width="1185" height="140" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" valign="top" style="padding:50px 0px 0px 0px;"><span class="et_30 ct_b bt_50">DELIVERY / PAYMENT</span></td>
    <td align="right" valign="top" style="padding:30px 0px 0px 0px;"><img src="${ctx}/skin/images/obar_02.jpg"></td>
  </tr>
</tbody></table>
<!-- 타이틀-->


<form method="post" name="member" id="member">	
<div id="zipcodeLayer" style="width:455px; height:500px; background:#fff; z-index:9999; display:none; border:7px solid #ededed;"></div><table width="1185" border="0" cellspacing="0" cellpadding="0">
              
            

              <input type="hidden" name="Cash" value="0">
              <input type="hidden" name="basic_Emoney" value="0">
              <input type="hidden" name="CartCount" value="1">
              <input type="hidden" name="Division" value="G">
              <input type="hidden" name="Kind" value="1">
              <input type="hidden" name="GoodAmount" value="19800">
			  <input type="hidden" name="cartseq" value="28086242">
			  <input type="hidden" name="ExpressDelivery" value="N">

      <tbody><tr>
        <td style="padding-bottom:20px;" align="left"><span class="ht_14 bt_60">※ 비회원 정보 수집 동의</span></td>
      </tr>
  <tr>
    <td>
    
<textarea rows="8" cols="138" style="border: 1px solid #d9d9d9 ; width:1155px; color: #616161; FONT-FAMILY: 돋움; FONT-SIZE: 12px; padding:15px;" name="textarea" readonly="">비회원 개인정보 수집ㆍ이용 동의

[개인정보의 수집ㆍ이용 목적]
(유)디스카운트가 기획,운영하는 통합 쇼핑사이트인 디스카운트몰(http://www.dscount.com/) 및 다홍몰(http://www.dahong.dscount.com/) 및 지니프몰(http://www.zinif.dscount.com/) 및 
크리마레몰(http://creemare.dscount.com/)등의 패밀리사이트(이하 합쳐서 "몰"이라 한다)에서 개인정보 수집ㆍ이용 목적은 아래와 같습니다.

1.서비스 제공에 관한 계약이행: 요금정산, 배송, 고객상담 및 민원처리 
2.주문관리: 본인확인, 개인식별, 불량 이용자의 부정 이용방지, 서비스 이용연령 제한, 분쟁조정을 위한 기록보존, 불만처리, 고지사항 전달 
3.마케팅 활용: 서비스 이용 통계, 접속빈도 파악 
4.법령의 준수: 서비스와 관련하여 법령에 따른 기록의 보존 및 제공 

[수집하는 개인정보 항목]
"몰"이 개인정보를 수집하는 항목은 아래와 같습니다.

1.물품 주문 시: 주문자의 성명, 전화번호, 이메일, 주문비밀번호와 물품 수령인의 성명, 배송주소, 전화번호, 기타요구사항 
2.대금 결제 시: 은행계좌정보 또는 신용카드정보 
3.현금영수증 발급 시: 휴대폰번호 
4.주문정보 확인 시: IP주소, 방문일시 
5.비회원게시판 질의 시 : 이름, 이메일, 전화번호

[개인정보의 보유 및 이용 기간]
"몰"은 거래완결 시까지 및 관계법령에 따라 회사가 개인정보 및 기록보유 의무나 권한을 가진 기간 동안 개인정보를 보유하고 이용하게 됩니다.

1.전자상거래소비자보호법에 따른 정보보관 기한 
2.통신비밀보호법 따른 정보보관 기한 
3.국세기본법 따른 정보보관 기한 
4.기타 관계법령에 따른 정보보관 기한 
</textarea> 
                    
                    </td>
  </tr>

  <tr>
    <td style="padding:15px 0px 15px 0px;" align="center">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><input type="radio" name="agree" id="agree1" value="Y"><span class="ht_13"> 동의함</span></td>
    <td style="padding-left:15px;"><input type="radio" name="agree" id="agree2" value="N" checked=""><span class="ht_13"> 동의안함</span></td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr>
    <td height="20"></td>
  </tr>
   <tr>
    <td align="left" style=" padding:0px 0px 15px 0px;"><span class="ht_18 ct_b bt_50">주문상품 확인 (1)</span></td>
  </tr>
  <tr>
    <td align="center" style="background:#f3f3f3;padding:17px 0px 17px 0px; border-top:solid 1px #dedede; border-bottom:solid 1px #dedede;">
      <table width="1185" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>                            
         <td align="center" width="525"><span class="ht_13 bt_50">상품정보</span></td> 
         <td align="center" width="220"><span class="ht_13 bt_50">판매가</span></td>                                
         <td align="center" width="220"><span class="ht_13 bt_50">수량</span></td>                                
         <td align="center" width="220"><span class="ht_13 bt_50">적립금</span></td>                                                             
        </tr>
      </tbody></table>
    </td>
  </tr>
 <!-- 상품루프-->
                
  <tr>
    <td style="padding-top:18px; padding-bottom:18px; border-bottom:solid 1px #e2e2e2;">
    
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td width="525" align="left" style="padding-left:20px;">
<!-- 상품옵션테이블-->
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-right:20px;"><img src="${ctx}/skin/images/727301_16631982_k3.jpg" width="80"></td>
    <td align="left" valign="middle">
<table border="0" cellspacing="0" cellpadding="0">

  <tbody><tr>
    <td align="left"><span class="ht_14 ct_b bt_50">오픈 넥 스트라이프 셔츠</span></td>
  </tr>

  <tr>
    <td align="left" style="padding-top:15px;"><span class="ht_13 ct_br">옵션 : [ 블랙 / ONE SIZE ]</span></td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>
<!-- 상품옵션테이블-->
    </td>
    <td width="220" align="center">    
	
	<span class="ht_13 ct_b">19,800원</span> 
	
    </td>
    <td width="220" align="center"><span class="ht_13 ct_b">1개</span></td>
    <td width="220" align="center"><span class="ht_13 ct_b">비회원 주문</span></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
   
               
<!-- 상품루프--> 
  
   <tr>
    <td height="30"></td>
  </tr> 
   <tr>
    <td style="border:solid 1px #3f4248; padding-top:40px; padding-bottom:40px;" align="center">
<!--결제금액-->
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="26"><span class="ht_16 ct_g10" style="padding-right:20px;">총상품금액</span><span class="ht_18"><b>19,800</b>원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_12.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">할인금액</span><span class="ht_18 ct_p3"><b>0</b>원</span></td>
        <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_11.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">배송비</span><span class="ht_18"><b>2,500</b>원</span></td>
    <td style="padding-left:30px; padding-right:30px;"><img src="${ctx}/skin/images/c_13.png"></td>
    <td><span class="ht_16 ct_g10" style="padding-right:20px;">결제예정금액</span><span class="ht_18"><b>22,300</b>원</span></td>
  </tr>
</tbody></table>
<!--결제금액-->
    </td>
</tr>
   <!-- 당일배송문구-->
	
  <!-- 당일배송문구--> 
  
  
  <tr><td height="50"></td></tr>
  <tr>
    <td>
    
   </td>
  </tr>
  <tr>
    <td>
<!-- 주문자 정보 시작-->
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="background:#f3f3f3; padding: 15px 0px 15px 25px; border:solid 1px #e0e0e0;"><span class="ht_14 bt_60">주문자 정보</span></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center" style="padding:20px 0px 20px 0px; border-bottom:solid 1px #e0e0e0; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">성 명 </span></td>
    <td align="left"><input type="text" name="member_name" id="member_name" value="" maxlength="10" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC; FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
			
            <table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">우편번호</span></td>
				<td width="60" align="left"><input type="text" name="member_zipcode" id="member_zipcode" readonly="" value="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                </td>
				<td align="left">
			<table width="90" border="0" cellspacing="0" cellpadding="0">
			  <tbody><tr>
				<td height="29" align="center" style="border:1 solid #CCCCCC; cursor:pointer;cursor:hand;" bgcolor="#E3E3E3"><a href="javascript:openZipcodelayer(&#39;M&#39;);"><span class="ht_12 bt_60">우편번호 검색</span></a></td>
			  </tr>
			</tbody></table>

				</td>
			  </tr>
			</tbody></table>   

</td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">주소</span></td>
    <td align="left"><input type="text" name="member_addr1" id="member_addr1" readonly="" value="" size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC; FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">나머지 주소</span></td>
    <td align="left"><input type="text" name="member_addr2" id="member_addr2" value="" size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    </td>
  </tr>

  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">연락처</span></td>
    <td align="left">
							<input type="text" name="member_hphone1" id="member_hphone1" size="3" maxlength="3" value="" onkeypress="onlyNumber()" style="width:70px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
							- 
							<input type="text" name="member_hphone2" id="member_hphone2" size="4" maxlength="4" value="" onkeypress="onlyNumber()" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
							- 
							<input type="text" name="member_hphone3" id="member_hphone3" size="4" maxlength="4" value="" onkeypress="onlyNumber()" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"> 
    </td>
  </tr>
</tbody></table>    </td>
  </tr>

  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">핸드폰</span></td>
    <td align="left">
    
							<input type="text" name="member_cphone1" id="member_cphone1" size="3" maxlength="3" value="" onkeypress="onlyNumber()" style="width:70px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
							- 
							<input type="text" name="member_cphone2" id="member_cphone2" size="4" maxlength="4" value="" onkeypress="onlyNumber()" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
							- 
							<input type="text" name="member_cphone3" id="member_cphone3" size="4" maxlength="4" value="" onkeypress="onlyNumber()" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"> 
   </td>
  </tr>
</tbody></table>    </td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" align="left"><span class="ht_14">이메일 주소</span></td>
    <td align="left"><input type="text" name="member_email" id="member_email" value="" size="30" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
  </tr>
</tbody></table>    
</td>
  </tr>
</tbody></table>    

   </td>
  </tr>
</tbody></table>
<!-- 주문자 정보 종료-->
  </td>
  </tr>

<script type="" language="JavaScript">
<!--

function openZipcodelayer(mode)
{
	url = "/lib/zipcode_api.asp?mode=" + mode;
	$('#zipcodeLayer').bPopup({
		content:'iframe',
		iframeAttr:"name='hidden_iframe' id='hidden_iframe' frameborder='0' width='100%' height='100%'",
		loadUrl:url

	});

}

function closeZipcodelayer()
{
	$('#zipcodeLayer').bPopup().close();
	$('#zipcodeLayer').html('');

}
//-->	
</script> 


  <tr>
    <td style="padding-top:40px;">
    
<!-- 배송지 정보 시작-->
 <div id="div_input_recieve" style="">     
   
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="background:#f3f3f3; padding: 15px 0px 15px 25px; border:solid 1px #e0e0e0;"><span class="ht_14 bt_60">수취인 정보</span></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" align="center" style="padding:20px 0px 20px 0px; border-bottom:solid 1px #e0e0e0; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;">
    
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tbody><tr>
    <td bgcolor="#FFFFFF" align="center">
<table width="94%" border="0" cellspacing="0" cellpadding="5">

  <tbody><tr>
    <td width="150" align="left"><span class="m_title10"><span class="ht_14">배송지 선택</span></span></td>
    <td align="left"><label>
      <select name="shipping" id="shipping" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;" onchange="check_shipping()">
	  <option value="1">주문자 정보와 동일
	  </option><option selected="" value="2">직접입력
	  </option><option value="4">해외배송
      </option></select>
    </label></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  
<!-- 직적입력 시작-->  
  <tr>
    <td bgcolor="#FFFFFF" align="center" id="div_shipping_info">
    
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tbody><tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">성명</span></td>
				<td align="left">
                <input type="text" name="r_name" id="r_name" value="" size="10" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                </td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>

			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">우편번호</span></td>
				<td width="60" align="left">
				<input type="text" name="r_zipcode" id="r_zipcode" value="" readonly="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                </td>
				<td align="left">
			<table width="90" border="0" cellspacing="0" cellpadding="0">
			  <tbody><tr>
				<td height="29" align="center" style="border:1 solid #CCCCCC" bgcolor="#E3E3E3"><a href="javascript:openZipcodelayer(&#39;O&#39;);"><span class="ht_12 bt_60">우편번호 검색</span></a></td>
			  </tr>
			</tbody></table>

				</td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">주소</span></td>
				<td align="left"><input type="text" name="r_addr1" id="r_addr1" value="" size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;" readonly=""></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">나머지 주소</span></td>
				<td align="left"><input type="text" name="r_addr2" id="r_addr2" value="" size="30" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;"></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">전화번호</span></td>
				<td align="left">
						<input type="text" name="r_hphone1" id="r_hphone1" maxlength="3" onkeypress="onlyNumber()" size="3" value="" style="width:70px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                        - 
                        <input type="text" name="r_hphone2" id="r_hphone2" maxlength="4" onkeypress="onlyNumber()" size="4" value="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                        - 
                        <input type="text" name="r_hphone3" id="r_hphone3" maxlength="4" onkeypress="onlyNumber()" size="4" value="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
				</td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">이동전화</span></td>
				<td align="left">
						<input type="text" name="r_cphone1" id="r_cphone1" maxlength="3" onkeypress="onlyNumber()" size="3" value="" style="width:70px; height:30px;line-height:30px; border:1px solid #CCCCCC;FONT-SIZE: 9pt; padding-left:10px;">
                        - 
                        <input type="text" name="r_cphone2" id="r_cphone2" maxlength="4" onkeypress="onlyNumber()" size="4" value="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC; FONT-SIZE: 9pt; padding-left:10px;">
                        - 
                        <input type="text" name="r_cphone3" id="r_cphone3" maxlength="4" onkeypress="onlyNumber()" size="4" value="" style="width:100px; height:30px;line-height:30px; border:1px solid #CCCCCC; FONT-SIZE: 9pt; padding-left:10px;">
				</td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			  <tr>
				<td bgcolor="#FFFFFF" align="center">
				
			<table width="94%" border="0" cellspacing="0" cellpadding="5">
			  <tbody><tr>
				<td width="150" align="left"><span class="ht_14">배송메세지</span></td>
				<td align="left"><textarea name="memo" id="memo" rows="5" style="width:450px; border:1px solid #CCCCCC; 굴림; padding: 5px 5px 5px 5px; FONT-SIZE: 9pt;"></textarea></td>
			  </tr>
			</tbody></table>

				
				</td>
			  </tr>
			</tbody></table>
    
    </td>
  </tr>
<!-- 직적입력 종료--> 

</tbody></table>

    
    
    </td>
  </tr>
</tbody></table>
<!-- 배송지 정보 종료-->
</div>          
        
   </td>
  </tr>

  <tr>
    <td height="20"></td>
  </tr>
  <tr>
    <td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">주요 결제수단 선택</span></td>
      </tr>
      <tr>
       <td bgcolor="ffffff" align="center">
          <table width="97%" border="0" cellspacing="0" cellpadding="5">
           <tbody><tr>
            <td style="padding-left:21px;" align="left">
    <span class="ht_13">
                <input type="radio" name="OrderFlag" value="1" checked="" onclick="payment_show(1)"> 신용카드&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="OrderFlag" value="6" onclick="payment_show(2)"> 실시간계좌이체&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="OrderFlag" value="2" onclick="payment_show(3)" ;=""> 무통장입금&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="OrderFlag" value="10" onclick="payment_show(10)" ;="">휴대폰결제<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="OrderFlag" value="9" onClick="payment_show(9)">네이버페이-->
                </span>
            </td>
          </tr>
         </tbody></table>
      </td>
     </tr>
      <tr>
       <td bgcolor="ffffff" align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td style="padding-left:23px;" align="left"><span class="ht_13"><font color="333333">※ 판매중인 모든 상품은 현금결제시 에스크로 가능합니다.&nbsp;&nbsp;&nbsp;(에스크로 예치업 등록번호 0200600001)</font></span></td>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td style="padding-left:23px;" align="left">
    
<table width="100" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center" style="border:1 solid #CCCCCC" bgcolor="#E3E3E3" height="25"><span class="ht_12 bt_60"><a href="https://pgweb.uplus.co.kr/pg/wmp/mertadmin/jsp/mertservice/s_escrowYn.jsp?mertid=juzinif" target="_blank">가입사실 확인</a></span></td>
  </tr>
</tbody></table>

    
   </td>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
</tbody></table>
      </td>
     </tr>
</tbody></table>
  
    </td>
  </tr>
<tr><td height="20"></td></tr>
  <tr>
                  <input type="hidden" name="SumMoney" value="22300">
                  <input type="hidden" name="LuckQty" value="">
    <td>   

		  
		  
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">총 결제예정 금액</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center" onclick="slide_div_orderamount_info()">
  
  
  <table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="30%" height="35" class="td01" style="padding-left:25px;" align="left"><span class="ht_13 bt_60">총 주문금액</span></td>
    <td width="65%" height="35" align="right">
<input type="text" name="jumun_sum" size="15" readonly="" value="22,300" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
      </td>
    <td width="5%" align="center"><img src="${ctx}/skin/images/rows_down.png" id="orderinfo_arrow"></td>
  </tr>
</tbody></table>

    </td>
 </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
   <div id="div_orderamount_info" style="display: none">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style=" border-bottom: 1px solid #e0e0e0" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">상품금액</span></td>
    <td width="65%" align="right">
<input type="text" name="jumun_sum" size="15" readonly="" value="19,800" style="width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%">&nbsp;</td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
  <tr>
    <td align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">배송비</span></td>
    <td width="65%" align="right">
 <input type="text" name="jumun_sum" size="15" readonly="" value="2,500" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%">&nbsp;</td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>
</div>

    </td>
  </tr>
  <tr>
    <td bgcolor="ffffff" align="center" onclick="slide_div_orderdiscount_info()">
  
  
  <table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="30%" height="35" class="td01" style="padding-left:25px;" align="left"><span class="ht_13 bt_60">총 할인금액</span></td>
    <td width="65%" height="35" align="right">
<input type="text" name="totSaleAmount" id="totSaleAmount" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
      </td>
    <td width="5%" align="center"><img src="${ctx}/skin/images/rows_down.png" id="discountinfo_arrow"></td>
  </tr>
</tbody></table>

    </td>
 </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
   <div id="div_orderdiscount_info" style="display: none">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style=" border-bottom: 1px solid #e0e0e0" align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">쿠폰할인</span></td>
    <td width="65%" align="right">
<input type="text" name="discount_coupon" id="discount_coupon" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td style=" border-bottom: 1px solid #e0e0e0" align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">상품할인</span></td>
    <td width="65%" align="right">
<input type="text" name="discount_good" id="discount_good" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td align="center">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="5%" align="left" style="padding-left:26px;"><img src="${ctx}/skin/images/rows.jpg"></td>
    <td width="25%" align="left"><span class="ht_13 bt_60">적립금</span></td>
    <td width="65%" align="right">
<input type="text" name="discount_emoney" id="discount_emoney" size="15" readonly="" value="0" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#000000;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>
</div>
    </td>
  </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
  
  
  <table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="30%" height="35" class="td01" style="padding-left:25px;" align="left"><span class="ht_13 bt_60">총 결제하실 금액</span></td>
    <td width="65%" height="35" align="right">
<input type="text" name="SumMoney" size="15" readonly="" value="22,300" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
      </td>
    <td width="5%" align="center"></td>
  </tr>
</tbody></table>

    </td>
 </tr>
</tbody></table>
    
    </td>
  </tr>
  </tbody></table>  

   </td>
  </tr>
  
<tr>
<td>

              <!--신용카드-->
      
               <div id="div_input_card"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">신용카드</span></td>
    <td align="right">
    
<input type="text" class="logbox" name="card" size="15" readonly="" onfocus="this.select()" value="22,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14">  원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table> 


              </div>
              <!--신용카드-->
             
              <!--휴대폰결제-->
      
               <div id="div_input_mobile" style="display: none"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">휴대폰결제</span></td>
    <td align="right">

      <input type="text" class="logbox" name="mobile" size="15" readonly="" onfocus="this.select()" value="22,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>  
              </div>
<!--              <휴대폰결제> -->             
             
              <!--계좌이체-->
              <div id="div_input_bank" style="display: none"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">실시간계좌이체</span></td>
    <td align="right">
<input type="text" class="logbox" name="bank" size="15" readonly="" onfocus="this.select()" value="22,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>            







              </div>
              <!--계좌이체-->
              <!--무통장입금-->
              <div id="div_input_online" style="display: none"> 
<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">무통장입금</span></td>
    <td align="right">
<input type="text" class="logbox" name="online" size="15" readonly="" onfocus="this.select()" value="22,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14"> 원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
  <input type="hidden" name="cashreceipt" id="cashreceipt" value="N">
  <input type="hidden" name="cashreceipt_info" id="cashreceipt_info" value="">
</tbody></table> 
              
              
              

              </div>
              <!--무통장입금-->

              <!--네이버페이-->
      
               <div id="div_input_naver" style="display: none"> 

<table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>

<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
      <tbody><tr>
        <td height="40" bgcolor="#f3f3f3" style="padding-left:25px;" align="left"><span class="ht_14 bt_60">결제 수단</span></td>
      </tr>
  <tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_13 bt_60">네이버페이</span></td>
    <td align="right">
    
<input type="text" class="logbox" name="naver" size="15" readonly="" onfocus="this.select()" value="22,300" onchange="formatNumber(this)" onkeypress="onlyNumber()" style="border:0px;width:100px;height:24px;line-height:24px;vertical-align:middle; text-align:right;color:#F92B82;font-family: &#39;Nanum Gothic&#39;, sans-serif;font-size:14px;font-weight:bold;background-color:transparent;padding: 0px 0px 0px 0px;"><span class="ht_14">  원</span>
    </td>
    <td width="5%"></td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table> 


              </div>
              <!--신용카드-->

              <!--에스크로-->
              <div id="div_input_escrow" style="display: none"> 
 <table width="97%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
</tbody></table>                 
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="e0e0e0">
  <tbody><tr>
    <td bgcolor="ffffff" align="center">
    
<table width="97%" border="0" cellspacing="0" cellpadding="5">
  <tbody><tr>
    <td width="150" style="padding-left:9px;" align="left"><span class="ht_14 bt_60">에스크로 결제</span></td>
    <td align="left">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="ffffff">
  <tbody><tr>
    <td align="left"><span class="ht_14"><input name="escrowflag" type="radio" value="N" checked=""> 미신청</span> &nbsp;&nbsp;<span class="ht_14"><input name="escrowflag" type="radio" value="Y">신청</span></td>
  </tr>
  <tr>
    <td style="padding-top:10px;" align="left"><span class="ht_14">(배송확인시 공인인증서가 필요합니다.)</span></td>
  </tr>
</tbody></table>
    
    </td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
</tbody></table>
              
                  
              </div>
              <!--에스크로-->


</td>
</tr>
  
      

		  <!-- 기프트 시작-->

	<input type="hidden" name="giftdisplay" id="giftdisplay" value="N">
	<input type="hidden" name="giftcheck" id="giftcheck" value="E">
	<input type="hidden" name="giftstate" id="giftstate" value="해당없음">

  
  <tr>
    <td height="40"></td>
  </tr>
      <tr>
        <td align="center"><input type="checkbox" name="confirm" id="confirm" value="Y" style="width:22px; height:22px;"><span class="ht_18"> 결제정보를 확인하였으며, 구매진행에 동의합니다.</span></td>
      </tr>
      <tr>
        <td height="50"></td>
      </tr>  
    <tr>
    <td align="center">


<table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td style="cursor:pointer;cursor:hand;" onclick="javascript:history.go(-1)"><img src="${ctx}/skin/images/o_01.jpg"></td>
        <td width="10"></td>
        <td style="cursor:pointer;cursor:hand;" onclick="javascript:FormOK()"><img src="${ctx}/skin/images/o_02.jpg"></td>
      </tr>
    </tbody></table>


</td>
  </tr>
  
    <tr>
    <td height="150"></td>
  </tr>


</tbody></table>
</form>





  
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="border-top:solid 2PX #000000;" align="center">
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="52" style="border-bottom:solid 1PX #878787;">
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding-left:18px;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://pay.dscount.com/customer/order_search.asp&#39;"><span class="ht_11 ct_6 bt_40">주문조회</span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://member.dscount.com/introduction/introduction2.asp&#39;"><span class="ht_11 ct_b bt_60">개인정보 취급방침 </span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://member.dscount.com/introduction/introduction3.asp&#39;"><span class="ht_11 ct_6 bt_40">이용약관</span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/Ncs/notice.asp&#39;"><span class="ht_11 ct_6 bt_40">공지사항</span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://pay.dscount.com/Nshopping/Shopping_OnlyYou.asp&#39;"><span class="ht_11 ct_6 bt_40">개인결제</span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/introduction/Recruit.asp&#39;"><span class="ht_11 ct_6 bt_40">채용공고</span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://www.dscount.com/Ncommunity/Model_Register.asp&#39;"><span class="ht_11 ct_6 bt_40">모델지원</span></td>
    <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
    <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;mailto:webbox@dahong.co.kr&#39;"><span class="ht_11 ct_6 bt_40">제휴문의</span></td>
<!-- 크리마레 -->

<!-- 크리마레 -->
  </tr>
</tbody></table>    </td>
    <td align="left" style="padding-left:18px;">
    <table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td style="padding-right:20px;"><span class="ht_11 ct_8 bt_60">관련사이트</span></td>
        <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://dahong.dscount.com/&#39;"><span class="ht_11 ct_b bt_60">다홍</span></td>
        <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
        <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://secondleeds.dscount.com/&#39;"><span class="ht_11 ct_b bt_60">세컨리즈</span></td>
        <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
        <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://zinif.dscount.com/&#39;"><span class="ht_11 ct_b bt_60">지니프</span></td>
        <td style="padding-left:15px; padding-right:15px;"><span class="ht_12 ct_g2">|</span></td>
        <td style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://creemare.dscount.com/&#39;"><span class="ht_11 ct_b bt_60">크리마레</span></td>
      </tr>
    </tbody></table>
    </td>
    <td align="right">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
<!-- 디스카운트 

<table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td ><a href="https://www.facebook.com/dahongmall" target="_blank" onClick="_gaq.push(['_trackEvent', 'SNS', 'Click', 'Facebook_dscount']);"><img src="//cdn.dscount.com/images_2016/bottom/bottom_sns01.jpg" border="0" /></a></td>
    <td><a href="https://instagram.com/love_dahong/" target="_blank" onClick="_gaq.push(['_trackEvent', 'SNS', 'Click', 'Instagram_dscount']);"><img src="//cdn.dscount.com/images_2016/bottom/bottom_sns02.jpg" border="0"></a></td>
  </tr>
</table>

 디스카운트 -->

<!-- 다홍 -->

<!-- 다홍 -->

<!-- 모노모리 

 모노모리 -->

<!-- 세컨드리즈 sns다시만듬

세컨드리즈 -->

<!-- 지니프 -->

<!-- 지니프 -->

<!-- 크리마레 -->

<!-- 크리마레 -->    
</td>
  </tr>
</tbody></table>    
</td>
  </tr>
</tbody></table>

    </td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr>
    <td style="padding-top:30px; padding-bottom:40px;" align="center">
    
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td colspan="2" align="left" valign="top" style="padding-right:60px; padding-left:18px;"><a href="http://www.dscount.com/"><img src="${ctx}/skin/images/bottom_logo00.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 25px 0px 0px 16px;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
   <span class="ht_11 ct_9 tl_20">
(유)디스카운트<br> 
대표이사 : 이동환 &nbsp;&nbsp;&nbsp;
사업자등록번호 : 211-88-67647 &nbsp;&nbsp;&nbsp;
통신판매신고번호 : 제 2017-서울강남-01966 호<br>
주소 : 서울 강남 논현 200-7 어반하이브 10층 ( 교환 및 반품은 반드시 교환반품 주소로 보내주세요 )<br>
개인정보보호 책임자 : 박경서  <a href="http://ftc.go.kr/info/bizinfo/communicationView.jsp?apv_perm_no=2017322016230201966&amp;area1=&amp;area2=&amp;currpage=1&amp;searchKey=04&amp;searchVal=2118867647&amp;stdate=&amp;enddate=" target="_blank"><span class="ht_11 ct_8 tl_20">[ 사업자정보확인 ]</span></a>
</span>
    </td>
  </tr>
  <tr>
    <td>
    <table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-top:5px;">
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-right:30px;"><span class="et_13 ct_9 bt_40">dscount.com ⓒ all rights reserved.</span></td>
    <td><img src="${ctx}/skin/images/bottom_important.jpg" width="200" height="27"></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

    </td>
    <td align="left" valign="top" style="padding-top:25px;">
   <span class="ht_13 bt_60 tl_20">
/ 교환반품주소 <br>
</span>
<span class="ht_12 bt_60 tl_20 ">
서울특별시 종로구 종로 6 (서린동) 광화문우체국 소포실 디스카운트앞<br>
<br>
</span>
<span class="ht_13 bt_60 tl_20">
/ 콜센터</span> <span class="ht_16 bt_60 tl_20">1577-6654<br>
</span>
<span class="ht_11 ct_3 bt_40 tl_20">
평일. 10:00 ~ 17:00 / 점심. 12:00 ~ 13:00 /  주말,공휴일 휴무
</span>
    </td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr>
    <td align="center" height="50">
    </td>
  </tr>
</tbody></table>



<!-- 페이지 탑 시작 -->

<!-- 페이지 탑 종료 -->

<script language="javascript">
//	document.write(parent.document.referrer);
</script>

<!-- 애널리틱스 스크립트 분류 시작 -->

<!-- GA 공통 -->
<!-- 구글 애널리틱스 시작 -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89472410-1', 'auto');
  ga('create', 'UA-97548710-1', 'auto', 'clientTracker');
  
  ga('send', 'pageview');
  ga('clientTracker.send', 'pageview');

</script>
<!-- 구글 애널리틱스 종료 -->

<!-- 에이스카운터 시작 -->
<!-- AceCounter Log Gathering Script V.7.5.2017020801 -->
<script language="javascript">
	var _AceGID=(function(){var Inf=['gtc19.acecounter.com','8080','AS4A41393170138','AW','0','NaPm,Ncisy','ALL','0']; var _CI=(!_AceGID)?[]:_AceGID.val;var _N=0;var _T=new Image(0,0);if(_CI.join('.').indexOf(Inf[3])<0){ _T.src =( location.protocol=="https:"?"https://"+Inf[0]:"http://"+Inf[0]+":"+Inf[1]) +'/?cookie'; _CI.push(Inf);  _N=_CI.length; } return {o: _N,val:_CI}; })();
	var _AceCounter=(function(){var G=_AceGID;var _sc=document.createElement('script');var _sm=document.getElementsByTagName('script')[0];if(G.o!=0){var _A=G.val[G.o-1];var _G=(_A[0]).substr(0,_A[0].indexOf('.'));var _C=(_A[7]!='0')?(_A[2]):_A[3];var _U=(_A[5]).replace(/\,/g,'_');_sc.src=(location.protocol.indexOf('http')==0?location.protocol:'http:')+'//cr.acecounter.com/Web/AceCounter_'+_C+'.js?gc='+_A[2]+'&py='+_A[4]+'&gd='+_G+'&gp='+_A[1]+'&up='+_U+'&rd='+(new Date().getTime());_sm.parentNode.insertBefore(_sc,_sm);return _sc.src;}})();
</script>
<noscript><img src='http://gtc19.acecounter.com:8080/?uid=AS4A41393170138&je=n&' border='0' width='0' height='0' alt=''></noscript>	
<!-- AceCounter Log Gathering Script End -->
<!-- 에이스카운터 종료 -->

<!-- 다음 클릭스 다홍 리마케팅 시작 -->
<script type="text/javascript">
    var roosevelt_params = {
        retargeting_id:'cxyD351v9BvcAACD.Tw4QA00',
        tag_label:'_-5rcOeQRYSX7UagGJv98Q'
    };
</script>
<script type="text/javascript" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/roosevelt.js.다운로드" async=""></script>
<!-- 다음 클릭스 다홍 리마케팅 시작 -->

<!-- 다음 클릭스 지니프 리마케팅 시작 -->
<script type="text/javascript">
    var roosevelt_params = {
        retargeting_id:'aiIo4FoqPAa7CqdZKbfg-w00',
        tag_label:'u9tb4oBqSHesEDp7IoKb1w'
    };
</script>
<script type="text/javascript" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/roosevelt.js.다운로드" async=""></script>

<!-- 다음 클릭스 지니프 리마케팅 종료 -->

<!-- 구글 다홍 리마케팅 시작 -->
<!-- Google 리마케팅 태그 코드 -->
<!--------------------------------------------------
리마케팅 태그를 개인식별정보와 연결하거나 민감한 카테고리와 관련된 페이지에 추가해서는 안 됩니다. 리마케팅 태그를 설정하는 방법에 대해 자세히 알아보려면 다음 페이지를 참조하세요. http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1056158760;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/f(3).txt">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1056158760/?guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- 구글 다홍 리마케팅 종료 -->

<!-- 구글 지니프 리마케팅 시작 -->
<!-- Google 리마케팅 태그 코드 -->
<!--------------------------------------------------
리마케팅 태그를 개인식별정보와 연결하거나 민감한 카테고리와 관련된 페이지에 추가해서는 안 됩니다. 리마케팅 태그를 설정하는 방법에 대해 자세히 알아보려면 다음 페이지를 참조하세요. http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1045659792;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/f(3).txt">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1045659792/?guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- 구글 지니프 리마케팅 종료 -->

<!-- 구글 크리마레 리마케팅 시작 -->
<!-- Google 리마케팅 태그 코드 -->
<!--------------------------------------------------
리마케팅 태그를 개인식별정보와 연결하거나 민감한 카테고리와 관련된 페이지에 추가해서는 안 됩니다. 리마케팅 태그를 설정하는 방법에 대해 자세히 알아보려면 다음 페이지를 참조하세요. http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 965164166;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/f(3).txt">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/965164166/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- 구글 크리마레 리마케팅 종료 -->

<!-- 타게팅게이츠 시작 -->
<!-- WIDERPLANET  SCRIPT START 2017.4.3 -->
<div id="wp_tg_cts" style="display:none;"><script id="wp_id_script_1528070236538" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/wp.js.다운로드"></script><script id="wp_tag_script_1528070236648" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/wpc.php"></script></div>
<script type="text/javascript">
var wptg_tagscript_vars = wptg_tagscript_vars || [];
wptg_tagscript_vars.push(
(function() {
	return {
		wp_hcuid:"",   /*Cross device targeting을 원하는 광고주는 로그인한 사용자의 Unique ID (ex. 로그인 ID, 고객넘버 등)를 암호화하여 대입.
				*주의: 로그인 하지 않은 사용자는 어떠한 값도 대입하지 않습니다.*/
		ti:"23019",	/*광고주 코드*/
		ty:"Home",	/*트래킹태그 타입*/
		device:"web"	/*디바이스 종류 (web 또는 mobile)*/
		
	};
}));
</script>
<script type="text/javascript" async="" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/wp_astg_4.0.js.다운로드"></script>
<!-- // WIDERPLANET  SCRIPT END 2017.4.3 -->
<!-- 타게팅게이츠 종료 -->


<!-- 네이버 웹로그 분석 시작 -->
<!-- 공통 적용 스크립트 , 모든 페이지에 노출되도록 설치. 단 전환페이지 설정값보다 항상 하단에 위치해야함 --> 
<script type="text/javascript" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/wcslog.js.다운로드"> </script> 

<!-- 네이버 웹로그 다홍 -->

<script type="text/javascript"> 
if (!wcs_add) var wcs_add={};
wcs_add["wa"] = "s_51149f5b03ca";
if (!_nao) var _nao={};
wcs.checkoutWhitelist = ["m.dahong.dscount.com", "m.secondleeds.dscount.com","m.zinif.dscount.com", "m.creemare.dscount.com","m.pay.dscount.com", "m.dscount.com","dahong.dscount.com", "secondleeds.dscount.com","zinif.dscount.com", "creemare.dscount.com","pay.dscount.com","dscount.com","www.dscount.com"]; 
wcs.inflow("dscount.com");
wcs_do(_nao);
</script>

<!-- Withpang Tracker v3.0 [공용] start -->
<script type="text/javascript">
<!--
	function mobRf(){
  		var rf = new EN();
		rf.setSSL(true);
  		rf.sendRf();
	}
  //-->
</script>
<script async="true" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/enliple_min2.js.다운로드" onload="mobRf()"></script>
<!-- Withpang Tracker v3.0 [공용] end -->

<!-- 애널리틱스 스크립트 분류 종료 -->
</td>
    </tr>
</tbody></table>
  <iframe name="hidden_wnd" width="0" height="0" frameborder="0" src="./디스카운트 [다홍_지니프_크리마레 통합멤버쉽]_files/saved_resource.html"></iframe> 
</center>



</body></html>