<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<script type="text/javascript">
function setSelectDetail(value){
	var group = value;
	var sel_detail = document.getElementById('detailGroup');
	
	switch(group){
	case 'OUTER': 
		var newOpt = new Option('JACKET','JACKET');
		sel_detail.options[0]= newOpt;
		var newOpt = new Option('JUMPER','JUMPER');
		sel_detail.options[1]= newOpt;
		var newOpt = new Option('COAT','COAT');
		sel_detail.options[2]= newOpt;
		var newOpt = new Option('VEST','VEST');
		sel_detail.options[3]= newOpt;
		break;
	case 'T-SHIRT':
		var newOpt = new Option('LONGSLEEVE','LONGSLEEVE');
		sel_detail.options[0]= newOpt;
		var newOpt = new Option('SHORTSLEEVE','SHORTSLEEVE');
		sel_detail.options[1]= newOpt;
		var newOpt = new Option('SLEEVELESS','SLEVELESS');
		sel_detail.options[2]= newOpt;
		break;
	case 'SHIRT':
		var newOpt = new Option('PATTERN','PATTERN');
		sel_detail.options[0]= newOpt;
		var newOpt = new Option('BASIC','BASIC');
		sel_detail.options[1]= newOpt;
		break;
	case 'KNEET/CARDIGAN':
		var newOpt = new Option('KNEET','KNEET');
		sel_detail.options[0]= newOpt;
		var newOpt = new Option('CARDIGAN','CARDIGAN');
		sel_detail.options[1]= newOpt;
		break;
	case 'PANTS':
		var newOpt = new Option('PANTS','PANTS');
		sel_detail.options[0]= newOpt;
		var newOpt = new Option('JEAN','JEAN');
		sel_detail.options[1]= newOpt;
		var newOpt = new Option('SHORT-PANTS','SHORT-PANTS');
		sel_detail.options[2]= newOpt;
		break;
	}
	
}

function plzNaN() {
	
	var num = document.getElementById('price');
	
	if (isNaN(num.value)){
		alert('숫자를 입력하세요.');
		num.value ="";
	}
	
}

function plzNaN2() {
	
	var num2 = document.getElementById('inventory');
	
	if (isNaN(num2.value)){
		alert('숫자를 입력하세요.');
		num2.value ="";
	}
	
}

function submit(){
	var frm = document.getElementById('frm');
	if(frm.productName.value==""){
		alert('상품 이름을 입력하세요');
		frm.productName.focus();
		return;
	}
	else if(frm.productGroup.value==""){
		alert('상품 그룹을 선택하세요');
		return;
	}
	else if (frm.detailGroup.value=="") {
		alert("세부 그룹을 선택하세요");
		return;
	}
	else if (frm.color.value=="") {
		alert("색상을 입력하세요");
		frm.color.focus();
		return;
	}
	else if (frm.productSize.value=="") {
		alert('사이즈를 입력하세요');
		return;
	}
	else if (frm.price.value=="") {
		alert('가격을 입력하세요');
		frm.price.focus();
		return;
	}
	else if (frm.detail.value=="") {
		alert('상품설명을 입력하세요');
		frm.detail.focus();
		return;
	}
	else if(frm.inventory.value==""){
		alert('상품 보유량를 입력하세요');
		frm.inventory.focus();
		return;
	}
	else{
		frm.submit();
	}
	
}
</script>

<c:if test="${admin eq null}">
<script type="text/javascript">
alert('관리자 전용 페이지입니다.');
location.href='${ctx}';
		</script>
</c:if>
<%@include file="header.jsp" %>
	
	
	
		<form class="form-inline" action="${ctx}/product/modify.do" method="post" enctype="multipart/form-data" id="frm">
		<input type="hidden" name ="id" value="${product.productId }">
		<table class="table" width ="70%">
		
		<tr height="50">
			<td width="10%"><h4>상품명 :</h4></h4></td> <td><input class="form-control" type="text" name="productName" placeholder="상품명" value="${product.productName }"></h4></td></tr>
		<tr height="50">
			<td><h4>상품그룹명 : </h4></td> <td><h4><select class ="form-control" name="productGroup"  onchange="setSelectDetail(value);">
					<option value= "${product.productGroup}">${product.productGroup}</option>
					<option value="OUTER">OUTER</option>
					<option value="T-SHIRT">T-Shirt</option>
					<option value="SHIRT">Shirt</option>
					<option value="KNEET/CARDIGAN">KNEET/CARDIGAN</option>
					<option value="PANTS">PANTS</option></select>
					</h4></td></tr>
		<tr height="50">
			<td><h4>분류명 :</h4></td><td><h4> <select class ="form-control" name="detailGroup" id ="detailGroup">
				<option value="${product.detailGroup}">${product.detailGroup}</option><
				</select>
				</h4></td></tr>
					
			<tr height="50">
			<td><h4>색상 :</h4></td><td><h4> <input class="form-control" type="text" name="color" placeholder="색상을 입력하세요" value="${product.color }"></h4></td>
			</tr>
			<tr height="50">
				<td><h4>상품사이즈 :</h4></td><td><h4> <select class ="form-control"  id="productSize" name="productSize">
				<option value="${product.productSize }">${product.productSize }</option> 
				<option value="ONE-SIZE">ONE-SIZE</option>
				<option value="XXL">XXL</option>
				<option value="XL">XL</option>
				<option value="L">L</option>
				<option value="M">M</option>
				<option value="S">S</option>
				</select></h4></td></tr>
			<tr height="50">
			<td><h4>상품가격 :</h4></td><td><h4> <input class="form-control" type="text" name="price" id="price" onkeyup="plzNaN();" placeholder="상품 가격" value="${product.price }"></h4></td></tr>
			
			<tr height="50">
			<td><h4>상품설명 : </h4></td> <td><h4><textarea  class ="form-control" rows="5" cols="50" name="detail">${product.detail }</textarea></h4></td></tr>
			<tr height="50">
			<td><h4>상품 재고 :</h4></td><td><input class="form-control" type="text" name="inventory" id="inventory" onkeyup="plzNaN2();" placeholder="보유 재고량" value="${product.inventory }"></td></tr>
			<tr height="50">
			<td><h4>상품 이미지파일 : </h4></td> <td><input class="form-control" type="file" name="file" ></td></tr>
			</table>
		</form>
		<center> <button class="btn btn-success" id="submit" onclick="submit();">변경</button>
				 <button type="button" class="btn btn-danger" onclick="history.back();">취소</button>
		</center>
		
		
	
	
	<%@include file="footer.jsp" %>
</body>
</html>
