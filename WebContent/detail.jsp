<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>상품정보 : ${product.productName }</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/AceCounter_AW.js.다운로드"></script><script async="" src="${ctx}/skin/js/analytics.js.다운로드"></script><script src="${ctx}/skin/js/top_javascript.js.다운로드"></script><script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script><script src="${ctx}/skin/js/jquery.cookie.js.다운로드"></script><script src="${ctx}/skin/js/cookie.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.bpopup.min.js.다운로드"></script>
<script src="${ctx}/skin/js/jquery.elevatezoom.js.다운로드"></script>
<script>
if (typeof jQuery == 'undefined') {
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "/lib/jquery.min.1.7.2.js";
    document.getElementsByTagName('head')[0].appendChild(script);
}
</script>

<script>

function chg_cart_ea(str)
{
	
	var number = parseInt(document.getElementById('Gqty').value);

	var step =1;
	var min = 1;
	var max = 99;
	
	
	
	if (isNaN(number) || number.value == '') {
		alert ("구매수량은 숫자만 가능합니다");
		document.getElement.value=step;
		obj.focus();
	} else {
		if(str=='up'&& number<max)
			{
			number = number+step;
			
			document.getElementById('Gqty').value = number;
			return;
			}
		else if(str=='dn'&&number>min){
			number = number-step;
			document.getElementById('Gqty').value = number;
			return;
		}

	}
}
</script>


<script language="JavaScript">





function DirectOrderOk() 
{		
	Gcnt= document.send_order.Gcnt.value
	if(document.send_order.chkStock.value == "Y") 
	{
		if(document.send_order.Gqty.value<1)
		{
			alert("상품 수량을 1이상으로 선택하세요.");		
			document.send_order.Gqty.focus();
			return;
		}

		if(document.send_order.Gcolor.value == "No") 
		{
			alert("상품 색상을 선택하세요.");		
			document.send_order.Gcolor.focus();
			
		}
		if(document.send_order.Gsize.value=="No"){
			alert("상품 사이즈를 선택하세요.");
			document.sent_order.Gsize.focus();
			
		}

		
		document.send_order.submit();	
	}
	else
	
	alert("죄송합니다. 본 상품은 판매중지된 상품입니다.");
}

function CartOk(Name) 
{		
	Gcnt= document.send_order.Gcnt.value
	if(document.send_order.chkStock.value == "Y") 
	{
		if(document.send_order.Gqty.value<1)
		{
			alert("상품 수량을 1이상으로 선택하세요.");		
			document.send_order.Gqty.focus();
			return;
		}

		if(document.send_order.Gcolor.value == "No") 
		{
			alert("상품 색상을 선택하세요.");		
			document.send_order.Gcolor.focus();
			
		}
		if(document.send_order.Gsize.value=="No"){
			alert("상품 사이즈를 선택하세요.");
			document.send_order.Gsize.focus();
			
		}

		document.send_order.action='${ctx}/myCart/regist.do';
		document.send_order.submit();
		
			
	}
	else
	{
		alert("죄송합니다. 본 상품은 판매중지된 상품입니다.");
	}
}

function SetCartOk()
{		
	Gcnt= document.send_order.Gcnt.value
	if(document.send_order.chkStock.value == "Y") 
	{
		if(document.send_order.Gqty.value<1)
		{
			alert("본상품 수량을 1이상으로 선택하세요.");		
			document.send_order.Gqty.focus();
			return;
		}

		if(document.send_order.gBarCode.value == "") 
		{
			alert("본상품 색상/사이즈를 선택하세요.");		
			document.send_order.gBarCode.focus();
			return;
		}			

		document.send_order.target = "innerJob";
		document.send_order.action = "/Cart/SetAddCart.asp";
		document.send_order.submit();	

	}
	else
	{
		alert("죄송합니다. 본 상품이 판매중지된 상품입니다.\n코디상품은 별도 페이지에서 구매하시기 바랍니다.");
	}
}


function SetDirectOrderOk() 
{		
	Gcnt= document.send_order.Gcnt.value
	if(document.send_order.chkStock.value == "Y") 
	{
		if(document.getElementById('Gqty').value<1)
		{
			alert("본상품 수량을 1이상으로 선택하세요.");		
			document.send_order.Gqty.focus();
			return;
		}

		if(document.getElementById('Gcolor').value =='No' || document.getElemetById('Gsize').value == "No") 
		{
			alert("본상품 색상/사이즈를 선택하세요.");		
			document.send_order.gBarCode.focus();
			return;
		}		

		
		document.send_order.submit();
	}
	else
	
	alert("죄송합니다. 본 상품은 판매중지된 상품입니다.");
}

function ShowSize()
{
   target = "size.htm";
   window.open(target,"size","status=no,toolbar=no,scrollbars=no,resizable=yes,width=460,height=110")
}

function layerPopWishListOnOff(nVal)
{
	if (nVal==0)
	{
		document.all['layerPopWishList'].style.display = "none"; 
	}
	else
	{
		document.all['layerPopWishList'].style.display = ""; 
	}
}

function layerPopSetWishListOnOff(nVal)
{
	if (nVal==0)
	{
		document.all['layerPopSetWishList'].style.display = "none"; 
	}
	else
	{
		document.all['layerPopSetWishList'].style.display = ""; 
	}
}

function layerPopCartOnOff(nVal)
{
	if (nVal==0)
	{
		document.all['layerPopCart'].style.display = "none"; 
	}
	else
	{
		document.all['layerPopCart'].style.display = ""; 
	}
}

function layerPopSetCartOnOff(nVal)
{
	if (nVal==0)
	{
		$('#layerPopSetCart').bPopup().close();
//		document.all['layerPopSetCart'].style.display = "none"; 
	}
	else
	{
		$('#layerPopSetCart').bPopup();
		//document.all['layerPopSetCart'].style.display = ""; 
	}
}

function num_only( Ev )
{
	if (window.event) // IE코드
		var code = window.event.keyCode;
	else // 타브라우저
		var code = Ev.which;

	if ((code > 34 && code < 41) || (code > 47 && code < 58) || (code > 95 && code < 106) || code == 8 || code == 9 || code == 13 || code == 46)
	{
		window.event.returnValue = true;
		return;
	}

	if (window.event)
		window.event.returnValue = false;
	else
		Ev.preventDefault();	
}


function ConverseCoupon(gamount,saleamount,maniasaleamount)
{
	var diffamount = gamount-maniasaleamount;
	var diffamount2 = saleamount-maniasaleamount;
	var head = document.getElementById("ConverseCoupon");
	alert("즉시 할인 쿠폰이 적용되었습니다!");
	head.innerHTML = "<span class='style1'>"+Display(maniasaleamount)+"원 (회원등급할인 "+Display(diffamount2)+"원 포함)</span><br><font color='blue'>[적용된 할인금액 : " + Display(diffamount) + "원]</font>";
}

function Display(displaynumber)
{
	displaynumber=displaynumber+"";
    var str = displaynumber.replace(/(\d)(?=(?:\d{3})+(?!\d))/g,'$1,');
    return str;
}



function viewitem01_act()
{
	if (document.getElementById("viewitem01_layer").style.display == 'none')
	{
		document.getElementById("viewitem01_layer").style.display = '';
	}
	else
	{
		document.getElementById("viewitem01_layer").style.display ='none';
	}
}

function viewitem02_act()
{
	if (document.getElementById("viewitem02_layer").style.display == 'none')
	{
		document.getElementById("viewitem02_layer").style.display = '';
	}
	else
	{
		document.getElementById("viewitem02_layer").style.display ='none';
	}
}

function gotoitem03_act()
{
	location = "#PostScriptArea";
	document.getElementById("viewitem04_layer").style.display ='none';
	viewitem03_act();
}


function gotoitem04_act()
{
	location ="#QAScriptArea";
	document.getElementById("viewitem03_layer").style.display ='none';
	viewitem04_act();
}



function viewitem03_act()
{
	if (document.getElementById("viewitem03_layer").style.display == 'none')
	{
		document.getElementById("viewitem03_layer").style.display = '';
	}
	else
	{
		document.getElementById("viewitem03_layer").style.display ='none';
	}
}


function viewitem04_act()
{
	if (document.getElementById("viewitem04_layer").style.display == 'none')
	{
		document.getElementById("viewitem04_layer").style.display = '';
	}
	else
	{
		document.getElementById("viewitem04_layer").style.display ='none';
	}
}


function viewitem05_act()
{
	if (document.getElementById("viewitem05_layer").style.display == 'none')
	{
		document.getElementById("viewitem05_layer").style.display = '';
	}
	else
	{
		document.getElementById("viewitem05_layer").style.display ='none';
	}
}


function ActLayerAfter(pIdx, MaxCnt) {
	if (document.getElementById(pIdx).style.display == "none") {
			 for (var i=1; i<=MaxCnt; i++){
						 document.getElementById("viewAfter_"+i).style.display = "none";
				}
			document.getElementById(pIdx).style.display = ""; 
	} else {
		document.getElementById(pIdx).style.display = "none";
	}	
 } 

function ActLayerQA(pIdx, MaxCnt, strCheck) 
{
	if (strCheck=="N")
	{
		alert("해당 게시물 질문자만 내용을 볼 수 있습니다");
	}
	else
	{
		if (document.getElementById(pIdx).style.display == "none") 
		{
			 for (var i=1; i<=MaxCnt; i++)
			 {
				document.getElementById("viewQA_"+i).style.display = "none";
			 }
			document.getElementById(pIdx).style.display = ""; 
		} 
		else 
		{
			document.getElementById(pIdx).style.display = "none";
		}	
	}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

</script>



<style type="text/css">
<!--
.style1 {
	color: #f92b82;
	font-weight: bold;
}
-->
</style>

<script src="${ctx}/skin/js/f.txt"></script><script src="${ctx}/skin/js/f(1).txt"></script><script src="${ctx}/skin/js/f(2).txt"></script></head>




<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>



  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td>
        <script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>


<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>

<script language="javascript">


function gotoItemMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
	
}

function gotoConceptGoodView(Gserial)
{
	location = "../Shopping/GoodView_Concept.asp?Gserial="+Gserial;	
	
}

function gotoCoordiView(Gserial)
{
	location = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	
}

function openCoordiView(Gserial)
{
	url = "../Shopping/GoodView_Coordi.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
	
}

function gotoItemGood(arg)
{
	location = "../Shopping/ItemShopping_detail.asp?b="+arg;	
	
}

function gotoMDGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoCOSGoodView(Gserial)
{
	location = "../NShopping/GoodView_CItem.asp?Gserial="+Gserial;	
}

function gotoZinifGoodView(Gserial)
{
	location = "../NShopping/GoodView_ZItem.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotoMDGabalGoodView(Gserial, Gseq)
{
	location = "../Shopping/GoodView_Gabal.asp?Gserial="+Gserial+"&gseq=" + Gseq;	
}

function gotoNormalGoodView(Gserial)
{
	location = "../Shopping/GoodView_NItem.asp?Gserial="+Gserial;	
}


function gotoDahongGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
}

function openCatGoodView(Gserial)
{
	url =  "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function gotoBrandGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Brand.asp?Gserial="+Gserial;	
}

function gotoKeywordGoodView(Gserial)
{
	location = "../Nshopping/GoodView_Keyword.asp?Gserial="+Gserial;	
}


function gotoSaleGoodView(Gserial)
{
	location = "../Shopping/GoodView_Sale.asp?Gserial="+Gserial;	
}


function gotoItemGoodView(Gserial)
{
	location = "../NShopping/GoodView_Item.asp?Gserial="+Gserial;	
}

function gotoSummerGoodView(Gserial)
{
	location = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
}

function gotoCatGoodView(Gserial)
{
	location = "../Shopping2/GoodView_Cat.asp?Gserial="+Gserial;	
}

function gototeamGoodView(Gserial)
{
	location = "../NShopping/GoodView_team.asp?Gserial="+Gserial;	
}

function gotomonoGoodView(Gserial)
{
	location = "../Shopping2/GoodView_monomori.asp?Gserial="+Gserial;	
}

function gotoCatCoordiView(Gserial)
{
	location = "../Nshopping/GoodView_StyleBook.asp?Gserial="+Gserial;	
	
}


function gotoBigGoodView(Gserial)
{
	location = "../Shopping/GoodView_Big.asp?Gserial="+Gserial;	
}

function gotoItemShoppingMain(arg)
{
	location = "../Shopping/ItemShopping_main.asp?a="+arg;	
}


function gotoBigShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_BigDetail.asp?b="+arg;	
}


function gotoSaleShoppingDetail(arg)
{
	location = "../Shopping/ItemShopping_SaleDetail.asp?a="+arg;	
}



function openItemGoodView(Gserial)
{
	url = "/Nshopping/GoodView_Item.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}

function openDahongGoodView(Gserial)
{
	url = "../Shopping2/GoodView_Dahong.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function openSummerGoodView(Gserial)
{
	url = "../NShopping/GoodView_Summer.asp?Gserial="+Gserial;	
	window.open(url,'_blank');
}


function zoomPicture(arg)
{
   target = "../Shopping/zoomPicture.asp?Gserial=" + arg;
   window.open(target,"Picture","status=no,toolbar=no,scrollbars=no,resizable=no,width=780,height=780")
}

function gotoAlert()
{
	alert("경고창")
}

function addBookMark(){
window.external.AddFavorite('http://dahong.dscount.com', '디스카운트 [다홍/지니프/크리마레 통합멤버쉽]')
}


function bookmarksite(title,url) {
	if (window.sidebar) // firefox 
		window.sidebar.addPanel(title, url, ""); 
	else if(window.opera && window.print) // opera 
	{ 
		var elem = document.createElement('a'); 
		elem.setAttribute('href',url); 
		elem.setAttribute('title',title); 
		elem.setAttribute('rel','sidebar'); 
		elem.click(); 
	} 
	else if(window.external && ('AddFavorite' in window.external)) // ie
		window.external.AddFavorite(url, title);
	else if(window.chrome) // chrome
	{ 
		alert('ctrl+D 를 눌러서 북마크에 추가해주세요!'); 
	}
}


</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/images6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  
<script src="${ctx}/skin/js/banners.js.다운로드"></script>



<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->





    <script type="text/JavaScript">
<!--
function div_my_onoff(state)
{
	if (state==1)
	{
		$("#div_my").css('z-index','3000');
		document.getElementById('div_my').style.display='';
	}
	else
	{
		document.getElementById('div_my').style.display='none';
		$("#div_my").css('z-index','1000');
	}
}

function div_cs_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_cs').style.display='';
	}
	else
	{
		document.getElementById('div_cs').style.display='none';
	}
}

function div_dscountlayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dscountlayer').style.display='';
	}
	else
	{
		document.getElementById('div_dscountlayer').style.display='none';
	}
}


function div_dahonglayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dahonglayer').style.display='';
	}
	else
	{
		document.getElementById('div_dahonglayer').style.display='none';
	}
}

function div_monomorilayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_monomorilayer').style.display='';
	}
	else
	{
		document.getElementById('div_monomorilayer').style.display='none';
	}
}


function div_secondleedslayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_secondleedslayer').style.display='';
	}
	else
	{
		document.getElementById('div_secondleedslayer').style.display='none';
	}
}

function div_ziniflayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_ziniflayer').style.display='';
	}
	else
	{
		document.getElementById('div_ziniflayer').style.display='none';
	}
}

function div_creemarelayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_creemarelayer').style.display='';
	}
	else
	{
		document.getElementById('div_creemarelayer').style.display='none';
	}
}

function searchform_act()
{
	if (document.getElementById("layersearchform").style.display == 'none')
	{
		document.getElementById("layersearchform").style.display = '';
	}
	else
	{
		document.getElementById("layersearchform").style.display ='none';
	}
}

function div_menu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_menu').style.display='';
	}
	else
	{
		document.getElementById('div_menu').style.display='none';
	}
}

function div_allmenu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_allmenu').style.display='';
	}
	else
	{
		document.getElementById('div_allmenu').style.display='none';
	}
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

      </td>
    </tr>
  </tbody></table>
  
<%@include file="header.jsp" %>
		

    
<!-- 상품옵션 선택 테이블 시작-->
 					<form name="send_order" id="send_order" method="GET" action="${ctx}/order/regist.do">
					
					<input type="hidden" name="chkStock" value="Y">
					<input type="hidden" name="Gcnt" value="999">
					<input type="hidden" name="gBarCode" value="">
					<input type="hidden" id ="SelectColor" value="N">
					<input type="hidden" name="pName" value="${product.productName}">
                    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center" valign="top">
    
<!-- 옵션 선택 테이블 시작-->
<table width="1185" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" style="padding:20px 0px 30px 0px;">

		<span class="ht_12 ct_9 bt_40"><font color="#999999">ZINIF</font><font color="#999999">&nbsp;&gt;&nbsp; </font><a href="${ctx}/findByGroup.do?group=${product.productGroup}"><font color="#999999">${product.productGroup}</font></a> 
		<font color="#999999">&nbsp;&gt;&nbsp;</font> 
		</span><span class="ht_12 ct_9 bt_60"> 
		<a href="${ctx}/findByDetail.do?group=${product.productGroup}&detail=${product.detailGroup}"><font color="#999999">${product.detailGroup}</font></a></span><font color="#999999">
	</font></td>
  </tr>
  <tr>
    <td>
    
<table border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td width="330" align="left" valign="top" style="padding:0px 0px 0px 0px;">
	<!--
<script language=javascript>   
	document.all["tabProduct"].style.display = 'block';
</script>  
-->
<table width="240" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
		<img id="zoom_01" src="${ctx}/skin/images/${product.image}" width="240" height="330">
		<!--
		<script>
			$('#zoom_01').elevateZoom({
			zoomType: "window",
		cursor: "crosshair",
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 750
		   }); 
		</script>	
		-->
        </td>
  </tr>
</tbody></table>    
</td>
    <td width="480" align="center" valign="top">

    
    
    
 <script language="javascript"> 
<!--
var xmlHttp;
var new_product_cnt;

function createXMLHttpRequest() 
{
	if (window.ActiveXObject) 
	{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else if (window.XMLHttpRequest) 
	{
		xmlHttp = new XMLHttpRequest();    
	}
}

function selGoodColor()
{
	var Gserial=document.send_order.Gserial.value;
	var selGcolor=document.send_order.Gcolor.options[document.send_order.Gcolor.selectedIndex].value;


	createXMLHttpRequest();

	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) 
		{
			if (xmlHttp.status == 200) 
			{
				if (typeof(document.getElementById("div_Gsize")) == "object") 
				{
					document.getElementById("div_Gsize").innerHTML = xmlHttp.responseText;
				}
			}
		}	
	}

	param ="gserial="+escape(Gserial)+'&gcolor='+escape(encodeURIComponent(selGcolor))+'&t='+ fetch_unix_timestamp();
	xmlHttp.open("POST","/common/Nshopping/selectGoodColor_Ajax.asp?" + param, true);
	xmlHttp.send();

	
}

function selGoodSize()
{
	var Gserial=document.send_order.Gserial.value;
	var selGcolor=document.getElementById('Gcolor').options[document.getElementById('Gcolor').selectedIndex].value;
	var selGsize=document.getElementById('Gsize').options[document.getElementById('Gsize').selectedIndex].value;
	//var selGcolor=document.send_order.Gcolor.options[document.send_order.Gcolor.selectedIndex].value;
	//var selGsize=document.send_order.Gsize.options[document.send_order.Gsize.selectedIndex].value;

	if (selGcolor != "No" && selGsize != "No")
	{
		createXMLHttpRequest();
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) 
			{
				if (xmlHttp.status == 200) 
				{
					if ( xmlHttp.responseText=="0" ) // 에러리턴 처리
					{
						alert('색상/사이즈 선택에서 오류가 발생하였습니다.');
						document.send_order.reset();
					}
					else // 정상 Barcode 처리
					{
						document.send_order.gBarCode.value=xmlHttp.responseText;
						selBarcode();
					}
				}
			}	
		}

		param = "Gserial="+escape(Gserial)+"&Gcolor="+escape(encodeURIComponent(selGcolor))+"&Gsize="+escape(encodeURIComponent(selGsize))+'&t='+ fetch_unix_timestamp()
		xmlHttp.open("POST","/common/Nshopping/selectGoodSize_ajax.asp?" + param, false);
		xmlHttp.send();
	}
	//alert(document.send_order.gBarCode.value)
}

function selBarcode()
{
	var selBarCode=document.send_order.gBarCode.value;
	if (selBarCode != "No")
		innerJob.location = "/common/Nshopping/CheckStockGood.asp?BarCode="+selBarCode;
	
}

function getCoordiGoodOption()
{
	createXMLHttpRequest();
	MainGserial = document.add_order.MainGserial.value;
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) 
		{
			if (xmlHttp.status == 200) 
			{
				if (typeof(document.getElementById("div_coordi_option_area")) == "object") 
				{
					document.getElementById("div_coordi_option_area_onoff").style.display='';
					document.getElementById("div_coordi_option_area").innerHTML = xmlHttp.responseText;
					
				}
			}
		}	
	}

	 xmlHttp.open("POST","GetCoordiGoodOption.asp?MainGserial="+escape(MainGserial)+"&t="+ fetch_unix_timestamp(), true);
	 xmlHttp.send();
}

//-->
</script>

<table width="470" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>
    
                          <table width="470" border="0" cellspacing="0" cellpadding="0">
                              <tbody><tr>
                                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tbody><tr>
                                      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                          <tbody><tr>
                                            <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody><tr>
                                                  <td width="470" align="left" valign="top" style="padding-bottom: 8px;">
												 <span class="ht_20 ct_b bt_60">${product.productName }</span>
                                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td align="left" style="padding-bottom: 10px;">
												                                                        </td>
                                                </tr>
                                                <tr><td height="2" bgcolor="#ECECEC"></td>
                                                </tr>
                                            </tbody></table></td>
                                          </tr>
                                      </tbody></table></td>
                                    </tr>
                                </tbody></table></td>
                              </tr>
                              
                              <tr>
                                <td style="padding-top:10px;" align="left"></td>
                              </tr>
                          </tbody></table>    </td>
  </tr>
                                      <tr><td height="35" align="left">
                                      <table width="470" border="0" cellspacing="0" cellpadding="0">
                                          <tbody><tr>
                                            <td width="80" align="left"><span class="ht_12 ct_b bt_60">판매가격</span></td>
                                            <td width="20" align="left"><strong>:</strong></td>
                                            <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tbody><tr>
                                                  <td>

	<span class="ht_16 ct_b bt_60">${product.price }원</span>
			</td>
                                                </tr>
                                            </tbody></table></td>
                                            <td style="padding-left:10px;"></td>
                                          </tr>
                                      </tbody></table></td>
                                    </tr>
                                    <tr><td height="1" bgcolor="#ECECEC"></td></tr>
                                    <!-- 적립금 시작 -->
                                    <tr>
                                      <td height="35" align="left"><table width="470" border="0" cellspacing="0" cellpadding="0">
                                          <tbody><tr>
                                            <td width="80" align="left"><span class="ht_12 ct_b bt_60">적립금</span></td>
                                            <td width="20" align="left"><strong>: </strong></td>
                                            <td width="370" align="left">
												<span class="ht_12 ct_b bt_60">${product.price/100 }원</span>									</td>
                                          </tr>
                                      </tbody></table></td>
                                    </tr>
                                    <tr><td height="1" bgcolor="#ECECEC"></td></tr>
									<!-- 적립금 종료 -->

  <tr>
                                      <td height="35" align="left">
                                      <table width="470" border="0" cellspacing="0" cellpadding="0">
                                          <tbody><tr>
                                            <td width="80" align="left"><span class="ht_12 ct_b bt_60">색상 
                                             </span></td>

                                            <td width="20" align="left"><strong>:</strong></td>
                                            <td width="370" align="left"><font color="#666666">
                                            
											<select name="Gcolor" id="Gcolor" style="HEIGHT: 25px; WIDTH: 210px; FONT-SIZE: 12px;" onchange="selGoodColor()">
											  <option value="No" selected="">- 옵션을 선택해 주세요 -</option>
											  <c:forEach items="${products}" var="product">
											
											<option value="${product.color}"> ${product.color}  </option>

											
											</c:forEach>
                                            </select>
                                            </font></td>
                                          </tr>
                                      </tbody></table>                                      </td>
                                    </tr>
                                    <tr><td height="1" bgcolor="#ECECEC"></td></tr><tr>
                                     <td height="35" align="left">
                                      <table width="470" border="0" cellspacing="0" cellpadding="0">
                                          <tbody><tr>
                                            <td width="80" align="left"><span class="ht_12 ct_b bt_60">사이즈 
                                             </span></td>
                                            <td width="20" align="left"><strong>: </strong></td>
                                            <td width="370" align="left" id="div_Gsize">
                                              
                                            <select name="Gsize" id="Gsize" style="HEIGHT: 25px; WIDTH: 210px; FONT-SIZE: 12px;" onchange="selGoodSize()">
											  <option value="No" selected="">- 색상을 선택해 주세요 -</option>
											  <option value="No">------------</option>
											  
											  <c:forEach items="${products}" var="product">
											 <option value="${product.productSize }">${product.productSize}</option>
						
											  </c:forEach>
                                            </select>                                            </td>
                                          </tr>
                                     </tbody></table></td>
                                    </tr>
                                    <tr><td height="1" bgcolor="#ECECEC"></td></tr><tr>
                                      <td height="35" align="left">
                                      <table width="470" border="0" cellspacing="0" cellpadding="0">
                                          <tbody><tr>
                                            <td width="80" align="left"><span class="ht_12 ct_b bt_60">수량 </span></td>
                                            <td width="20" align="left"><strong>: </strong></td>
                                            <td width="370" align="left">
                                            
       <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td><font color="#666666"><input type="text" name="Gqty" id="Gqty" style="margin:0px 0px; padding:0px 10px; border:solid 1px #CCCCCC; width:30px; height:24px; line-height:24px; text-align:left;" value="1"></font></td>
            <td width="15"></td>

            <td><img src="${ctx}/skin/images/minus.jpg" style="cursor:hand" onclick="chg_cart_ea('dn');"></td>
            <td width="10"></td>
            <td><img src="${ctx}/skin/images/plus.jpg" style="cursor:hand" onclick="chg_cart_ea('up');"></td>
          </tr>
        </tbody></table>                                            </td>
                                          </tr>
                                      </tbody></table>                                      </td>
                                    </tr>
                                    <tr><td height="1" bgcolor="#ECECEC"></td></tr>
						<!-- 상품 배송가능 상태 시작 -->
                        <tr>
                          <td height="30">
                          
      <table width="470" border="0" cellspacing="0" cellpadding="2">
		  <tbody><tr>
			<td valign="top"><span class="m_title10"><div id="div-choose"></div></span></td>
		  </tr>
	  </tbody></table>                          </td>
                        </tr>
						<!-- 상품 배송가능 상태 끝 -->
  
  <tr id="div_coordi_option_area_onoff" style="display:none">
    <td align="center" id="div_coordi_option_area"> </td>
  </tr>
  <tr id="div_onecart_button" style="display:">
    <td>
    
    
                          <table width="470" border="0" cellpadding="0" cellspacing="0">
                              <tbody><tr>
                                <td align="left"><img src="${ctx}/skin/images/goodview_bu_01.jpg" onclick="javascript:DirectOrderOk()" style="cursor:pointer;cursor:hand;" name="Image1" border="0" id="Image1"></td>
                                <td align="center"><img src="${ctx}/skin/images/goodview_bu_02.jpg" onclick="javascript:CartOk('${product.productName}')" style="cursor:pointer;cursor:hand;" name="Image2" border="0" id="Image2"></a></td>
                              </tr>
                          </tbody></table>     </td>
  </tr>
  <tr id="div_setcart_button" style="display:none">
    <td align="center">
                                                
                          <table width="470" border="0" cellpadding="0" cellspacing="0">
                              <tbody><tr><td align="left"><a href="">
                              <img src="${ctx}/skin/images/goodview_bu_01.jpg" onclick="javascript:SetDirectOrderOk()" style="cursor:pointer;cursor:hand;" name="Image1" border="0" id="Image1"></a></td>
                               
                                <td align="center"><a href=">
                                <img src="${ctx}/skin/images/goodview_bu_02.jpg" onclick="javascript:SetCartOk()" style="cursor:pointer;cursor:hand;" name="Image2" border="0" id="Image2"></a></td>
                               
                                <td align="right"><a href="">
                                <img src="${ctx}/skin/images/goodview_bu_03.jpg" onclick="javascript:SetAddWish()" style="cursor:pointer;cursor:hand;" name="Image3" border="0" id="Image3"></a></td>
                              </tr>
                          </tbody></table>     </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</tbody></table><a name="CartArea">

    </a></td>
    </tr>
</tbody></table>   
 </td>
  </tr>
</tbody></table>
<!-- 옵션 선택 테이블 종료-->
    </td>
  </tr>
  <tr>
    <td height="50" align="center" valign="top">&nbsp;</td>
  </tr>
</tbody></table>
                   
<!-- 상품옵션 선택 테이블 종료-->
<!-- 상품옵션 선택 테이블 종료-->




  <!-- 정보보기 바-->

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="right">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center" style="padding:30px 0px 0px 0px;">

   </td>
  </tr>
</tbody></table>

    
    </td>
  </tr>
<script type="text/JavaScript">
<!--
function displayGoodInfo(arg)
{
	if(arg=="A")
	{

		document.all['good_info_s0'].style.display = ""; 	
		document.all['good_info_s1'].style.display = ""; 	
		document.all['good_info_s2'].style.display = ""; 	
		document.all['good_info_s3'].style.display = ""; 
		document.all['good_info_s4'].style.display = "";	
		document.all['infobtn_0'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn01_up.jpg" 
		document.all['infobtn_1'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn02.jpg" 
		document.all['infobtn_2'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn03.jpg" 
		document.all['infobtn_3'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn04.jpg" 
		document.all['infobtn_4'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn05.jpg" 
	}
	else if(arg=="1")
	{
		document.all['good_info_s0'].style.display = "none"; 	
		document.all['good_info_s1'].style.display = ""; 	
		document.all['good_info_s2'].style.display = "none"; 	
		document.all['good_info_s3'].style.display = "none"; 
		document.all['good_info_s4'].style.display = "none";	
		document.all['infobtn_0'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn01.jpg" 
		document.all['infobtn_1'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn02_up.jpg" 
		document.all['infobtn_2'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn03.jpg" 
		document.all['infobtn_3'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn04.jpg" 
		document.all['infobtn_4'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn05.jpg"  
	}
	else if(arg=="2")
	{
		document.all['good_info_s0'].style.display = "none"; 	
		document.all['good_info_s1'].style.display = "none"; 	
		document.all['good_info_s2'].style.display = ""; 	
		document.all['good_info_s3'].style.display = "none"; 
		document.all['good_info_s4'].style.display = "none";	
		document.all['infobtn_0'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn01.jpg" 
		document.all['infobtn_1'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn02.jpg" 
		document.all['infobtn_2'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn03_up.jpg" 
		document.all['infobtn_3'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn04.jpg" 
		document.all['infobtn_4'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn05.jpg" 
	}
	else if(arg=="3")
	{
		document.all['good_info_s0'].style.display = "none"; 	
		document.all['good_info_s1'].style.display = "none"; 	
		document.all['good_info_s2'].style.display = "none"; 	
		document.all['good_info_s3'].style.display = ""; 
		document.all['good_info_s4'].style.display = "none";	
		document.all['infobtn_0'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn01.jpg" 
		document.all['infobtn_1'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn02.jpg" 
		document.all['infobtn_2'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn03.jpg" 
		document.all['infobtn_3'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn04_up.jpg" 
		document.all['infobtn_4'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn05.jpg" 
	}
		else if(arg=="4")
	{
		document.all['good_info_s0'].style.display = "none"; 	
		document.all['good_info_s1'].style.display = "none"; 	
		document.all['good_info_s2'].style.display = "none"; 	
		document.all['good_info_s3'].style.display = "none"; 
		document.all['good_info_s4'].style.display = "";	
		document.all['infobtn_0'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn01.jpg" 
		document.all['infobtn_1'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn02.jpg" 
		document.all['infobtn_2'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn03.jpg" 
		document.all['infobtn_3'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn04.jpg" 
		document.all['infobtn_4'].src = "http://cdn.dscount.com/images_2016/shopping/goodview_btn05_up.jpg" 
	}
}
-->
</script>
</tbody></table>


<!-- 정보보기 바--> 





<div id="good_info_s0" style="display:">
 <!-- 상품설명-->
 
 <table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="70">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">
    
				      <table width="800" border="0" cellspacing="0" cellpadding="0">
                      <tbody><tr><td align="center"><img src="${ctx}/skin/images/t_title.gif" width="556" height="57"></td>
                      </tr><tr><td height="50"></td>
                      </tr>
                      <tr>
                        <td align="center" class="good-content"><p><br><br>
<strong><span style="font-size:14px">${product.productName}</span></strong></p><br>
<br>
<p><br><br>
&nbsp;</p><br>
<br>
<p><strong><span style="font-size:14px"># MD COMMENT</span></strong></p><br>

<strong> <span style="fone-size:20PX">${product.detail}</span></strong>
<p><br>
&nbsp;</p><br>
<br>
<p>&nbsp;</p><br>
<br><br>* 컬러가 있는 옷은 단독세탁하세요.<br><br>* 장식이 있거나 실루엣이 잡힌 옷은 드라이크리닝 하시면 예쁘게 오래 입으실 수 있습니다.</td>
                      </tr>
                    </tbody></table>
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</tbody></table>


  <!-- 상품설명-->
 
 

 
 



      <!----- 2014 가을신상

      2014 가을신상 끝------>

  
<!-- 특정 상품을 위한 프로그래밍 넣은 곳 -->
 
<!-- 특정 상품을 위한 프로그래밍 넣은 곳 -->
  
<!-- 2015 비치웨어 동영상 추가 이사님 지시사항-->

<!-- 2015 비치웨어 동영상 추가 이사님 지시사항-->


<!-- 상품 이미지-->
  

 
    <!-- 상품 이미지-->
 </div>
 </form>
 
 
 
  <div id="good_info_s3" style="display:">

 
 
 <!--소재정보-->
 
 <!-- 신축성 정보-->
 




 <!-- 신축성 정보-->






 
 
 
 <!-- 사이즈 세탁정보-->
 <table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding:100px 0px 25px 25px;" align="left"><span class="ht_30 ct_b bt_20">사이즈 정보</span></td>
  </tr>
  <tr><td height="14" background="${ctx }/skin/images/diagonal_line.jpg"></td>
  </tr>
  <tr><td height="40"></td>
  </tr>
</tbody></table>
<table width="1185" border="0" cellspacing="0" cellpadding="0"> 
  <tbody><tr>
    <td align="center">
    <table width="1185" border="0" cellspacing="0" cellpadding="20">
      <tbody><tr>
        <td width="300" align="center" valign="top" style="border-top:1px solid #cacaca; border-bottom:1px solid #cacaca; border-left:1px solid #cacaca;">
        
        <!-- 도식화-->
				
                    <table width="250" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td style="padding:">&nbsp;<img src="${ctx}/skin/images/170_Kr.jpg" border="0"></td>
          </tr>
        </tbody></table>
        <!-- 도식화-->        </td>
        <td width="700" align="center" style="border-top:1px solid #cacaca; border-bottom:1px solid #cacaca; border-right:1px solid #cacaca;">
        
        <!-- 사이즈표-->

						<!--
									<textarea name="sizeinfo" cols="40" rows="3">[S기준] 허리-약72cm , 힙-약88cm , 기장-약98.5cm , 허벅지통-약51cm , 밑단통-약32cm , 밑위-약23cm

[M기준] 허리-약76cm , 힙-약91cm , 기장-약100.5cm , 허벅지통-약53cm , 밑단통-약33cm , 밑위-약24cm

[L기준] 허리-약80cm , 힙-약95cm , 기장-약102.5cm , 허벅지통-약55cm , 밑단통-약34cm , 밑위-약25cm


[XL기준] 허리-약84cm , 힙-약99cm , 기장-약104.5cm , 허벅지통-약59cm , 밑단통-약35cm , 밑위-약26cm
</textarea>
									<textarea name="sizeinfo" cols="40" rows="3">S기준]허리-72,힙-88,기장-98.5,허벅지통-51,밑단통-32,밑위-23<br><br>M기준]허리-76,힙-91,기장-100.5,허벅지통-53,밑단통-33,밑위-24<br><br>L기준]허리-80,힙-95,기장-102.5,허벅지통-55,밑단통-34,밑위-25<br><br><br>XL기준]허리-84,힙-99,기장-104.5,허벅지통-59,밑단통-35,밑위-26<br></textarea>
						-->

	
						  <table width="600" border="0" cellpadding="3" cellspacing="1" bgcolor="#E0E0E0">

							<tbody><tr>
							  <td width="100" align="center" valign="top" bgcolor="#F2F2F2">&nbsp;</td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허리</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">힙</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">기장</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허벅지통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑단통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑위</span></td>
										
							</tr>

								

											<tr>
											<td width="100" align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_60">S기준</span></td>

										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">72</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">88</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">98.5</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">51</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">32</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">23</span></td>
										
											</tr>
								
			    </tbody></table>
										<br>

						  <table width="600" border="0" cellpadding="3" cellspacing="1" bgcolor="#E0E0E0">

							<tbody><tr>
							  <td width="100" align="center" valign="top" bgcolor="#F2F2F2">&nbsp;</td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허리</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">힙</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">기장</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허벅지통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑단통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑위</span></td>
										
							</tr>

								

											<tr>
											<td width="100" align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_60">M기준</span></td>

										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">76</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">91</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">100.5</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">53</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">33</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">24</span></td>
										
											</tr>
								
			    </tbody></table>
										<br>

						  <table width="600" border="0" cellpadding="3" cellspacing="1" bgcolor="#E0E0E0">

							<tbody><tr>
							  <td width="100" align="center" valign="top" bgcolor="#F2F2F2">&nbsp;</td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허리</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">힙</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">기장</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허벅지통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑단통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑위</span></td>
										
							</tr>

								

											<tr>
											<td width="100" align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_60">L기준</span></td>

										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">80</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">95</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">102.5</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">55</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">34</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">25</span></td>
										
											</tr>
								
			    </tbody></table>
										<br>

						  <table width="600" border="0" cellpadding="3" cellspacing="1" bgcolor="#E0E0E0">

							<tbody><tr>
							  <td width="100" align="center" valign="top" bgcolor="#F2F2F2">&nbsp;</td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허리</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">힙</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">기장</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">허벅지통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑단통</span></td>
										
											<td align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_20">밑위</span></td>
										
							</tr>

								

											<tr>
											<td width="100" align="center" valign="top" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_60">XL기준</span></td>

										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">84</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">99</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">104.5</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">59</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">35</span></td>
										
											<td align="center" valign="top" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">26</span></td>
										
											</tr>
								
			    </tbody></table>
										<br>

 <!-- 사이즈표-->
 

 
        
<!-- 세탁정보 -> 기존 세탁 정보처럼 해당 기호가 보여지는것이 아니라 체크한 정보에 다홍 소재 정보처럼 체크 버튼이 체크되게 됩니다.

<table width="410" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
    
    

    <table width="337" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_01.gif" width="337" height="22"></td>
      </tr>
      <tr>
        <td><table width="337" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td rowspan="5"><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_02.gif" width="20" height="102"></td>
            <td>
            
            <table width="0" border="0" cellspacing="0" cellpadding="0">
              <tr>
                
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check_b.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_2.gif" width="76" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_5.gif" width="76" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_8.gif" width="93" height="22"></td>
              </tr>
            </table>
            
            </td>
          </tr>
          <tr>
            <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_09.gif" width="317" height="18"></td>
          </tr>
          <tr>
            <td>
            
            <table width="0" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_6.gif" width="76" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check_b.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_7.gif" width="76" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_1.gif" width="93" height="22"></td>
              </tr>
            </table>
            
            </td>
          </tr>
          <tr>
            <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_16.gif" width="317" height="18"></td>
          </tr>
          <tr>
            <td>
            
<table width="0" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check_b.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_3.gif" width="76" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/check_b.gif" width="24" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_4.gif" width="76" height="22"></td>
                <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_24.gif" width="117" height="22"></td>
              </tr>
            </table>
            
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="http://cdn.dscount.com/images_2012/cleaning/cleaning_23.gif" width="337" height="27"></td>
      </tr>
    </table>
    
    
    
    
    </td>
  </tr>
</table>

  세탁정보 끝-->       
  
           
 <!-- 인치정보-->  
										
					<!-- Inch-Cm 변환표 시작 -->
<table width="600" border="0" cellspacing="1" cellpadding="0" bgcolor="#E0E0E0">
  <tbody><tr>
    <td width="150" height="35" align="center" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_60">주문사이즈( inch )</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">24</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">25</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">26</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">27</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">28</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">29</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">30</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">32</span></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">34</span></td>
  </tr>
  <tr>
    <td height="35" align="center" bgcolor="#F2F2F2"><span class="ht_12 ct_b bt_60">허리둘레( cm )</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">61</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">64</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">66</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">69</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">71</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">74</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">76</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">81</span></td>
    <td align="center" bgcolor="#FFFFFF"><span class="ht_12 ct_b bt_20">86</span></td>
  </tr>
</tbody></table>
					<!-- Inch-Cm 변환표 끝 -->
					

<!-- 인치정보-->
  
   </td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>

   <!-- 착용달 정보-->

  <!--착용달 정보-->

 
  <!-- 사이즈 세탁정보-->
 
         



	<div id="good_info_s1" style="display:"> 
 <!-- 게시판-->
 
 <table width="1185" border="0" cellspacing="0" cellpadding="0">
  <!-- 상품 후기 시작 -->
  <tbody><tr>
   <td align="center" style="padding:100px 0px 0px 0px;" id="div_good_postscript">

<style type="text/css">
<!--
.style10 {
	font-family: "돋움";
	font-size: 16px;
	font-weight: bold;
	color: #000000;
}
.style2 {
	color: #990000;
	font-weight: bold;
	font-size: 13px;
}
.style3 {
	font-family: "돋움";
	font-size: 11px;
	color: #9e9e9e;
}
.style4 {color: #666666}
.style5 {
	color: #999999;
	font-weight: bold;
}
-->
</style>

<script language="javascript">   
<!--

function ActViewAfter_Form()
{		

	if(document.send_order.LoginCheck.value == "Yes") 
	{
		document.location.href='http://pay.dscount.com/my/myOrder.asp';		
	}
	else
	{
		alert("로그인후 이용하실수 있습니다.");	
	}
}

-->
</script>

<


<!-- 상품후기 종료 --></td>
  </tr>
  <!-- 상품 후기 종료 -->
  </tbody></table>
  </div>
  
 <div id="good_info_s2" style="display:">  
 <table width="1185" border="0" cellspacing="0" cellpadding="0">
  <!-- 상품 문의 시작 -->
  <tbody><tr>
   <td align="center" style="padding:100px 0px 0px 0px;" id="div_good_qna">

<style type="text/css">
<!--
.style10 {
	font-family: "돋움";
	font-size: 16px;
	font-weight: bold;
	color: #000000;
}
.style2 {
	color: #990000;
	font-weight: bold;
	font-size: 13px;
}
.style3 {
	font-family: "돋움";
	font-size: 11px;
	color: #9e9e9e;
}
.style4 {color: #666666}
.style5 {
	color: #999999;
	font-weight: bold;
}
-->
</style>

<script language="javascript">   
<!--

function ActViewQA_Form()
{		

	if(document.send_order.LoginCheck.value == "Yes") 
	{
		if (document.all["viewQA_Form"].style.display == 'none') document.all["viewQA_Form"].style.display = 'block';
		else if (document.all["viewQA_Form"].style.display == 'block') document.all["viewQA_Form"].style.display = 'none';
	}
	else
		alert("로그인후 이용하실수 있습니다.");	
}

function WriteQAOk()
{

	if(document.qa.title.value==false){alert("\n무슨글을 쓰실꺼죠.. 제목을 적어주세요!");return;}
    if(document.qa.contents.value==false){alert("\n내용을 입력해주세요...!");return;}
	
	document.qa.sending.value=eval(document.qa.sending.value)+1;	

	if (document.qa.sending.value>1)
	{
		alert("글남기기를 진행중입니다. 확인을 누르신 후 잠시만 기다려주세요.");
	}
	else
	{
	    document.qa.submit();
	}
}    

  
-->
</script>

<!-- 상품문의 시작 -->



<!-- 상품후기 종료 --></td>
  </tr>
  <!-- 상품 문의 종료 -->
</tbody></table>
<!-- 게시판-->
  </div>
    



<!-- 교환정보-->
<div id="good_info_s4" style="display:">

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding:100px 0px 25px 25px;" align="left"><span class="ht_30 ct_b bt_20">교환/환불/배송정보</span></td>
  </tr>
  <tr><td height="14" background="${ctx }/skin/images/diagonal_line.jpg"></td>
  </tr>
  <tr><td height="40"></td>
  </tr>
  <tr><td>
  <table width="1185" border="0" cellspacing="0" cellpadding="15">

  <tbody><tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border:1px solid #cacaca;  "><span class="ht_12 ct_b bt_60">교환/반품 여부</span></td>
    <td align="left" style="border-top:1px solid #cacaca;border-right:1px solid #cacaca;border-bottom:1px solid #cacaca;">
    
<span class="ht_12 ct_r2 bt_20">교환/반품 가능합니다</span><br>

    </td>
  </tr>

    <tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border-left:1px solid #cacaca;border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_60">배송정보</span></td>
    <td align="left" style="border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_20">배송기간 : 결제확인 후 3~5일 소요됩니다.(주말, 공휴일 제외, 제주/도서산간지역 1~2일 추가 소요예상)<br>
      *상품주문 시에 배송업체, 생산업체, 통관 문제 등으로 예기치 않게 품절, 지연 될 수 있습니다. </span></td>
  </tr>

    <tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border-left:1px solid #cacaca;border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_60">배송료</span></td>
    <td align="left" style="border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_20"> 50,000원 이상 구매 시 배송비(2,500원)는 무료이며, 도서지역 추가 배송비는 없습니다.</span> </td>
  </tr>

    <tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border-left:1px solid #cacaca;border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_60">택배사</span></td>
    <td align="left" style="border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_20">우체국택배(1588-1300) http://parcel.epost.go.kr</span> </td>
  </tr>

    <tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border-left:1px solid #cacaca;border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_60">교환/반품</span></td>
    <td align="left" style="border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_20">교환/반품은 상품 수령일 포함 7일 이내에 마이 페이지에서 교환/반품 신청을 접수해 주시고 교환/반품 주소로 반송 상품을 보내주세요. <br>
      재화의 내용이 표시,광고 내용과 다르거나 계약 내용이 다르게 이행된 경우 재화 수령일로부터 3개월 이내, <br>
      그 사실을 안 날 또는 알 수 있었던 날부터 30일 이내 청약철회가 가능합니다. </span></td>
  </tr>

    <tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border-left:1px solid #cacaca;border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_60">교환/반품이<br>불가능한 경우</span></td>
    <td align="left" style="border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_20"> - 고의로 상품을 훼손한 경우<br>
       - 제품에 부착된 상품 텍(TAG)이 떼어졌거나 이미 사용된 흔적의 상품<br>
       - 반송 시일을 지켜주지 못하셨을 경우
      <br>
       * 더 궁금하신 사항은 Q&amp;A 반품/교환 방법 안내 참조</span> </td>
  </tr>

    <tr>
    <td width="120" align="center" bgcolor="f8f8f8" style="border-left:1px solid #cacaca;border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;"><span class="ht_12 ct_b bt_60">COPYRIGHT</span></td>
    <td align="left" style="border-bottom:1px solid #cacaca;border-right:1px solid #cacaca;">
    <span class="ht_12 ct_b bt_20">지니프에 기재된 모든 이미지 및 데이터베이스등은 저작권법 제 4조에 의한 저작물로 그 소유권은 (주)디스카운트에 있으며, 저작권법에 규정된 목적 이외에 <br>어떤 경우도 무단으로 복사 및 도용할 수 없습니다. 만약 이를 위반하고 허락없이 무단복사 및 도용을 하는 경우<br> <u>저작권법 제 77조에 의해 복제 및 전송중단을 요구할 수 있으며, 제93조 2항에 의해 손해배상을 청구할 수 있습니다.</u></span>
     </td>
  </tr>


</tbody></table>
</td>
</tr>
</tbody></table>


  </div>
<!-- 교환정보-->
<br><br><br><br><br>

<%@include file="footer.jsp" %>


<!--0--><br><!--0.3439941--></a></body></html>