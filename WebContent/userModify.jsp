<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html >
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<!-- saved from url=(0044)https://member.dscount.com/my/ModifyForm.asp -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml="" :lang="ko"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>디스카운트 [다홍/지니프/크리마레 통합멤버쉽]</title>
<link rel="stylesheet" type="text/css" href="${ctx}/skin/css/TextForm.css">
<script src="${ctx}/skin/js/AceCounter_AW.js.다운로드"></script><script async="" src="${ctx}/skin/js/analytics.js.다운로드"></script><script src="${ctx}/skin/js/top_javascript.js.다운로드"></script><script src="${ctx}/skin/js/jquery.min.1.7.2.js.다운로드"></script><script src="${ctx}/skin/js/jquery.cookie.js.다운로드"></script><script src="${ctx}/skin/js/cookie.js.다운로드"></script>

<script src="${ctx}/skin/js/jquery.bpopup.min.js.다운로드"></script>

<script language="JavaScript">
<!--
$(document).ready(function() {
	$('#hidden_iframe').load(function() {
	  $(this)
		.contents()
		.find('#searchAddr')
		.bind('focus', function() {
			//  do stuff
			$('#zipcodeLayer').css({position:'absolute'});
			$(window).scrollTop(0);
		});
	  $(this)
		.contents()
		.find('#searchAddr')
		.bind('blur', function() {
			$('#zipcodeLayer').css({position:'fixed'});
		});
	});
});

function isValidPassword()
{

	var objForm = document.member;
	var idReg = /^[a-z]+[a-z0-9]{5,15}$/;


	if( !idReg.test(objForm.newpass1.value)) {
		document.getElementById("div-password1").innerHTML = "비밀번호는 영문자로 시작하는 6~16자 영문자 또는 숫자이어야 합니다.";
//		objForm.newpass1.value=""
		return false;
	}
	else
	{
		document.getElementById("div-password1").innerHTML = ""
		return true;
	}

}

function confirmPassword()
{
	var objForm = document.member;
	if(objForm.newpass1.value != objForm.newpass2.value) 
	{
		document.getElementById("div-password2").innerHTML = "비밀번호와 비밀번호 확인란의 값이 틀립니다. 다시 한번 입력해 주세요.";
//		objForm.member_pass.value=""
//		objForm.member_repass.value=""
//		objForm.member_pass.focus();
		return false;
	}
	else
	{
		document.getElementById("div-password2").innerHTML = ""
		return true;
	}
}

var xmlHttp;

function createXMLHttpRequest() 
{
	if (window.ActiveXObject) 
	{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else if (window.XMLHttpRequest) 
	{
		xmlHttp = new XMLHttpRequest();    
	}
}

function passwd_change_toggle()
{
	if ( document.getElementById('div_passwd_change').style.display == '' )
		document.getElementById('div_passwd_change').style.display = 'none';
	else
		document.getElementById('div_passwd_change').style.display = '';
}

function change_passwd()
{
	var password=document.getElementById('password').value;
	var pass=document.getElementById('pass').value;
	var pass2=document.getElementById('pass2').value;

	if ( password=='' )
	{
		alert("현재 비밀번호를 입력해주세요!");
		document.getElementById('password').focus();
		return;
	}
	if(password !='${user.password}'){
		alert("비밀번호가 틀립니다.");
		document.getElementById('password').focus();
		return;
	}

	if ( pass=='' )
	{
		alert("새 비밀번호를 입력해주세요!");
		document.getElementById('pass').focus();
		return;
	}

	if ( pass2=='' )
	{
		alert("새 비밀번호 확인을 입력해주세요!");
		document.getElementById('pass2').focus();
		return;
	}

	if ( pass!=pass2 )
	{
		alert("새 비밀번호의 값이 일치하지 않습니다!");
		document.getElementById('pass').focus();
		return;
	}
	
	else
	{
		document.member.action="${ctx}/modifyPw.do";
		document.member.submit();
		
	}
	
}

function fetch_unix_timestamp()
{
	return Math.floor(new Date().getTime() / 1000);
}


function trim(str) 
{	
		strNewName = str
		if(strNewName != null) {			
			for(intLoop = 0; intLoop < strNewName.length; intLoop++) {
				if(strNewName.charAt(intLoop) != " ") break				
			}
			startIndex = intLoop;
			strNewName = strNewName.substr(startIndex);
				
			for(intLoop = strNewName.length - 1; intLoop >= 0; intLoop--) {
				if(strNewName.charAt(intLoop) != " ") break							
			}
			endIndex = intLoop;
			strNewName = strNewName.substr(0, endIndex + 1);
			
			if(strNewName.length <= 0) strNewName = null
		}		
		return strNewName
}
	
function isNumeric(str) 
{	
		strNewName = str
		
		if(strNewName != null) {			
			for(intLoop = 0; intLoop < strNewName.length; intLoop++) {
				if(strNewName.charAt(intLoop) < "0" || strNewName.charAt(intLoop) > "9") {						
					return false;
				}				
			}			
			return true;
		}
		else
			return false;
}	
		
function FormOK() 
{		
	   var objForm = document.member;
       var mail =  objForm.member_email.value;				
		
		if (trim(objForm.member_zipcode.value) == null) {
			alert(" 우편번호를 검색하여 해당주소를 선택해 주십시요.");
			objForm.member_zipcode.focus();
			return;
		}				
		
	
		if (mail.search(/(\S+)@(\S+)\.(\S+)/) == -1) {
			alert("전자우편주소의 형식이 올바르지 않습니다."); 
			objForm.member_email.focus();
			return;
		}				
		
		
		if (objForm.member_cphone.length < 10) {
			alert(" 이동전화를  정확히 입력해주세요.");
			objForm.home_phone2.focus();
			return;
		}							
	
		objForm.action="${ctx}/userModify.do";
		objForm.submit();
}
function ConfirmDelete()
{
	if(confirm("정말 삭제하시겠습니까?") == true){
		location.href="${ctx}/userDelete.do";
	}else{
		return;
	}
		
}

function FormCancel() 
{
	document.member.reset();
	alert('회원정보 수정이 취소되었습니다.');
	location.href="${ctx}"
}

//-->
</script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script>

    function sample6_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('member_zipcode').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('member_addr1').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('member_addr2').focus();
            }
        }).open();
    }
</script>

<script src="${ctx}/skin/js/f.txt"></script><script src="${ctx}/skin/js/f(1).txt"></script><script src="${ctx}/skin/js/f(2).txt"></script></head><body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><center>

  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td align="center"><script>
//	openWin = window.open('../notice.htm','gongzi',"scrollbars,resizable,width=540,height=500,scrollbars=yes, resizable=no");			
</script>
<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
</script>


<script language="javascript">
function hide_topbanner(target)
{
	if( $.cookie("topbannershow")!=target)
	{
		$.cookie("topbannershow", target);
		$("#topbanner").hide();
	}

}

function open_ftc_info()
{
	var url = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1278649753";
	window.open(url, "communicationViewPopup", "width=750, height=700;");
}


function Right(e) {

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("오른쪽 마우스는 사용하실수 없습니다.");
        return false;
    }
    return true;
}

document.onmousedown=Right;

if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown=Right;
}

</script>

<script language="javascript">

function preMember()
{
		alert("디스카운트. \n\r 회원가입이나 로그인해주시기바랍니다!");
}

function loginForm()
{	
		if(confirm("디스카운트. \n\r로그인/회원가입 화면으로 이동 하시겠습니까?"))						
		location = "../Member/LoginForm.asp";
}
</script>

<script language="JavaScript">
/* if(parent.frames.length <= 0) { top.location.href="/"; }*/
// 쿠키값 가져오기
function getCookie(key)
{
  var cook = document.cookie + ";";
  var idx =  cook.indexOf(key, 0);
  var val = "";
 
  if(idx != -1)
  {
    cook = cook.substring(idx, cook.length);
    begin = cook.indexOf("=", 0) + 1;
    end = cook.indexOf(";", begin);
    val = unescape( cook.substring(begin, end) );
  }
 
  return val;
}
 
// 쿠키값 설정
function setCookie(name, value, expiredays)
{
  var today = new Date();
  today.setDate( today.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";"
} 

</script>

<script language="JavaScript">
<!--
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>


<script language="javascript">
	function openwindow(name, url, width, height, scrollbar) {
		scrollbar_str = scrollbar ? 'yes' : 'no';
		window.open(url, name, 'width='+width+',height='+height+',scrollbars='+scrollbar_str);
	}
</script>

<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function newsread(no)
{
	var url;
	url ="../my/NewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread2(no)
{
	var url;
	url ="../my/NipponNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function newsread3(no)
{
	var url;
	url ="../my/MDNewsRead.asp?no="+no;
	window.open(url,'mypop','width=620,height=500,scrollbars=yes');
	return;
}

function gotoMydahong()
{
	location = "../My/MyDahong.asp";	
}

function gotoMemo()
{
	location = "../My/MyMemo.asp";	
}

function GiftGoodView()
{
	var url;
	url ="../my/GiftGoodView.asp";
	window.open(url,'GiftGood','width=620,height=500,scrollbars=yes');
	return;
}

function gotoPurchase()
{
	var url;
	url ="../GiftTicket/GiftTicketInfo.html";
	window.open(url,'GiftTicket','width=700,height=600,scrollbars=no');
	return;
}
 -->




</script>



<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function size()
{
	var url;
	url ="/shopping/images6/main/size.asp"
	window.open(url,'mypop','width=720,height=500,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function model()
{
	var url;
	url ="http://model.dahong.co.kr/Model_Register_Form_new.asp"
	window.open(url,'mypop','width=738,height=600,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function photo_re()
{
	var url;
	url ="/photojenic/10th/10photot_list.htm"
	window.open(url,'mypop','width=670,height=700,scrollbars=yes');
	return;
}
 -->
</script>


<script language="JavaScript" type="TEXT/JAVASCRIPT">
<!--
function event0108()
{
	var url;
	url ="/event/event_20070108.htm"
	window.open(url,'mypop','width=617,height=700,scrollbars=yes');
	return;
}
 -->
</script>

<script language="javascript">
function CyworldConnect(gserial)
{
	window.open('http://api.cyworld.com/openscrap/post/v1/?xu=http://dahong.dscount.com/XML/CyworldConnect.asp?gserial='+gserial+'&sid=ksBrwWMJBeUecZF3gfMjvBNotcUtZCnN', 'cyopenscrap', 'width=450,height=410');
}

function GetGoodUrl(bcomcat_param, gserial_param)
{
	var strHost;
	var strUrl;
	var strUri;

	strUrl = "/NShopping/GoodView_Item.asp?Gserial=" + gserial_param;

	switch(bcomcat_param) {
		case '2':
			strHost = "dahong.dscount.com";
			break;
		case '1':
			strHost = "zinif.dscount.com";
			break;
		case '232':
			strHost = "secondleeds.dscount.com";
			break;
		case '323':
			strHost = "creemare.dscount.com";
			break;
		case '330':
			strHost = "milcott.dscount.com";
			break;
		case '362':
			strHost = "monomori.dscount.com";
			break;
		case '476':
			strHost = "dahong.dscount.com";
			break;
	}

	strUri = "http://" + strHost + strUrl
	document.location.href=strUri;
}

</script>

  

<script src="${ctx}/skin/js/banners.js.다운로드"></script>
<script src="${ctx}/skin/js/lnb.js.다운로드"></script>




<style type="text/css">

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>


<!-- 최상단 배너 시작 -->

<!-- 최상단 배너 종료 -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="center">
<!-- 공통상단 시작 -->


<script language="javascript">
function div_promotion_onoff(state)
	{
		if (state==1)
		{
			document.getElementById('div_promotion').style.display='';
		}
		else
		{
			document.getElementById('div_promotion').style.display='none';
		}
	}

	function goto_promotion()
	{
		document.location.href='http://dahong.dscount.com/Nshopping/promotion_list.asp';
	}

</script>


    <script type="text/JavaScript">
<!--
function div_my_onoff(state)
{
	if (state==1)
	{
		$("#div_my").css('z-index','3000');
		document.getElementById('div_my').style.display='';
	}
	else
	{
		document.getElementById('div_my').style.display='none';
		$("#div_my").css('z-index','1000');
	}
}

function div_cs_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_cs').style.display='';
	}
	else
	{
		document.getElementById('div_cs').style.display='none';
	}
}

function div_dscountlayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dscountlayer').style.display='';
	}
	else
	{
		document.getElementById('div_dscountlayer').style.display='none';
	}
}


function div_dahonglayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_dahonglayer').style.display='';
	}
	else
	{
		document.getElementById('div_dahonglayer').style.display='none';
	}
}

function div_monomorilayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_monomorilayer').style.display='';
	}
	else
	{
		document.getElementById('div_monomorilayer').style.display='none';
	}
}


function div_secondleedslayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_secondleedslayer').style.display='';
	}
	else
	{
		document.getElementById('div_secondleedslayer').style.display='none';
	}
}

function div_ziniflayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_ziniflayer').style.display='';
	}
	else
	{
		document.getElementById('div_ziniflayer').style.display='none';
	}
}

function div_creemarelayer_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_creemarelayer').style.display='';
	}
	else
	{
		document.getElementById('div_creemarelayer').style.display='none';
	}
}

function searchform_act()
{
	if (document.getElementById("layersearchform").style.display == 'none')
	{
		document.getElementById("layersearchform").style.display = '';
	}
	else
	{
		document.getElementById("layersearchform").style.display ='none';
	}
}

function div_menu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_menu').style.display='';
	}
	else
	{
		document.getElementById('div_menu').style.display='none';
	}
}

function div_allmenu_onoff(state)
{
	if (state==1)
	{
		document.getElementById('div_allmenu').style.display='';
	}
	else
	{
		document.getElementById('div_allmenu').style.display='none';
	}
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>




 

<!-- 내용 테이블 시작-->
<%@include file="header.jsp" %>
<!-- 타이틀-->

<c:if test="${user eq null }">
<script>
alert("회원전용 페이지입니다 로그인 하십시오");
location.replace("${ctx}/login.jsp")

</script>]

</c:if>
<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-top:70PX; padding-bottom:50PX; padding-left:0px; " align="left">
    <span class="et_30 ct_b bt_50">MY PAGE</span>
   </td>
  </tr>
  <!--
  <tr><td height="2" background="//cdn.dscount.com/images_2016/top/diagonal_line.jpg"></td>
  </tr>-->
</tbody></table>
<!-- 타이틀 -->

<table width="1185" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td width="165" align="left" valign="top"><table width="165" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
  <tbody><tr>
    <td style="padding:25px 0px 25px 0px;" align="center" valign="top">
    
<table width="120" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:0px 0px 10px 5px; border-bottom:dotted 1px #cccccc">
<!-- 주문관리-->    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:0px 0px 20px 0px;">
<span class="ht_14 ct_b bt_60">주문관련</span>
    </td>
  </tr>
  <tr>
    <td align="left" style="padding-bottom:15px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;https://pay.dscount.com/my/MyOrder.asp&#39;"><span class="ht_11 ct_3 bt_60">·&nbsp;주문/배송조회</span></td>
  </tr>
  <tr>
    <td align="left" style="padding-bottom:15px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;https://pay.dscount.com/my/myOrder_cancel.asp&#39;"><span class="ht_11 ct_3 bt_60">·&nbsp;취소/교환/반품</span></td>
  </tr>
  <tr>
    <td align="left" style="padding-bottom:15px;cursor:pointer;cursor:hand;" onclick="location.href=&#39;http://pay.dscount.com/my/myIndivisualPayment.asp&#39;"><span class="ht_11 ct_3 bt_60">·&nbsp;개인결제내역</span></td>
  </tr>
</tbody></table>
<!-- 주문관리-->
    
    </td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 0px 10px 5px; border-bottom:dotted 1px #cccccc;">

<!-- 통장관리-->

<!-- 통장관리-->
    </td>
  </tr>
  
  <tr>
    <td align="left" style="padding:0px 0px 10px 5px;">
<!-- 개인정보-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left" style="padding:20px 0px 20px 0px;">
<span class="ht_14 ct_b bt_60">개인정보</span>
    </td>
  </tr>
  <tr>
    <td align="left" style="cursor:pointer;cursor:hand;" onclick="location.href=&#39;https://member.dscount.com/my/ModifyForm.asp&#39;"><span class="ht_11 ct_3 bt_60">·&nbsp;개인정보수정</span></td>
  </tr>
</tbody></table>
<!-- 개인정보-->
    </td>
  </tr>
</tbody></table>

    
    
    </td>
  </tr>
  <tr>
    <td height="50"></td>
  </tr>
</tbody></table>
</td>
    <td width="1020" align="right" valign="top">
    
<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td>



<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->

function mypage01_act()
{
	if (document.getElementById("mypage01_layer").style.display == 'none')
	{
		document.getElementById("mypage01_layer").style.display = '';
	}
	else
	{
		document.getElementById("mypage01_layer").style.display ='none';
	}
}


</script>

<center>





<!-- 나의상세내역-->
  

<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="30" align="left" valign="top"><span class="ht_16 bt_60 ct_0">${user.name }님의 정보</span></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#CCCCCC"></td>
  </tr>
  <tr>
    <td height="170"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
     
        <td width="240" align="center" style="border-right:1px dotted #cccccc;">
          <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td height="40" align="center" valign="top"><span class="ht_16 bt_60 ct_3">적립금</span></td>
          </tr>
          <tr>
            <td height="40" align="center" onclick="location.href=&#39;http://www.dscount.com/my/myEmoney.asp&#39;" style="cursor:pointer; cursor:hand;"><span class="ht_28 bt_60 ct_b">${user.mileage}원</span></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"></td>
          </tr>
        </tbody></table>
        </td>
        

      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#CCCCCC"></td>
  </tr>
  <tr>
    <td height="50"></td>
  </tr>
</tbody></table>



 

<!--나의상세내역 끝-->
</center>          
                  


</td>
  </tr>
</tbody></table>    
<!-- 내용-->
<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="20"></td>
  </tr>
  <tr>
    <td align="left"><span class="ht_20 bt_60">개인정보수정</span></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>


<!-- 페이지 이동테이블 배송주소록 불필요로 인한 히든처리 
 <tr>
  <td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" height="30" align="center" bgcolor="#585b61" ><span class="m_title12">개인정보수정</span></td>
    <td width="50%" align="center"    onClick="document.location.href='/my/DeliveryAddress_List.asp'" ><span class="ht_11 bt_60 ct_3">배송 주소록</span></td>
  </tr>
</table>
</td>
</tr>
 <tr>
 <td height="1" bgcolor="#585b61"></td>
 </tr>
  <tr>
  <td height="25"></td>
  </tr>
 </table>
  </td>
  </tr>
 -->
  <tr>
    <td>
                     
 <form name="member" method="post">   
<input type="hidden" id ="id" name="id" value="${user.userId }">
<input type="hidden" id ="modi" name ="modi" value="Y">;    
<div id="zipcodeLayer" style="width:455px; height:500px; background:#fff; z-index:9999; display:none; border:7px solid #ededed;"></div><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #E2E2E2">
  <tbody><tr>
    <td width="25%" height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3 bt_60 ct_3">아이디</span></td>
    <td width="75%" align="left"><span class="ht_11 bt_60 ct_3">${user.userId }</span></td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
    <td width="25%" height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">이름</span></td>
    <td width="75%" align="left"><span class="ht_11 bt_60 ct_3">${user.name }</span></td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
       <td colspan="2">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="40" width="25%" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">비밀번호</span></td>
    <td width="75%" align="left">
    <table width="110" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td height="29" align="center" bgcolor="#f7f7f7" style="border:1px solid #CCCCCC;cursor:pointer;cursor:hand;" onclick="javascript:passwd_change_toggle()"><span class="ht_11 bt_60 ct_3 bt_60">비밀번호 수정</span></td>
      </tr>
    </tbody></table>    </td>
  </tr>
  <tr id="div_passwd_change" style="display: none;">
    <td align="right" style="padding:10px">&nbsp;</td>
    <td style="padding:15px 0px 15px 0px;">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f7f7f7">
      <tbody><tr><td colspan="3" height="10"></td></tr>
      <tr>
        <td width="20%" align="left" style="padding-left:15px;"><span class="ht_11 bt_60 ct_3">현재 비밀번호</span></td>
        <td width="30%" align="left" style="padding-bottom:5px; padding-top:5px;"><input name="password" type="password" id="password" size="8" maxlength="16" style=" width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;" autocomplete="off"></td>
        <td width="50%" align="left"><span class="ht_11 bt_20 ct_6">현재 비밀번호를 입력해주세요.</span></td>
      </tr>
      <tr>
        <td align="left" style="padding-left:15px;"><span class="ht_11 bt_60 ct_3">새 비밀번호</span></td>
        <td align="left" style="padding-bottom:5px; padding-top:5px;"><input name="pass" type="password" id="pass" size="8" maxlength="16" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;" autocomplete="off" onblur="isValidPassword();"></td>
        <td align="left"><span class="ht_11 bt_20 ct_6">영문대/소문자+숫자조합 6~16자이내</span></td>
      </tr>
      <tr>
        <td style="padding-left:8px;"></td>
        <td colspan="2" align="left">
        <span class="ht_11 bt_60 ct_3">
          <div id="div-password1" class="alertmessage"></div>
        </span>
        </td>
      </tr>
      <tr>
        <td align="left" style="padding-left:15px;"><span class="ht_11 bt_60 ct_3">새 비밀번호 확인</span></td>
        <td align="left" style="padding-bottom:5px; padding-top:5px;"><input name="pass2" type="password" id="pass2" size="8" maxlength="16" style=" width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;" autocomplete="off" onblur="isValidPassword();confirmPassword();"></td>
        <td align="left"><span class="ht_11 bt_20 ct_6">영문대/소문자+숫자조합 6~16자이내</span></td>
      </tr>
      <tr>
        <td style="padding-left:8px;"></td>
        <td colspan="2" align="left">
        <span class="ht_11 bt_60 ct_3">
          <div id="div-password2" class="alertmessage"></div>
        </span>
        </td>
      </tr>
      <tr>
        <td style="padding-left:8px;"></td>
        <td colspan="2" style="padding-bottom:15px; padding-top:5px;" align="left">
        <table width="110" height="29" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td align="center" bgcolor="333333" onclick="javascript:change_passwd()" style="cursor:pointer;cursor:hand;"><span class="ht_11 ct_w bt_60">수정하기</span></td>
            </tr>
        </tbody></table>
        </td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>
  
       </td>
  </tr>
  
  
  
  
  
  
  
  
  

  
  
  
  
  
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
    <td height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">생년월일</span></td>
    <td align="left"><span class="ht_11 bt_60 ct_3">${user.birth }</span></td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
    <td height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">성별</span></td>
    <td align="left"><span class="ht_11 bt_60 ct_3">${user.gender }자</span></td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
    <td height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">이메일</span></td>
    <td align="left"><span class="ht_11 bt_60 ct_3">
    <input type="text" name="member_email" value="${user.email}" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;">
    </span></td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <script type="" language="JavaScript">
<!--
function openZipcodelayer(mode)
{
	url = "/lib/zipcode_api.asp?mode=" + mode;
	$('#zipcodeLayer').bPopup({
		content:'iframe',
		iframeAttr:"name='hidden_iframe' id='hidden_iframe' frameborder='0' width='100%' height='100%'",
		loadUrl:url
	});
}

function closeZipcodelayer()
{
	$('#zipcodeLayer').bPopup().close();
	$('#zipcodeLayer').html('');
}

//-->	
</script> 


  <tr>
    <td height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">우편번호</span></td>
    <td align="left">
    <table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td><input type="text" name="member_zipcode" id="member_zipcode" value="${user.zipCode }" readonly="" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;"></td>
        <td width="15"></td>
        <td>
            <table width="150" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
             <td height="30" align="center" bgcolor="#f7f7f7" style="border:1px solid #CCCCCC;cursor:pointer;cursor:hand;" onclick="javascript:sample6_execDaumPostcode();"><span class="ht_11 bt_60 ct_3 ">우편번호 검색</span></td>
            </tr>
           </tbody></table>
       </td>
      </tr>

    </tbody></table>
    </td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
    <td height="80" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">주소</span></td>
    <td align="left">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td align="left"><input type="text" name="member_addr1" id="member_addr1" value="${user.address }" readonly="" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;"></td>
  </tr>
  <tr><td height="5"></td></tr>
  <tr>
    <td align="left"><input type="text" name="member_addr2"  id="member_addr2" value="" style="width:400px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;"></td>
  </tr>
</tbody></table>

    </td>
  </tr>
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  <tr>
    <td height="40" style="padding-left:30px" align="left"><span class="ht_11 bt_60 ct_3">휴대폰</span></td>
    <td align="left"><input type="text" name="member_cphone" value="${user.phone }" maxlength="16" style="width:200px; height:30px;line-height:30px; border:1px solid #CCCCCC; padding-left:10px;"></td>
  </tr>
  
  <tr><td colspan="2" height="1" bgcolor="#E2E2E2"></td></tr>
  
</tbody></table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td height="30"></td>
  </tr>
  <tr>
    <td align="center">
    
<table border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="padding-right:10px;">
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td class="date_bt8" onclick="javascript:FormOK()"><span class="ht_11 bt_60 ct_w">수정하기</span></td>
          </tr>
        </tbody></table>
    </td>
    <td style="padding-right:10px;">
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td class="date_bt9" onclick="javascript:FormCancel();"><span class="ht_11 ct_3 bt_60">취  소</span></td>
          </tr>
        </tbody></table>
    </td>
    <td>
        <table border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td class="date_bt9" onclick="javascipt:ConfirmDelete(); "><span class="ht_11 ct_3 bt_60">회원탈퇴</span></td>
          </tr>
        </tbody></table>
    </td>
  </tr>
</tbody></table>

    
    
    </td>
  </tr>
</tbody></table>



    </form>
    
    </td>
  </tr>

  <tr>
    <td height="250"></td>
  </tr>

</tbody></table>

<!-- 내용-->
   </td>
  </tr>
</tbody></table>
<%@ include file="footer.jsp" %>


</body></html>