package domain;

public class Deliver {
	
	private String deliverId;
	private String receieverName;
	private String receieverAddress;
	private String receieverPhone;
	private String deliverCompany;
	
	
	public String getDeliverId() {
		return deliverId;
	}
	public void setDeliverId(String deliverId) {
		this.deliverId = deliverId;
	}
	public String getReceieverName() {
		return receieverName;
	}
	public void setReceieverName(String receieverName) {
		this.receieverName = receieverName;
	}
	public String getReceieverAddress() {
		return receieverAddress;
	}
	public void setReceieverAddress(String receieverAddress) {
		this.receieverAddress = receieverAddress;
	}
	public String getReceieverPhone() {
		return receieverPhone;
	}
	public void setReceieverPhone(String receieverPhone) {
		this.receieverPhone = receieverPhone;
	}
	public String getDeliverCompany() {
		return deliverCompany;
	}
	public void setDeliverCompany(String deliverCompany) {
		this.deliverCompany = deliverCompany;
	}
	
	

}
