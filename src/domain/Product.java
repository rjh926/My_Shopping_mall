package domain;

public class Product {
	
	private String productId;
	private String productName;
	private String productGroup;
	private String detailGroup;
	private String color;
	private String productSize;
	private int price;
	private String detail; // �긽�뭹 �긽�꽭�젙蹂�
	private int inventory; // �옱怨�
	private String image;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductGroup() {
		return productGroup;
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public String getDetailGroup() {
		return detailGroup;
	}
	public void setDetailGroup(String detailGroup) {
		this.detailGroup = detailGroup;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getProductSize() {
		return productSize;
	}
	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getInventory() {
		return inventory;
	}
	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productGroup=" + productGroup
				+ ", detailGroup=" + detailGroup + ", color=" + color + ", productSize=" + productSize + ", price="
				+ price + ", detail=" + detail + ", inventory=" + inventory + ", image=" + image + "]";
	}

}
