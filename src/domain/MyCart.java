package domain;

public class MyCart {
	
	private String userId;
	private int ProductId;
	private Product product;
	private int amount;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getProductId() {
		return ProductId;
	}
	public void setProductId(int productId) {
		ProductId = productId;
	}
	public int getAmmount() {
		return amount;
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public void setAmmount(int ammount) {
		this.amount = ammount;
	}
	
	@Override
	public String toString() {
		return "MyCart [userId=" + userId + ", ProductId=" + ProductId + ", product=" + product + ", amount=" + amount
				+ "]";
	}

}
