package domain;

import java.sql.Date;
import java.util.List;

/**
 * @author RyouJH
 *
 */
public class Order {	
	private String orderId;
	private List<MyCart> products ;
	private String buyerName;
	private int invoiceNum;
	private String payment;
	private Date orderDate;
	private String isPaid;
	private String address;
	private String phone;
	private User isUser;
	
	
	public User getIsUser() {
		return isUser;
	}
	public void setIsUser(User isUser) {
		this.isUser = isUser;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public List<MyCart> getProducts() {
		return products;
	}
	public void setProducts(List<MyCart> products) {
		this.products = products;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public int getInvoiceNum() {
		return invoiceNum;
	}
	public void setInvoiceNum(int invoiceNum) {
		this.invoiceNum = invoiceNum;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(String isPaid) {
		this.isPaid = isPaid;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		String str =  "Order [orderId=" + orderId + ", products=" + products + ", buyerName=" + buyerName + ", invoiceNum="
				+ invoiceNum + ", payment=" + payment + ", orderDate=" + orderDate + ", isPaid=" + isPaid + ", address="
				+ address + ", isUser="+ isUser +", phone=" + phone + "]" +"Products = "+this.products.size();
		
		return str;
	}
	
	
	
	
	
	
	
}
