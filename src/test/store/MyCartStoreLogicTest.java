package test.store;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import domain.MyCart;
import store.MyCartStore;
import store.logic.MyCartStoreLogic;

public class MyCartStoreLogicTest {
	
	MyCartStore store;
	@Before
	public void init() {
		store = new MyCartStoreLogic();
		
	}

	@Test
	public void testCreate() {
		MyCart cart = new MyCart();
		cart.setUserId("rjh926");
		cart.setProductId(12);
		cart.setAmmount(4);
		
		store.create(cart);
	}

	@Test
	public void testRead() {

		assertEquals(2,store.read("rjh926").size());
	}

	@Test
	public void testUpdate() {
		MyCart cart = new MyCart();
		cart.setUserId("rjh926");
		cart.setProductId(1234);
		cart.setAmmount(10);
		store.update(cart);
		
	}

	@Test
	public void testDelete() {
		MyCart cart = new MyCart();
		cart.setUserId("rjh926");
		cart.setProductId(12);
		
		store.delete(cart);
	}

	@Test
	public void testDeleteAll() {
		store.deleteAll("rjh926");
		
		assertEquals(0,store.read("rjh926").size());
	}

}
