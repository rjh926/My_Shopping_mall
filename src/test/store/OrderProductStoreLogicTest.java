package test.store;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import domain.Order;
import domain.OrderList;
import store.OrderProductStore;

public class OrderProductStoreLogicTest {

OrderProductStore store;
	
	@Before
	public void init() {
		store = new store.logic.OrderProductStoreLogic();
		
		for(int i= 1;i<5;i++) {
			OrderList list= new OrderList();
			list.setOrderId(6);
			list.setProductId(i+30);
			list.setAmount(i+20);
			store.create(list);
		}
	}

	@Test
	public void testCreate() {
		OrderList list = new OrderList();
		list.setOrderId(5);
		list.setProductId(35);
		list.setAmount(25);
		
		store.create(list);	
		
		
		
		}

	@Test
	public void testDelete() {
		OrderList list = new OrderList();
		list.setOrderId(6);
		list.setProductId(32);
		
		store.delete(list);
		
		Order order = new Order();
		order.setOrderId("6");
		
		
		assertEquals(3, store.selectByOrder(order).size());
	}

	@Test
	public void testSelectByOrder() 
	{
		OrderList list = new OrderList();
		list.setOrderId(6);
		
		Order order = new Order();
		order.setOrderId("6");

		assertEquals(4, store.selectByOrder(order).size());
	}
	@After
	public void finish() {
		OrderList list1 = new OrderList();
		list1.setOrderId(5);
		list1.setProductId(35);
		store.delete(list1);
		
		for(int i= 1;i<5;i++) {
			OrderList list= new OrderList();
			list.setOrderId(6);
			list.setProductId(i+30);
			list.setAmount(i+20);
			store.delete(list);
		}
	}

}
