package test.store;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.List;
import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import domain.Order;
import domain.User;
import store.OrderStore;
import store.logic.OrderStoreLogic;

public class OrderStoreLogicTest {

	OrderStore store;
	
	@Before
	public void init() {
		store = new OrderStoreLogic();
		
//		for(int i=0;i<5;i++) {
//			Order order = new Order();
//			java.util.Date uDate = new java.util.Date();
//			
//			order.setOrderDate(new Date(uDate.getTime()));
//			
//			order.setBuyerName("Ryou "+i);
//			order.setAddress("광주 "+i);
//			order.setPhone("010 "+i);
//			order.setPayment("무통장 "+i);
//			order.setIsPaid("FALSE");
//			store.create(order);
//			
//			
//		}
	}

	@Test
	public void testCreate() {
//		Order order = new Order();
//		
//		java.util.Date uDate = new java.util.Date();
//		
//		order.setOrderDate(new Date(uDate.getTime()));
//		order.setBuyerName("Ryou");
//		order.setAddress("광주");
//		order.setPhone("010");
//		order.setPayment("무통장");
//		order.setIsPaid("FALSE");
//		store.create(order);
	}

	@Test
	public void testReadById() {
		Order order = store.readById("2");
	//	assertNull(order);
		assertEquals("서울",order.getAddress());
	}

	@Test
	public void testReadByUser() {
		User user = new User();
		user.setName("Ryou");
		List<Order> orders = store.readByUser(user);
		
		assertEquals(2,orders.size());
	}

	@Test
	public void testReadByBuyer() {
		List<Order> orders = store.readByBuyer("류재형","010-3329-8569");
		
		assertEquals(2,orders.size());	}

	@Test
	public void testReadAll() {
		List<Order> orders = store.readAll();
		assertEquals(3,orders.size());
	}
	
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}
	@Test
	public void testGetPaid() {
		
		
		store.getPaid("2");
		Order order = store.readById("2");
		assertEquals("TRUE",order.getIsPaid());
	}
	
	@After
	public void finish() {
//		for(int i=0;i<5;i++) {
//			Order order = new Order();
//			java.util.Date uDate = new java.util.Date();
//			order.setOrderDate(new Date(uDate.getTime()));
//			order.setOrderId(i+"");
//			order.setBuyerName("Ryou "+i);
//			order.setAddress("광주 "+i);
//			order.setPhone("010 "+i);
//			order.setPayment("무통장 "+i);
//			
//			
//			store.delete(order.getOrderId());
//		}
	
	}
	

}
