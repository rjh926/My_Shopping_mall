package controller.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

@WebServlet("/findByName.do")
public class FindByNameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String productName = request.getParameter("productName");
		
		ProductService productService = new ProductServiceLogic();

		List<Product> products = productService.findByName(productName);

		request.setAttribute("products", products);

		request.getRequestDispatcher("list4.jsp").forward(request, response);

	}

}
