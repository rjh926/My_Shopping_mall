package controller.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

/**
 * Servlet implementation class ProductDetailServlet
 */
@WebServlet("/product/detail.do")
public class ProductDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProductService service = new ProductServiceLogic();
		Product product = service.findById(request.getParameter("id"));
		List<Product> products = service.findByName(product.getProductName());
		List<String> colors = new ArrayList<>();
		List<String> sizes = new ArrayList<>();
		
		if(products != null) {
			for(Product p : products) {
			colors.add(p.getColor());
			sizes.add(p.getProductSize());
			}
		}
		
		request.setAttribute("colors",colors);
		request.setAttribute("sizes", sizes);		
		request.setAttribute("product",product);
		request.setAttribute("products",products);
		
		request.getRequestDispatcher("/detail.jsp").forward(request, response);
	}

}
