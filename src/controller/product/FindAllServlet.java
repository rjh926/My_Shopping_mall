package controller.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

/**
 * Servlet implementation class FindAllServlet
 */
@WebServlet("/findAll.do")
public class FindAllServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProductService service = new ProductServiceLogic();	
		List<Product> products = service.findAll();
		
		request.setAttribute("products",products);
		request.setAttribute("productsGroup","ALL");
		request.setAttribute("total",products.size());
		
		request.getRequestDispatcher("/list.jsp").forward(request, response);
	}

}
