package controller.product;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

@WebServlet("/product/upload.do")
public class ProductCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		String uploadPath = request.getRealPath("/skin/images");
		
		ProductService service = new ProductServiceLogic();
		int maxSize = 1024 * 1024 * 10;
		String name = "";
		String subject = "";
		
		String fileName1  = "";
		String originName = "";
		long fileSize = 0;
		String fileType ="";
		
		MultipartRequest multi = null;
		PrintWriter out = response.getWriter();
		Product product = null;
		
		try {
			multi = new MultipartRequest(request, uploadPath,maxSize,"UTF-8",new DefaultFileRenamePolicy());
			
			name = multi.getParameter("productName");
			fileName1 = multi.getFilesystemName("file");
			originName = multi.getOriginalFileName("file");
			
			out.println("name : "+name);
			out.println("filename :"+fileName1);
			if(fileName1 != null) {
			product = new Product();
			String image = fileName1;
			product.setProductName(name);
			product.setProductGroup(multi.getParameter("productGroup"));
			product.setDetailGroup(multi.getParameter("detailGroup"));
			product.setColor(multi.getParameter("color"));
			product.setProductSize(multi.getParameter("productSize"));
			product.setPrice(Integer.parseInt(multi.getParameter("price")));
			product.setDetail(multi.getParameter("detail"));
			product.setInventory(Integer.parseInt(multi.getParameter("inventory")));
			product.setImage(image);
			}
			
		}catch (Exception e) {
			e.printStackTrace(); 
		}
		try {
		
		out.print(product.toString());
		
		service.register(product);
		response.sendRedirect(request.getContextPath());
		
		}catch (Exception e) {
			response.sendRedirect("/information/error.jsp");
		}
		
		

	}
}
