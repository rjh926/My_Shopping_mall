package controller.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

@WebServlet("/findByDetail.do")
public class FindByDetailServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String detailGroup = request.getParameter("detail");

		ProductService productService = new ProductServiceLogic();

		List<Product> products = productService.findByDetail(detailGroup);

		request.setAttribute("products", products);
		request.setAttribute("total", products.size());
		request.setAttribute("productGroup",request.getParameter("productGroup"));

		request.getRequestDispatcher("/list.jsp").forward(request, response);
	}


}
