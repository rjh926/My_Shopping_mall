package controller.product;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

/**
 * Servlet implementation class DeleteProductServlet
 */
@WebServlet("/product/delete.do")
public class DeleteProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		ProductService service= new ProductServiceLogic();
		
		Product product = service.findById(id);
		String image = request.getRealPath("/skin/images")+"/"+product.getImage();
		
		service.remove(product);
		File file = new File(image);
		
		if(file.exists()) {
			file.delete();
		}
		
		response.sendRedirect(request.getContextPath()+"/product/modifylist.do");
	}

}
