package controller.product;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

/**
 * Servlet implementation class ModifyProductServlet
 */
@WebServlet("/product/modify.do")
public class ModifyProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		ProductService service= new ProductServiceLogic();
		
		Product product = service.findById(id);
		request.setAttribute("product",product);
		request.getRequestDispatcher("/productModify.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String uploadPath = request.getRealPath("/skin/images");
		
		ProductService service = new ProductServiceLogic();
		
		int maxSize = 10 * 1024 * 1024;
		String fileName = "";
		Product product = null;
		MultipartRequest multi = null;
		
		try {
			multi = new MultipartRequest(request, uploadPath,maxSize,"UTF-8",new DefaultFileRenamePolicy());
			fileName = multi.getFilesystemName("file"); 
			if(fileName== null) {
				product = new Product();
				product.setProductId(multi.getParameter("id"));
				product.setProductName(multi.getParameter("productName"));
				product.setProductGroup(multi.getParameter("productGroup"));
				product.setDetailGroup(multi.getParameter("detailGroup"));
				product.setProductSize(multi.getParameter("productSize"));
				product.setColor(multi.getParameter("color"));
				product.setPrice(Integer.parseInt(multi.getParameter("price")));
				product.setDetail(multi.getParameter("detail"));
				product.setInventory(Integer.parseInt(multi.getParameter("inventory")));
				product.setImage(service.findById(multi.getParameter("id")).getImage());
				
				service.modify(product);
				response.sendRedirect(request.getContextPath()+"/product/modifylist.do");
			}
			else {
				fileName = multi.getFilesystemName("file");
				product = new Product();
				product.setProductId(multi.getParameter("id"));
				product.setProductName(multi.getParameter("productName"));
				product.setProductGroup(multi.getParameter("productGroup"));
				product.setDetailGroup(multi.getParameter("detailGroup"));
				product.setProductSize(multi.getParameter("productSize"));
				product.setColor(multi.getParameter("color"));
				product.setPrice(Integer.parseInt(multi.getParameter("price")));
				product.setDetail(multi.getParameter("detail"));
				product.setInventory(Integer.parseInt(multi.getParameter("inventory")));
				product.setImage(multi.getFilesystemName("file"));
				
				PrintWriter out = response.getWriter();
				out.println(product.toString());
				
				
				String originFile = service.findById(multi.getParameter("id")).getImage();
				File file = new File(uploadPath+"/"+originFile);
				out.println("파일 : "+uploadPath+"/"+originFile);
				if(file.exists()) {
					file.delete();
					out.print("파일삭제");
				}
				service.modify(product);
				response.sendRedirect(request.getContextPath()+"/product/modifylist.do");
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

}
