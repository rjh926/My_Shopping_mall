package controller.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;
import service.ProductService;
import service.logic.ProductServiceLogic;

@WebServlet("/findByGroup.do")
public class FindByGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String productGroup = request.getParameter("productGroup");

		ProductService productService = new ProductServiceLogic();

		List<Product> products = productService.findByGroup(productGroup);

		request.setAttribute("products", products);

		request.getRequestDispatcher("list.jsp").forward(request, response);

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String productGroup = request.getParameter("group");
		
		

		ProductService productService = new ProductServiceLogic();

		List<Product> products = productService.findByGroup(productGroup);

		request.setAttribute("products", products);
		request.setAttribute("productGroup", productGroup);
		request.setAttribute("total",products.size());

		request.getRequestDispatcher("list.jsp").forward(request, response);
	}


}
