package controller.mycart;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.MyCart;
import domain.Product;
import domain.User;
import service.MyCartService;
import service.ProductService;
import service.logic.MyCartServiceLogic;
import service.logic.ProductServiceLogic;

/**
 * Servlet implementation class MyCartAddServlet
 */
@WebServlet("/myCart/regist.do")
public class MyCartAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		if(user==null) {
			response.sendRedirect(request.getContextPath()+"/information/logout.jsp");
		}
		ProductService service = new ProductServiceLogic();
		MyCartService cartService = new MyCartServiceLogic();
		MyCart cart = new MyCart();
		String size = request.getParameter("Gsize");
		String color = request.getParameter("Gcolor");
		String name = request.getParameter("pName");
		
		Product product = service.findByOption(size, color, name);
		
		System.out.println(product.toString());
		System.out.println(user.toString());
		cart.setUserId(user.getUserId());
		cart.setProductId(Integer.parseInt(product.getProductId()));
		cart.setAmmount(Integer.parseInt(request.getParameter("Gqty")));
		
		System.out.println(cart.toString());
		cartService.register(cart);
		
		response.sendRedirect(request.getContextPath()+"/myCart/list.do");
		
		
		
	}

}
