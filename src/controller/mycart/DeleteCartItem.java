package controller.mycart;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.MyCart;
import domain.User;
import service.MyCartService;
import service.logic.MyCartServiceLogic;

/**
 * Servlet implementation class DeleteCartItem
 */
@WebServlet("/myCart/removeItem.do")
public class DeleteCartItem extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		MyCartService cService = new MyCartServiceLogic();
		User user =(User) session.getAttribute("user");
		String pId = request.getParameter("pId");
		MyCart cart = new MyCart();
		
		cart.setUserId(user.getUserId());
		cart.setProductId(Integer.parseInt(pId));
		cService.remove(cart);
		
		response.sendRedirect(request.getContextPath()+"/myCart/list.do");
		
	}

}
