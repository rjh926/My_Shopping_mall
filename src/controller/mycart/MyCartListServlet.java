package controller.mycart;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.MyCart;
import domain.Order;
import domain.OrderList;
import domain.Product;
import domain.User;
import service.MyCartService;
import service.ProductService;
import service.logic.MyCartServiceLogic;
import service.logic.ProductServiceLogic;
import store.ProductStore;


@WebServlet("/myCart/list.do")
public class MyCartListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MyCartService cartService = new MyCartServiceLogic();
		ProductService productService = new ProductServiceLogic();
		
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		int totalPrice = 0;
		
		List<MyCart> carts = cartService.findByUser(user.getUserId());
		
		for(MyCart cart : carts) {
			
			Product product = productService.findById(cart.getProductId()+"");
			cart.setProduct(product);
			
			totalPrice += product.getPrice()  * cart.getAmmount();
			
			
			
		}
		request.setAttribute("carts", carts);
		request.setAttribute("totalPrice",totalPrice);
		session.setAttribute("cartsNum", carts.size());
		
		request.getRequestDispatcher("/mycart.jsp").forward(request, response);
		
	}

	
}
