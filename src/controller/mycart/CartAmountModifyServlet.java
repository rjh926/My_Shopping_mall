package controller.mycart;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.MyCart;
import domain.User;
import service.MyCartService;
import service.logic.MyCartServiceLogic;

/**
 * Servlet implementation class CartAmountPlusServlet
 */
@WebServlet("/myCart/amountModify.do")
public class CartAmountModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user"); 
		String pId = request.getParameter("pId");
		String amount = request.getParameter("amount");
		String userId = user.getUserId();
		String cal = request.getParameter("cal");
		
		PrintWriter out = response.getWriter();
		
		out.println(cal);
		MyCartService cartService = new MyCartServiceLogic();
		int modify = Integer.parseInt(amount);
		if(cal.equals("add")) {
			modify ++;
			
			MyCart cart = new MyCart();
			
			cart.setUserId(userId);
			cart.setProductId(Integer.parseInt(pId));
			cart.setAmount(modify);
			
			cartService.modify(cart);
			
			response.sendRedirect(request.getContextPath()+"/myCart/list.do");
		}
		else if(cal.equals("subtract")) {
			
			if(modify>1) {
				modify--;
				
				MyCart cart = new MyCart();
				
				cart.setUserId(userId);
				cart.setProductId(Integer.parseInt(pId));
				cart.setAmount(modify);
				
				cartService.modify(cart);
				
				response.sendRedirect(request.getContextPath()+"/myCart/list.do");
			
			
			}
			else if(modify==1) {
				MyCart cart = new MyCart();
				cart.setUserId(userId);
				cart.setProductId(Integer.parseInt(pId));
				cartService.remove(cart);
				response.sendRedirect(request.getContextPath()+"/myCart/list.do");
			}
			
		}
	
		
		
		
		
		
		
		
		
	
		
	}

}
