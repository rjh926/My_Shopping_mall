package controller.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.MyCart;
import domain.User;
import service.MyCartService;
import service.UserService;
import service.logic.MyCartServiceLogic;
import service.logic.UserServiceLogic;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/login.do")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UserService service = new UserServiceLogic();
		MyCartService cartS = new MyCartServiceLogic();
		String userId = request.getParameter("userid");
		String password = request.getParameter("pass");
		String loginDenied = null;
		
		
		if(userId.equals("admin")) {
			if(password.equals("admin")) {
			HttpSession session = request.getSession();
			session.setAttribute("admin","admin");
			response.sendRedirect(request.getContextPath());
			
			}
		}

		else {
		
		User login = service.login(userId,password);
		

		if(login==null) {
			response.sendRedirect(request.getContextPath()+"/information/loginDenied.jsp");
		}
		else {
			HttpSession session = request.getSession();
			
			List<MyCart> carts = cartS.findByUser(login.getUserId());
			session.setAttribute("cartsNum",carts.size());
			session.setAttribute("user",login);
			response.sendRedirect(request.getContextPath());
			

		}
		}
	}
}