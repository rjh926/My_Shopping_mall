package controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;
import service.UserService;
import service.logic.UserServiceLogic;
import store.UserStore;


@WebServlet("/findPw.do")
public class FindPwServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		UserService service = new UserServiceLogic();
		
		String name = request.getParameter("Name");
		String id = request.getParameter("UserID");
		String email = request.getParameter("email");
		
		String password = service.findPw(name, id, email);
		
		request.setAttribute("password",password);
		request.setAttribute("id",id);
		request.getRequestDispatcher("/findPwOk.jsp").forward(request, response);
		
	
		
		
	}

}
