package controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;
import service.UserService;
import service.logic.UserServiceLogic;

/**
 * Servlet implementation class UserJoinServlet
 */
@WebServlet("/register.do")
public class UserJoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		User user = new User();
		UserService service = new UserServiceLogic();
		
		user.setName(request.getParameter("member_name"));
		user.setUserId(request.getParameter("member_id"));
		user.setPassword(request.getParameter("member_pass"));
		user.setBirth(request.getParameter("birth_year")+"-"+
						request.getParameter("birth_month")+"-"+
						request.getParameter("birth_day"));
		if(request.getParameter("sex").equals("0")) user.setGender("여");
		else user.setGender("남");
		
		user.setZipCode(request.getParameter("member_zipcode"));
		user.setAddress(request.getParameter("member_addr1")+" "+request.getParameter("member_addr2"));
		user.setEmail(request.getParameter("member_email"));
		user.setPhone(request.getParameter("cp_phone1")+"-"+request.getParameter("cp_phone2")+"-"+request.getParameter("cp_phone3"));
		
		service.register(user);
		
		response.sendRedirect(request.getContextPath()+"/login.jsp");
		
		
	

}
}
