package controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import service.UserService;
import service.logic.UserServiceLogic;

/**
 * Servlet implementation class UserModifyServlet
 */
@WebServlet("/userModify.do")
public class UserModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");

		UserService service = new UserServiceLogic();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		user.setEmail(request.getParameter("member_email"));
		user.setZipCode(request.getParameter("member_zipcode"));
		user.setAddress(request.getParameter("member_addr1")+" "+request.getParameter("member_addr2"));
		user.setPhone(request.getParameter("member_cphone"));
		
		service.modify(user);
		session.setAttribute("user",user);
		
		response.sendRedirect(request.getContextPath()+"/information/userModify.jsp");
		
		
	}

}
