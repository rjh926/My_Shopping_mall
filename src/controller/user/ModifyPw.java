package controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import service.UserService;
import service.logic.UserServiceLogic;

/**
 * Servlet implementation class ModifyPw
 */
@WebServlet("/modifyPw.do")
public class ModifyPw extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		UserService service = new UserServiceLogic();
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String modi = request.getParameter("modi");
		String pw = request.getParameter("pass");

		User user = service.login(id, password);
		
		
		user.setPassword(pw);
		
		service.modify(user);
		
		session.setAttribute("user",user);
		
		
		if(modi.equals("Y")) {
			response.sendRedirect(request.getContextPath() + "/userModify.jsp");
		}
		else
		response.sendRedirect(request.getContextPath() + "/information/passModify.jsp");
		
		
		
		
	}

}
