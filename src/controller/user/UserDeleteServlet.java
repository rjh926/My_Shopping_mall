package controller.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import service.UserService;
import service.logic.UserServiceLogic;

@WebServlet("/userDelete.do")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UserService service = new UserServiceLogic();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		session.invalidate();
		
		service.remove(user);
		
		response.sendRedirect(request.getContextPath());
		
	}

}
