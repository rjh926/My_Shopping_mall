package controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;
import service.logic.UserServiceLogic;

/**
 * Servlet implementation class FindIdServlet
 */
@WebServlet("/findId.do")
public class FindIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String name = request.getParameter("Name");
		String email = request.getParameter("email");
		
		UserService service= new UserServiceLogic();
		
		String userId = service.findId(name, email);
		
		PrintWriter out = response.getWriter();
		
		
		out.println(name+","+email);
		out.println(userId);
		
		
		request.setAttribute("userId", userId);
		request.getRequestDispatcher("/findIdOk.jsp").forward(request, response);
		
		
	}

}
