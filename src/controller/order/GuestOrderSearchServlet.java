package controller.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.MyCart;
import domain.Order;
import domain.Product;
import service.OrderService;
import service.ProductService;
import service.logic.OrderServiceLogic;
import service.logic.ProductServiceLogic;


/**
 * Servlet implementation class GuestOrderSearchServlet
 */
@WebServlet("/order/guest.do")
public class GuestOrderSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		OrderService service = new OrderServiceLogic();
		ProductService pservice = new ProductServiceLogic();
		String name = request.getParameter("name");
		String phone = request.getParameter("phone1") + "-"+request.getParameter("phone2")+"-"+request.getParameter("phone3");
		
		List<Order> orders = service.findBuyerName(name, phone);
		
		for(Order order : orders) {
		List<MyCart> carts = order.getProducts();
		
		for(MyCart cart : carts) {
		Product product = pservice.findById(cart.getProductId()+"");
		
		cart.setProduct(product);
		}
		
		}
		request.setAttribute("orders",orders);
		request.setAttribute("size", orders.size());
		
		request.getRequestDispatcher("/orderList.jsp").forward(request, response);
	}

}
