package controller.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.MyCart;
import domain.Order;
import domain.Product;
import domain.User;
import service.OrderService;
import service.ProductService;
import service.logic.OrderServiceLogic;
import service.logic.ProductServiceLogic;

@WebServlet("/order/list.do")
public class OrderListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProductService pservice= new ProductServiceLogic();
		OrderService service = new OrderServiceLogic();
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		List<Order> orders = service.findByUser(user);
		
		for(Order order : orders) {
		List<MyCart> carts = order.getProducts();
		
		for(MyCart cart : carts) {
		Product product = pservice.findById(cart.getProductId()+"");
		
		cart.setProduct(product);
		}
		
		}
		request.setAttribute("orders",orders);
		request.setAttribute("size", orders.size());
		
		request.getRequestDispatcher("/orderList.jsp").forward(request, response);
		
		
		
		
		
		
		
	}

}
