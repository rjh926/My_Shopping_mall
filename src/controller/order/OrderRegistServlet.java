package controller.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.javassist.expr.NewArray;

import domain.MyCart;
import domain.Order;
import domain.OrderList;
import domain.Product;
import domain.User;
import service.OrderService;
import service.ProductService;
import service.logic.OrderServiceLogic;
import service.logic.ProductServiceLogic;

/**
 * Servlet implementation class OrderRegistServlet
 */
@WebServlet("/order/regist.do")
public class OrderRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProductService pService = new ProductServiceLogic();
		String productSize = request.getParameter("Gsize");
		String color = request.getParameter("Gcolor");
		String productName = request.getParameter("pName");
		int amount = Integer.parseInt(request.getParameter("Gqty"));
		Product product = pService.findByOption(productSize,color,productName);
		int totalMoney = 0;
		List<OrderList> lists = new ArrayList<>();
		OrderList list = new OrderList();
		list.setProduct(product);
		list.setAmount(amount);
		list.setProductId(Integer.parseInt(product.getProductId()));
		
		lists.add(list);
		
		for(OrderList l : lists ) {
			totalMoney += l.getProduct().getPrice() * l.getAmount();
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("orderList", lists);
		request.setAttribute("total",lists.size());
		request.setAttribute("totalMoney",totalMoney);
		
		
		request.getRequestDispatcher("/order.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		
		HttpSession session = request.getSession();
		OrderService service = new OrderServiceLogic();
		Order order = new Order();
		Date date = new Date(); 
		List<MyCart> carts = new ArrayList<>();
		
		List<OrderList> lists = (List<OrderList>) session.getAttribute("orderList");
		
		for(OrderList list : lists) {
			MyCart cart = new MyCart();
			cart.setUserId(request.getParameter("member_name"));
			cart.setProductId(list.getProductId());
			cart.setAmmount(list.getAmount());
			carts.add(cart);
		}
		
		
		order.setProducts(carts);
		order.setBuyerName(request.getParameter("member_name"));
		order.setAddress(request.getParameter("member_addr1"));
		order.setPhone(request.getParameter("member_cphone1"));
		order.setOrderDate(new java.sql.Date(date.getTime()));
		order.setIsUser((User) session.getAttribute("user"));
		order.setPayment(request.getParameter("payment"));
		order.setInvoiceNum(0);
		order.setIsPaid("FALSE");
		
		PrintWriter out = response.getWriter();
		
		
		service.register(order);
		
		
		
		
		session.removeAttribute("orderList");
		
		response.sendRedirect(request.getContextPath()+"/order/list.do");
		
		
	}

}
