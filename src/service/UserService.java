package service;

import domain.User;

public interface UserService {
	public void register(User user);
	public void remove(User user);
	public void modify(User user);
	public User login(String id, String password);
	public int checkMileage(String userId);
	public String findId(String name, String email);
	public String findPw(String name, String id, String email);
	public String ConfirmId(String userId);
}
