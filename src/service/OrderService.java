package service;

import java.util.List;

import domain.Order;
import domain.User;

public interface OrderService {
	public int register(Order order);
	public Order findById(String orderId);
	public List<Order> findByUser(User user);
	public List<Order> findBuyerName(String name,String phone);
	public void remove(int OrderId);
	public void checkPaid(String orderId);
	
}
