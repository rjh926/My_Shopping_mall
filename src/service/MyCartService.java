package service;

import java.util.List;

import domain.MyCart;

public interface MyCartService {
	public void register(MyCart cart);
	public List<MyCart> findByUser(String userId);
	public int totalPrice(String userId);
	public void modify(MyCart cart);
	public void remove(MyCart cart);
	public void removeAll(String userId);
}
