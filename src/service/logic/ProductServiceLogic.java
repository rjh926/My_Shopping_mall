package service.logic;

import java.util.List;

import domain.Product;
import service.ProductService;
import store.ProductStore;
import store.logic.ProductStoreLogic;
import store.logic.UserStoreLogic;

public class ProductServiceLogic implements ProductService{
	
	ProductStore store;
	public ProductServiceLogic() {
		store = new ProductStoreLogic();
	}
	

	@Override
	public void register(Product product) {
		store.create(product);
		
	}

	@Override
	public void modify(Product product) {
		store.update(product);
		
	}

	@Override
	public void remove(Product product) {
		store.delete(product.getProductId());
		
	}

	@Override
	public List<Product> findAll() {
		List<Product> list = null;
		list = store.readAll();
		return list;
	}

	@Override
	public List<Product> findByName(String name) {
		List<Product> list = null;
		list = store.readByName(name);
		return list;
	}

	@Override
	public List<Product> findByGroup(String group) {
		List<Product> list = null;
		list = store.readByGroup(group);
		return list;
	}

	@Override
	public List<Product> findByDetail(String group) {
		List<Product> list = null;
		list = store.readByDetail(group);
		return list;
	}


	@Override
	public Product findById(String id) {
		Product product = store.readById(id);
		
		return product;
		
	}


	@Override
	public Product findByOption(String productSize, String color, String productName) {
		Product product= store.readByOption(productSize, color, productName);
		return product
				;
	}

}
