package service.logic;

import org.apache.catalina.Store;

import domain.User;
import service.UserService;
import store.UserStore;
import store.logic.UserStoreLogic;

public class UserServiceLogic implements UserService{

	UserStore store;
	public UserServiceLogic() {
		store = new UserStoreLogic();
	}
	@Override
	public void register(User user) {
		store.create(user);
	}



	@Override
	
	public void remove(User user) {
		store.delete(user);
	}

	@Override
	public User login(String id, String password) {
		
		User foundUser = store.readById(id);
		if(foundUser==null) return null;
		if(foundUser.getPassword().equals(password)) {
			return foundUser;
		}
		else return null;
	}

	@Override
	public int checkMileage(String userId) {
		User user = store.readById(userId);
		
		return user.getMileage();
	}



	@Override
	public String findId(String name, String email) {
		String userId = store.readID(name,email);
		return userId;
	}

	@Override
	public String findPw(String name,String id, String email) {
		String password = store.readPw(name,id,email);
		return password;
	}
	@Override
	public void modify(User user) {
	store.update(user);
		
	}
	@Override
	public String ConfirmId(String userId) {
		String check = store.checkId(userId);
		return check;
	}

}
