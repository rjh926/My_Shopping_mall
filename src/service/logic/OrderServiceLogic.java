package service.logic;

import java.util.ArrayList;
import java.util.List;

import domain.MyCart;
import domain.Order;
import domain.OrderList;
import domain.Product;
import domain.User;
import oracle.net.aso.p;
import service.OrderService;
import store.OrderProductStore;
import store.OrderStore;
import store.ProductStore;
import store.UserStore;
import store.logic.OrderProductStoreLogic;
import store.logic.OrderStoreLogic;
import store.logic.ProductStoreLogic;
import store.logic.UserStoreLogic;

public class OrderServiceLogic implements OrderService{

	OrderStore oStore;
	OrderProductStore opStore;
	UserStore uStore;
	ProductStore pStore;
	
	public OrderServiceLogic() {
		oStore = new OrderStoreLogic();
		opStore = new OrderProductStoreLogic();
		uStore = new UserStoreLogic();
		pStore = new ProductStoreLogic();
	}
	@Override
	public int register(Order order) {
		
		int sumMileage = 0;
		User orderUser = order.getIsUser();
		int totalMileage = 0;
		int msg=0;
		/* 0. 성공
		 * 1. 재고 부족 
		 * 
		 * */
		List<MyCart> list = order.getProducts();
		for(MyCart cart : list) {
			Product product = pStore.readById(cart.getProductId()+"");
			if(product.getInventory()>=cart.getAmmount()) continue;
			else {
				msg =1; // 재고가 부족합니다.
				break;
			}
		}
		if(msg==1) return msg;
		oStore.create(order);
		
		
		
		
		for(MyCart cart : list) {
			
			OrderList products = new OrderList();
			
			products.setOrderId(Integer.parseInt(order.getOrderId()));
			products.setProductId(cart.getProductId());
			products.setAmount(cart.getAmmount());
			
			
			if(orderUser != null) { // 회원일경우 마일리지 적립
			Product product = pStore.readById(Integer.toString(cart.getProductId()));
			sumMileage += ( product.getPrice() * cart.getAmmount() ) * 0.01;
			totalMileage += orderUser.getMileage() + sumMileage;
			orderUser.setMileage(totalMileage);
			}
			
			// 재고 수 차감
			Product product = pStore.readById(Integer.toString(cart.getProductId()));
			int inventory = product.getInventory();
			inventory -= products.getAmount();
			product .setInventory(inventory);
			pStore.update(product);
			
			//OrderList 등록
			opStore.create(products);
			
			
			
		}
		if(orderUser!=null) {
			uStore.update(order.getIsUser());
		}
		
		
		return msg;
		
		
		
	}

	@Override
	public List<Order> findByUser(User user) {
		
		List<Order> orders = oStore.readByUser(user);
		
		for(Order order : orders) {
			List<MyCart> carts = new ArrayList<>();
			for(OrderList list: opStore.selectByOrder(order)) {
				MyCart cart = new MyCart();
				cart.setUserId(user.getUserId());
				cart.setProductId(list.getProductId());
				cart.setAmmount(list.getAmount());
				carts.add(cart);
			}
			order.setProducts(carts);
		}
		
		return orders;
	}

	@Override
	public List<Order> findBuyerName(String name,String phone) {
		List<Order> orders = oStore.readByBuyer(name,phone);
		
		for(Order order : orders) {
			List<MyCart> carts = new ArrayList<>();
			
			for(OrderList list : opStore.selectByOrder(order)) {
				MyCart cart = new MyCart();
				cart.setUserId(name);
				cart.setProductId(list.getProductId());
				cart.setAmmount(list.getAmount());
				
				carts.add(cart);
			}
			order.setProducts(carts);
		}
		
		return orders;
	}

	@Override
	public void remove(int OrderId) {
		
		Order order = this.findById(Integer.toString(OrderId));
		List<MyCart> carts = order.getProducts();
		User orderUser = order.getIsUser();
		int mileage = 0;
		int totalMileage =0;
		// 재고량 반환
		
		for(MyCart cart : carts) {
			Product product = pStore.readById(Integer.toString(cart.getProductId()));
			int inventory = product.getInventory() + cart.getAmmount(); System.out.println("inventory : "+inventory);
			product.setInventory(inventory);
			pStore.update(product);
			
			
			
			if(orderUser != null) { // 회원일경우 마일리지 차감
				//product = pStore.readById(Integer.toString(cart.getProductId()));
				mileage -= (( product.getPrice() * cart.getAmmount() ) * 0.01);
				totalMileage -= orderUser.getMileage() + mileage;
				orderUser.setMileage(totalMileage);
				}
		}
		OrderList list = new OrderList();
		list.setOrderId(Integer.parseInt(order.getOrderId()));
		opStore.delete(list);
		
		oStore.delete(OrderId+"");
		
	}

	@Override
	public void checkPaid(String orderId) {
		oStore.getPaid(orderId);

	}
	@Override
	public Order findById(String orderId) {
		Order order = oStore.readById(orderId);
		User user = order.getIsUser();
		
	
		List<OrderList> lists = opStore.selectByOrder(order);
		List<MyCart> carts = new ArrayList<>();
		
		for(OrderList list : lists) {
			MyCart cart = new MyCart();
			cart.setUserId(order.getBuyerName());
			cart.setProductId(list.getProductId());
			cart.setAmmount(list.getAmount());
			
			carts.add(cart);
		}
		order.setProducts(carts);
		
		return order;
	}
}
