package service.logic;

import java.util.ArrayList;
import java.util.List;

import domain.MyCart;
import domain.Product;
import service.MyCartService;
import store.MyCartStore;
import store.ProductStore;
import store.logic.MyCartStoreLogic;
import store.logic.ProductStoreLogic;

public class MyCartServiceLogic implements MyCartService{
	MyCartStore cStore ;
	ProductStore pStore;
	
	public MyCartServiceLogic() {
		cStore = new MyCartStoreLogic();
		pStore = new ProductStoreLogic();
	}

	
	@Override
	public void register(MyCart cart) {
		
		cStore.create(cart);
		
		
	}

	@Override
	public int totalPrice(String userId) {
		List<MyCart> carts = cStore.read(userId);
		int totalprice= 0;
		
		for(MyCart cart : carts ) {
			Product product = pStore.readById(cart.getProductId()+"");
			totalprice += product.getPrice() * cart.getAmmount();
		}
		
		return totalprice;
	}

	@Override
	public void modify(MyCart cart) {
		cStore.update(cart);
		
	}

	@Override
	public void remove(MyCart cart) {
		cStore.delete(cart);
		
	}

	@Override
	public List<MyCart> findByUser(String userId) {
		List<MyCart> carts = cStore.read(userId);
		return carts;
	}


	@Override
	public void removeAll(String userId) {
		cStore.deleteAll(userId);
		
	}
	

}
