package service;

import java.util.List;

import domain.Product;

public interface ProductService {
	public void register(Product product);
	public void modify(Product product);
	public void remove(Product product);
	public Product findById(String id);
	public List<Product> findAll();
	public List<Product> findByName(String name);
	public List<Product> findByGroup(String group);
	public List<Product> findByDetail(String group);
	public Product findByOption(String productSize, String color,String productName);
}
