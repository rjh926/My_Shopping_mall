package service;

import domain.Deliver;

public interface DeliveryService {
	public void register(Deliver delivery);
	public Deliver findByOrderId(String orderId);
}
