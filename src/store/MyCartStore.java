package store;

import java.util.List;

import domain.MyCart;

public interface MyCartStore {
	
	public void create(MyCart cart);
	public List<MyCart> read(String userId);
	public void update(MyCart cart);
	public void delete(MyCart cart);
	public void deleteAll(String userId);

}
