package store;

import domain.User;

public interface UserStore {
	
	public void create(User user);
	public User readById(String userId);
	public String readID(String name, String email);
	public String readPw(String name,String id, String email);
	public void update(User user);
	public void delete(User user);
	public String checkId(String userId);

}
