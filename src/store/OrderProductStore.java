package store;

import java.util.List;

import domain.Order;
import domain.OrderList;

public interface OrderProductStore {
	
	public void create(OrderList list);
	public void delete(OrderList list);
	public List<OrderList> selectByOrder(Order order);

}
