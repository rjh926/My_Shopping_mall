package store;

import java.util.List;

import domain.Order;
import domain.User;

public interface OrderStore {

	public void create(Order order);
	public Order readById(String id);
	public List<Order> readByUser(User user);
	public List<Order> readByBuyer(String Buyername,String phone);
	public List<Order> readAll();
	public void delete(String orderId);
	public void update(Order order);
	public void getPaid(String orderId);
}
