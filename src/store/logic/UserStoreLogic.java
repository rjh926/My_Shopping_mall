package store.logic;

import org.apache.ibatis.session.SqlSession;

import domain.User;
import store.UserStore;
import store.mapper.UserMapper;

public class UserStoreLogic implements UserStore {
	
	private ProjectSessionFactory factory;

	public UserStoreLogic() {

		factory = ProjectSessionFactory.getInstance();
	}
	@Override
	public void create(User user) {
		SqlSession session = factory.getSession();
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.insert(user);
			session.commit();
		}finally {
			
			session.close();
		}
	}

	@Override
	public User readById(String userId) {
		SqlSession session = factory.getSession();
		User user = null ; 
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			user=mapper.readById(userId);
		}finally {
			session.close();
		}
		return user;
	}

	@Override
	public String readID(String name,String email) {
		SqlSession session = factory.getSession();
		String id = null; 
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			User user = new User();
			user.setEmail(email);
			user.setName(name);
			id = mapper.readId(user);
		}finally {
			session.close();
		}
		return id;
	}

	@Override
	public String readPw(String name, String id, String email) {
		SqlSession session = factory.getSession();
		String pw = null ; 
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			User user = new User();
			user.setName(name);
			user.setEmail(email);
			user.setUserId(id);
			
			pw = mapper.readPw(user);
		}finally {
			session.close();
		}
		return pw;
	}

	@Override
	public void update(User user) {
		SqlSession session = factory.getSession();
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.update(user);
			session.commit();
		}finally {
			session.close();
		}

	}

	@Override
	public void delete(User user) {
		SqlSession session = factory.getSession();
		 
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.delete(user);
		}finally {
			session.commit();
			session.close();
		}
	}
	@Override
	public String checkId(String userId) {
		SqlSession session = factory.getSession();
		String id = null;
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			id = mapper.checkId(userId);
		}finally {
			session.close();
		}
		return id;
	}

}
