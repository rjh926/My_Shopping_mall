package store.logic;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import domain.Order;
import domain.OrderList;
import store.OrderProductStore;
import store.mapper.OrderProductMapper;

public class OrderProductStoreLogic implements OrderProductStore {
	
	ProjectSessionFactory factory;
	
	public OrderProductStoreLogic() {
		factory = ProjectSessionFactory.getInstance();
	}

	@Override
	public void create(OrderList list) {
		SqlSession session = factory.getSession();
		
		try {
			OrderProductMapper mapper = session.getMapper(OrderProductMapper.class);
			mapper.create(list);
			session.commit();
		}finally {
			session.close();
		}

	}


	@Override
	public void delete(OrderList list) {
		SqlSession session = factory.getSession();
		
		try {
			OrderProductMapper mapper = session.getMapper(OrderProductMapper.class);
			mapper.delete(list);
			session.commit();
		}finally {
			session.close();
		}

	}

	@Override
	public List<OrderList> selectByOrder(Order order) {
		SqlSession session = factory.getSession();
		List<OrderList> products = null;
		try {
			OrderProductMapper mapper = session.getMapper(OrderProductMapper.class);
			products = mapper.selectByOrder(order);
		}finally {
			session.close();
		}
		
		return products;
	}

}
