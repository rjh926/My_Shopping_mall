package store.logic;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import domain.MyCart;
import store.MyCartStore;
import store.mapper.MyCartMapper;

public class MyCartStoreLogic implements MyCartStore {

	ProjectSessionFactory factory;
	
	public MyCartStoreLogic() {
		factory = ProjectSessionFactory.getInstance();
	}
	@Override
	public void create(MyCart cart) {
		SqlSession session = factory.getSession();
		try {
			MyCartMapper mapper = session.getMapper(MyCartMapper.class);
			mapper.create(cart);
			session.commit();
		}finally {
			session.close();
		}

	}

	@Override
	public List<MyCart> read(String userId) {
		SqlSession session = factory.getSession();
		List<MyCart> carts = null;
		
		try {
			MyCartMapper mapper = session.getMapper(MyCartMapper.class);
			carts = mapper.read(userId);
			
		}finally {
			session.close();
		}
		return carts;
	}

	@Override
	public void update(MyCart cart) {
		SqlSession session = factory.getSession();
		
		try {
			MyCartMapper mapper = session.getMapper(MyCartMapper.class);
			mapper.update(cart);
			session.commit();
		}finally {
			session.close();
		}
	}

	@Override
	public void delete(MyCart cart) {
		SqlSession session = factory.getSession();
		
		try {
			MyCartMapper mapper = session.getMapper(MyCartMapper.class);
			mapper.delete(cart);
			session.commit();
		}finally {
			session.close();
		}
	}
	@Override
	public void deleteAll(String userId) {
		SqlSession session = factory.getSession();
		
		try {
			MyCartMapper mapper = session.getMapper(MyCartMapper.class);
			mapper.deleteAll(userId);
			session.commit();
		}finally {
			session.close();
		}

}
}
