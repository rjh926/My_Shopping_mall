package store.logic;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class ProjectSessionFactory {
	
	private static ProjectSessionFactory instance;
	
	private static SqlSessionFactory factory;
	private static final String RESOURCE = "mybatis-config.xml";
	private ProjectSessionFactory() {	}

	public static ProjectSessionFactory getInstance() {
		if(instance ==null) {
			instance = new ProjectSessionFactory();
		}
		return instance;
	}
	
	public SqlSession getSession() {
		if(factory ==null) {
			Reader reader = null;
			
			try {
				reader = Resources.getResourceAsReader(RESOURCE);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			factory = new SqlSessionFactoryBuilder().build(reader);
		}
		return factory.openSession();
	}
}
