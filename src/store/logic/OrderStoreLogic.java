package store.logic;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import domain.Order;
import domain.User;
import store.OrderStore;
import store.mapper.OrderMapper;

public class OrderStoreLogic implements OrderStore {
	
	ProjectSessionFactory factory;
	
	public OrderStoreLogic() {
		factory = ProjectSessionFactory.getInstance();
	}

	@Override
	public void create(Order order) {
		SqlSession session = factory.getSession();
		
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			mapper.create(order);
			session.commit();
		}finally {
			session.close();
		}
	}

	@Override
	public Order readById(String orderId) {
		SqlSession session = factory.getSession();
		Order order = null;
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			order = mapper.readById(orderId);
		}finally {
			session.close();
		}
		return order;
	}

	@Override
	public List<Order> readByUser(User user) {

		SqlSession session = factory.getSession();
		List<Order> orders = null;
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			orders = mapper.readByUser(user);
		}finally {
			session.close();
		}
		return orders;
	}

	@Override
	public List<Order> readByBuyer(String buyerName,String phone) {
		SqlSession session = factory.getSession();
		List<Order> orders = null;
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			User user = new User();
			user.setName(buyerName);
			user.setPhone(phone);
			orders = mapper.readByBuyer(user);
		}finally {
			session.close();
		}
		return orders;
	}

	@Override
	public List<Order> readAll() {
		SqlSession session = factory.getSession();
		List<Order> orders = null;
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			orders = mapper.readAll();
		}finally {
			session.close();
		}		return orders;
	}

	@Override
	public void delete(String orderId) {
		SqlSession session = factory.getSession();
		
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			
			mapper.delete(orderId);
			session.commit();
		}finally {
			session.close();
		}
	}

	@Override
	public void update(Order order) {
		// TODO Auto-generated method stub

		
	}

	@Override
	public void getPaid(String orderId) {
		SqlSession session = factory.getSession();
		
		try {
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			
			mapper.getPaid(orderId);
			session.commit();
		}finally {
			session.close();
		}
		
	}
	

	

}
