package store.logic;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import domain.Product;
import store.ProductStore;
import store.mapper.ProductMapper;

public class ProductStoreLogic implements ProductStore {

	ProjectSessionFactory factory;
	
	public ProductStoreLogic() {
		factory = ProjectSessionFactory.getInstance();
	}
	
	@Override
	public void create(Product product) {
		SqlSession session = factory.getSession();
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			mapper.create(product);
			session.commit();
		}finally {
			session.close();
		}

	}

	@Override
	public Product readById(String productId) {
		SqlSession session = factory.getSession();
		Product product = null;
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			product = mapper.readById(productId);
		}finally {
			session.close();
		}
		return product;
	}

	@Override
	public List<Product> readByGroup(String group) {
		SqlSession session = factory.getSession();
		List<Product> list = null;
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			list = mapper.readByGroup(group);
		}finally {
			session.close();
		}
		return list;
	}

	@Override
	public List<Product> readByDetail(String detail) {
		SqlSession session = factory.getSession();
		List<Product> list = null;
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			list = mapper.readByDetail(detail);
		}finally {
			session.close();
		}
		return list;
	}

	@Override
	public List<Product> readAll() {
		SqlSession session = factory.getSession();
		List<Product> list = null;
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			list = mapper.readAll();
		}finally {
			session.close();
		}
		return list;
	}
	@Override
	public List<Product> readByName(String name) {
		SqlSession session = factory.getSession();
		List<Product> list = null;
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			list = mapper.readByName(name);
		}finally {
			session.close();
		}
		return list;
	}

	@Override
	public void update(Product product) {
		SqlSession session = factory.getSession();
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			mapper.update(product);
		}finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void delete(String productId) {
		SqlSession session = factory.getSession();
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			mapper.delete(productId);
		}finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public Product readByOption(String productSize, String color, String productName) {
		SqlSession session = factory.getSession();
		Product product = null;
		HashMap<String, String> map = new HashMap<>();
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			map.put("color",color);
			map.put("productSize",productSize);
			map.put("productName",productName);
			product = mapper.readByOption(map);
		}finally {
			session.close();
		}
		return product;
	}

	

}
