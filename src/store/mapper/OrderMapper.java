package store.mapper;

import java.util.List;

import domain.Order;
import domain.User;

public interface OrderMapper {
	public void create(Order order);
	public Order readById(String id);
	public List<Order> readByUser(User user);
	public List<Order> readByBuyer(User user);
	public List<Order> readAll();
	public void delete(String orderId);
	public void update(Order order);
	public void getPaid(String orderId);
}
