package store.mapper;

import domain.User;

 public interface UserMapper {
	
	 void insert(User user);
	 User readById(String userId);
	 String readId(User user);
	 String readPw(User user);
	 void update(User user);
	 void delete(User user);
	 String checkId(String userId);

}
