package store.mapper;

import java.util.List;

import domain.MyCart;

public interface MyCartMapper {
	
	public void create(MyCart cart);
	public List<MyCart> read(String id);
	public void update(MyCart cart);
	public void delete(MyCart cart);
	public void deleteAll(String userId);

}
