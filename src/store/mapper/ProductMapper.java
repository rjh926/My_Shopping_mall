package store.mapper;

import java.util.HashMap;
import java.util.List;

import domain.Product;

public interface ProductMapper {
	public void create(Product product);
	public Product readById(String productId);
	public List<Product> readByGroup(String group);
	public List<Product> readByDetail(String detail);
	public List<Product> readAll();
	public void update(Product product);
	public void delete(String productId);
	public List<Product> readByName(String name);
	public Product readByOption(HashMap<String,String> map);
	
}
