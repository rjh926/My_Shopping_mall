package store;

import domain.Deliver;

public interface DeliveryStore {
	
	public void create(Deliver delivery);
	public Deliver readByOrderId(int orderId);
	public void delete(int orderId);

}
